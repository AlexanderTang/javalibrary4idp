package examples._3coloring;

import static org.junit.Assert.*;
import idp.annotations.*;
import idp.elements.ThreeValuedIndicator;
import idp.exceptions.*;
import idp.logic.IDPOptimizationResults;
import idp.logic.IDPSolver;
import idp.logic.IDPStructure;
import idp.logic.IDPTerm;
import idp.logic.IDPTheory;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestThreeColoring {
	IDPTheory theory;
	URL idpConfigPath;
	List<NodeId> hashNodeIds;
	List<Node> nodes;
	List<Edge> edges;
	List<String> colors;
	
	@Before
	public void setUp() throws Exception {
		this.idpConfigPath = getClass().getClassLoader().getResource("idp.config");
		this.theory = IDPTheory.loadFile(getClass().getClassLoader().
				getResource("_3coloring/ThreeColoring.idptheory"));
		
		this.hashNodeIds = new ArrayList<NodeId>();
		for(int i = 0; i<8; i++)
			this.hashNodeIds.add(new NodeId());
		
		this.colors = new ArrayList<>();
		colors.addAll(Arrays.asList("blue", "red", "white"));
		
		this.edges = new ArrayList<>();
		edges.add(new Edge(this.hashNodeIds.get(0), this.hashNodeIds.get(1))); // link nodeid 1 with nodeid 2
		edges.add(new Edge(this.hashNodeIds.get(1), this.hashNodeIds.get(2))); // link nodeid 2 with nodeid 3
		edges.add(new Edge(this.hashNodeIds.get(1), this.hashNodeIds.get(3))); // link nodeid 2 with nodeid 4
		edges.add(new Edge(this.hashNodeIds.get(2), this.hashNodeIds.get(4))); // link nodeid 3 with nodeid 5
		edges.add(new Edge(this.hashNodeIds.get(2), this.hashNodeIds.get(5))); // link nodeid 3 with nodeid 6
		edges.add(new Edge(this.hashNodeIds.get(1), this.hashNodeIds.get(5))); // link nodeid 2 with nodeid 6
		edges.add(new Edge(this.hashNodeIds.get(0), this.hashNodeIds.get(6))); // link nodeid 1 with nodeid 7
		edges.add(new Edge(this.hashNodeIds.get(2), this.hashNodeIds.get(6))); // link nodeid 3 with nodeid 7
		edges.add(new Edge(this.hashNodeIds.get(6), this.hashNodeIds.get(7))); // link nodeid 7 with nodeid 8
		
		this.nodes = new ArrayList<>();
	}

	@After
	public void tearDown() throws Exception {
		this.theory = null;
		this.hashNodeIds = null;
		this.nodes = null;
		this.edges = null;
		this.colors = null;
	}

	/**
	 * Tests the default setting with nodes being an empty collection, entered as a 2-valued structure.
	 */
	@Test
	public void testDefault() {
		IDPStructure inputStruc = new IDPStructure();
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addCollection(null, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}
		
		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(false)
				.execTimeOutInMinutes(1)
				.build();
			
			boolean modelFound = solver.sat(inputStruc);
			assertTrue(modelFound);
			
			IDPStructure struc = solver.calculateDefinitions(inputStruc);
			try{
				struc.getCollection(Node.class);
				fail("No exception is thrown when retrieving 2-valued interpretation for"
						+ " Node.class, but was expecting UnavailableCollectionException.class.");
			} catch(UnavailableCollectionException e){
			}
			Collection<Node> trueNodes = struc.getTrueCollection(Node.class);
			assertTrue(trueNodes.isEmpty());
			Collection<Node> falseNodes = struc.getFalseCollection(Node.class);
			assertTrue(falseNodes.isEmpty());
			Collection<Node> unknownNodes = struc.getUnknownCollection(Node.class);
			assertEquals(unknownNodes.size(), 24);
			
			Collection<IDPStructure> defaultStructs = solver.modelExpand(inputStruc);
			assertTrue(defaultStructs.size() <= solver.getDefaultNumberModels());
			for(IDPStructure structure : defaultStructs){
				Collection<Node> resultingNodes = structure.getCollection(Node.class);
				assertEquals(resultingNodes.size(), this.hashNodeIds.size());
				Collection<Node> resultingTrueNodes = 
						structure.getCollection(Node.class, ThreeValuedIndicator.CT);
				for(Node n : resultingNodes){
					assertTrue(this.hashNodeIds.contains(n.getId()));
					assertEquals(resultingNodes.size(), resultingTrueNodes.size());
					assertTrue(resultingTrueNodes.contains(n));
				}
				assertTrue(this.nodes.isEmpty()); // verify that original collection remains unchanged
			}
			Collection<IDPStructure> allStructs = solver.modelExpand(inputStruc, 0, false);
			assertEquals(allStructs.size(), 144);
			
			IDPTerm term = IDPTerm.loadString("#{a[NodeId] : Node(a) = \"blue\"}");
			IDPOptimizationResults minResults = solver.minimize(inputStruc, term);
			Collection<IDPStructure> minStructs = minResults.getStructures();
			assertTrue(minStructs.size() <= solver.getDefaultNumberModels());
			assertEquals(minStructs.size(), 5);	// 10 models found, but 5 returned due to nbmodels option
			boolean minFound = minResults.optimumFound();
			assertTrue(minFound);
			int min = minResults.getOptimalValue();
			assertEquals(min, 1);
			
			URL url = getClass().getClassLoader().getResource("_3coloring/minimumBlue.idpterm");
			IDPTerm term2 = IDPTerm.loadFile(url);
		    IDPOptimizationResults maxResults = solver.maximize(inputStruc, term2);
		    Collection<IDPStructure> maxStructs = maxResults.getStructures();
		    assertTrue(maxStructs.size() <= solver.getDefaultNumberModels());
		    assertEquals(maxStructs.size(), 2);	// 2 models found
			boolean maxFound = maxResults.optimumFound();
			assertTrue(maxFound);
			int max = maxResults.getOptimalValue();
			assertEquals(max, 5);
		} catch (IDPException | IOException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		}
	}

	/**
	 * Tests the default setting with nodes being entered as a 3-valued structure. One object has been
	 * added to <ct> for the Node class.
	 */
	@Test
	public void testThreeValuedStructure(){
		IDPStructure inputStruc = new IDPStructure();
		Node node = new Node(this.hashNodeIds.get(0), "blue");
		this.nodes.add(node);
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addTrueCollection(this.nodes, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}

		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(false)
				.build();
		
			boolean modelFound = solver.sat(inputStruc);
			assertTrue(modelFound);
			
			{
				IDPStructure struc = solver.calculateDefinitions(inputStruc);
				
				try{
					struc.getCollection(Node.class);
					fail("No exception is thrown when retrieving 2-valued interpretation for"
							+ " Node.class, but was expecting UnavailableCollectionException.class.");
				} catch(UnavailableCollectionException e){
				}
				
				Collection<Node> trueNodes = struc.getTrueCollection(Node.class);
				assertTrue(trueNodes.contains(node)); // check correct function object reference
				assertEquals(trueNodes.size(), 1);
				
				Collection<Node> falseNodes = struc.getFalseCollection(Node.class);
				assertTrue(falseNodes.isEmpty());
				
				Collection<Node> unknownNodes = struc.getUnknownCollection(Node.class);
				assertEquals(unknownNodes.size(), 23); // 24-1-0
				
				Collection<Edge> allEdges = struc.getCollection(Edge.class);
				for(Edge e : allEdges)
					assertTrue(this.edges.contains(e)); // check correct predicate object references
			}
			
			{
				IDPStructure struc = solver.groundPropagate(inputStruc);
				
				try{
					struc.getCollection(Node.class);
					fail("No exception is thrown when retrieving 2-valued interpretation for"
							+ " Node.class, but was expecting UnavailableCollectionException.class.");
				} catch(UnavailableCollectionException e){
				}
				
				Collection<Node> trueNodes = struc.getTrueCollection(Node.class);
				assertTrue(trueNodes.contains(node));
				assertEquals(trueNodes.size(), 1);
				
				Collection<Node> falseNodes = struc.getFalseCollection(Node.class);
				assertEquals(falseNodes.size(), 4);
				
				for(Node n : falseNodes){
					if(n.getId().equals(node.getId()))
						assertTrue(n.getColor().equals("red") || n.getColor().equals("white"));
					else	n.getColor().equals("blue");
				}
				Collection<Node> unknownNodes = struc.getUnknownCollection(Node.class);
				assertEquals(unknownNodes.size(), 19); // 24-1-4
			}
			
			{
				IDPStructure struc = solver.optimalPropagate(inputStruc);
				
				try{
					struc.getCollection(Node.class);
					fail("No exception is thrown when retrieving 2-valued interpretation for"
							+ " Node.class, but was expecting UnavailableCollectionException.class.");
				} catch(UnavailableCollectionException e){
				}
				
				Collection<Node> trueNodes = struc.getTrueCollection(Node.class);
				assertTrue(trueNodes.contains(node));
				assertEquals(trueNodes.size(), 1);
				
				Collection<Node> falseNodes = struc.getFalseCollection(Node.class);
				assertEquals(falseNodes.size(), 4);
				for(Node n : falseNodes){
					if(n.getId().equals(node.getId()))
						assertTrue(n.getColor().equals("red") || n.getColor().equals("white"));
					else	n.getColor().equals("blue");
				}
				
				Collection<Node> unknownNodes = struc.getUnknownCollection(Node.class);
				assertEquals(unknownNodes.size(), 19); // 24-1-4
			}
			
			{
				IDPStructure struc = solver.propagate(inputStruc);
				
				try{
					struc.getCollection(Node.class);
					fail("No exception is thrown when retrieving 2-valued interpretation for"
							+ " Node.class, but was expecting UnavailableCollectionException.class.");
				} catch(UnavailableCollectionException e){
				}
				
				Collection<Node> trueNodes = struc.getTrueCollection(Node.class);
				assertTrue(trueNodes.contains(node));
				assertEquals(trueNodes.size(), 1);
				
				Collection<Node> falseNodes = struc.getFalseCollection(Node.class);
				assertEquals(falseNodes.size(), 2);
				for(Node n : falseNodes){
					assertTrue(n.getId().equals(node.getId()));
					assertTrue(n.getColor().equals("red") || n.getColor().equals("white"));
				}
				
				Collection<Node> unknownNodes = struc.getUnknownCollection(Node.class);
				assertEquals(unknownNodes.size(), 21); // 24-1-2
			}
			
			Collection<IDPStructure> defaultStructs = solver.modelExpand(inputStruc);
			assertTrue(defaultStructs.size() <= solver.getDefaultNumberModels());
			for(IDPStructure structure : defaultStructs){
				Collection<Node> resultingNodes = structure.getCollection(Node.class);
				assertEquals(resultingNodes.size(), this.hashNodeIds.size());
				for(Node n : resultingNodes)
					assertTrue(this.hashNodeIds.contains(n.getId()));
				assertEquals(this.nodes.size(), 1); // verify that original collection remains unchanged
				assertTrue(this.nodes.contains(node));
			}
			Collection<IDPStructure> allStructs = solver.modelExpand(inputStruc, 0, false);
			assertEquals(allStructs.size(), 48);
			
			IDPTerm term = IDPTerm.loadString("#{a[NodeId] : Node(a) = \"blue\"}");
			IDPOptimizationResults minResults = solver.minimize(inputStruc, term);
			Collection<IDPStructure> minStructs = minResults.getStructures();
			for(IDPStructure minStruc : minStructs){
				assertEquals(minStruc.getCollection(Node.class).size(), 8);
				assertEquals(minStruc.getTrueCollection(Node.class).size(), 8);
				assertTrue(minStruc.getUnknownCollection(Node.class).isEmpty());
			}
			assertTrue(minStructs.size() <= solver.getDefaultNumberModels());
			assertEquals(minStructs.size(), 5);	// 6 models found, but 5 returned due to nbmodels option
			boolean minFound = minResults.optimumFound();
			assertTrue(minFound);
			int min = minResults.getOptimalValue();
			assertEquals(min, 2);
			
			URL url = getClass().getClassLoader().getResource("_3coloring/minimumBlue.idpterm");
			IDPTerm term2 = IDPTerm.loadFile(url);
		    IDPOptimizationResults maxResults = solver.maximize(inputStruc, term2);
		    Collection<IDPStructure> maxStructs = maxResults.getStructures();
		    for(IDPStructure maxStruc : maxStructs){
				assertEquals(maxStruc.getCollection(Node.class).size(), 8);
				assertEquals(maxStruc.getTrueCollection(Node.class).size(), 8);
				assertTrue(maxStruc.getUnknownCollection(Node.class).isEmpty());
			}
		    assertTrue(maxStructs.size() <= solver.getDefaultNumberModels());
		    assertEquals(maxStructs.size(), 2);	// 2 models found
			boolean maxFound = maxResults.optimumFound();
			assertTrue(maxFound);
			int max = maxResults.getOptimalValue();
			assertEquals(max, 5);
		} catch (IDPException | IOException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		}
	}
	
	/**
	 * Tests the default setting with nodes being entered as a 3-valued structure. This uses the
	 * alternative ways of adding/getting 3-valued collections from the IDP structure (using enums
	 * from the ThreeValuedIndicator class.
	 * @see idp.elements.ThreeValuedIndicator
	 */
	@Test
	public void testThreeValuedStructure2(){
		IDPStructure inputStruc = new IDPStructure();
		Node node = new Node(this.hashNodeIds.get(0), "blue");
		Node node2 = new Node(this.hashNodeIds.get(1), "red");
		this.nodes.add(node);
		this.nodes.add(node2);
		List<Node> falseInitialNodes = new ArrayList<>();
		Node node3 = new Node(this.hashNodeIds.get(2), "white");
		falseInitialNodes.add(node3);
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addCollection(this.nodes, Node.class, ThreeValuedIndicator.CT);
			inputStruc.addCollection(falseInitialNodes, Node.class, ThreeValuedIndicator.CF);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}

		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(false)
				.build();
		
			boolean modelFound = solver.sat(inputStruc);
			assertTrue(modelFound);
			
			{
				IDPStructure struc = solver.calculateDefinitions(inputStruc);
				
				try{
					struc.getCollection(Node.class);
					fail("No exception is thrown when retrieving 2-valued interpretation for"
							+ " Node.class, but was expecting UnavailableCollectionException.class.");
				} catch(UnavailableCollectionException e){
				}
				
				Collection<Node> trueNodes = struc.getCollection(Node.class, ThreeValuedIndicator.CT);
				assertEquals(trueNodes.size(), 2);
				for(Node n : trueNodes)
					assertTrue(this.nodes.contains(n));
				
				Collection<Node> falseNodes = struc.getCollection(Node.class, ThreeValuedIndicator.CF);
				assertEquals(falseNodes.size(), 1);
				assertTrue(falseNodes.contains(node3));
				
				Collection<Node> unknownNodes = struc.getCollection(Node.class, ThreeValuedIndicator.U);
				assertEquals(unknownNodes.size(), 21); // 24-2-1
				for(Node n : unknownNodes){
					assertFalse(trueNodes.contains(n));
					assertFalse(falseNodes.contains(n));
				}
			}
			
			{
				IDPStructure struc = solver.groundPropagate(inputStruc);
				
				try{
					struc.getCollection(Node.class);
					fail("No exception is thrown when retrieving 2-valued interpretation for"
							+ " Node.class, but was expecting UnavailableCollectionException.class.");
				} catch(UnavailableCollectionException e){
				}
				
				Collection<Node> trueNodes = struc.getTrueCollection(Node.class);
				assertTrue(trueNodes.contains(node));
				assertEquals(trueNodes.size(), 4);
				
				Collection<Node> falseNodes = struc.getFalseCollection(Node.class);
				assertEquals(falseNodes.size(), 11);
				
				Collection<Node> unknownNodes = struc.getUnknownCollection(Node.class);
				assertEquals(unknownNodes.size(), 9); // 24-4-11
			}
			
			{
				IDPStructure struc = solver.optimalPropagate(inputStruc);
				
				try{
					struc.getCollection(Node.class);
					fail("No exception is thrown when retrieving 2-valued interpretation for"
							+ " Node.class, but was expecting UnavailableCollectionException.class.");
				} catch(UnavailableCollectionException e){
				}
				
				Collection<Node> trueNodes = struc.getTrueCollection(Node.class);
				assertTrue(trueNodes.contains(node));
				assertEquals(trueNodes.size(), 4);
				
				Collection<Node> falseNodes = struc.getFalseCollection(Node.class);
				assertEquals(falseNodes.size(), 11);
				
				Collection<Node> unknownNodes = struc.getUnknownCollection(Node.class);
				assertEquals(unknownNodes.size(), 9); // 24-4-11
			}
			
			{
				IDPStructure struc = solver.propagate(inputStruc);
				
				try{
					struc.getCollection(Node.class);
					fail("No exception is thrown when retrieving 2-valued interpretation for"
							+ " Node.class, but was expecting UnavailableCollectionException.class.");
				} catch(UnavailableCollectionException e){
				}
				
				Collection<Node> trueNodes = struc.getTrueCollection(Node.class);
				assertTrue(trueNodes.contains(node));
				assertEquals(trueNodes.size(), 2);
				
				Collection<Node> falseNodes = struc.getFalseCollection(Node.class);
				assertEquals(falseNodes.size(), 5);
				
				Collection<Node> unknownNodes = struc.getUnknownCollection(Node.class);
				assertEquals(unknownNodes.size(), 17); // 24-2-5
			}
			
			Collection<IDPStructure> defaultStructs = solver.modelExpand(inputStruc);
			assertTrue(defaultStructs.size() <= solver.getDefaultNumberModels());
			for(IDPStructure structure : defaultStructs){
				Collection<Node> resultingNodes = structure.getCollection(Node.class);
				assertEquals(resultingNodes.size(), this.hashNodeIds.size()); // there needs to be an equal amount of nodes as node ids
				Collection<Node> resultingTrueNodes = structure.getTrueCollection(Node.class);
				for(Node n : resultingNodes){
					assertTrue(this.hashNodeIds.contains(n.getId()));
					assertEquals(resultingNodes.size(), resultingTrueNodes.size());
					assertTrue(resultingTrueNodes.contains(n));
				}
				assertTrue(structure.getUnknownCollection(Node.class).isEmpty());
				assertEquals(this.nodes.size(), 2); // verify that original collection remains unchanged
				assertTrue(this.nodes.contains(node));
			}
			Collection<IDPStructure> allStructs = solver.modelExpand(inputStruc, 0, false);
			assertEquals(allStructs.size(), 16);
			
			IDPTerm term = IDPTerm.loadString("#{a[NodeId] : Node(a) = \"blue\"}");
			IDPOptimizationResults minResults = solver.minimize(inputStruc, term);
			Collection<IDPStructure> minStructs = minResults.getStructures();
			assertTrue(minStructs.size() <= solver.getDefaultNumberModels());
			assertEquals(minStructs.size(), 4);
			for(IDPStructure minStruc : minStructs){
				assertEquals(minStruc.getCollection(Node.class).size(), 8);
				assertEquals(minStruc.getTrueCollection(Node.class).size(), 8);
				assertTrue(minStruc.getUnknownCollection(Node.class).isEmpty());
			}
			boolean minFound = minResults.optimumFound();
			assertTrue(minFound);
			int min = minResults.getOptimalValue();
			assertEquals(min, 2);
			
			URL url = getClass().getClassLoader().getResource("_3coloring/minimumBlue.idpterm");
			IDPTerm term2 = IDPTerm.loadFile(url);
		    IDPOptimizationResults maxResults = solver.maximize(inputStruc, term2);
		    Collection<IDPStructure> maxStructs = maxResults.getStructures();
		    for(IDPStructure maxStruc : maxStructs){
				assertEquals(maxStruc.getCollection(Node.class).size(), 8);
				assertEquals(maxStruc.getTrueCollection(Node.class).size(), 8);
				assertTrue(maxStruc.getUnknownCollection(Node.class).isEmpty());
			}
		    assertTrue(maxStructs.size() <= solver.getDefaultNumberModels());
		    assertEquals(maxStructs.size(), 4);	
			boolean maxFound = maxResults.optimumFound();
			assertTrue(maxFound);
			int max = maxResults.getOptimalValue();
			assertEquals(max, 4);
		} catch (IDPException | IOException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		}
	}
	
	/**
	 * Tests predicate collection with no @Predicate annotated class.
	 */
	@Test(expected=InvalidAnnotationException.class)
	public void testInvalidPredicateAnnotationCollection() throws InvalidAnnotationException{
		IDPStructure inputStruc = new IDPStructure();
		List<DummyPredicate> preds = new ArrayList<>();
		preds.add(new DummyPredicate());
		try {
			inputStruc.addCollection(preds, DummyPredicate.class);
		} catch (InvalidStructureException e) {
			fail("Did not expect this exception to be thrown. Error msg: " + e.getMessage());
		}
	}
	
	public class DummyPredicate{
		@ArgumentType(1)
		private String color;
		
		private DummyPredicate(){
			this.color = "blue";
		}
	}
	
	/**
	 * Tests predicate collection with both @Predicate and @Function annotated class.
	 */
	@Test(expected=InvalidAnnotationException.class)
	public void testInvalidPredicateAnnotationCollection2() throws InvalidAnnotationException{
		IDPStructure inputStruc = new IDPStructure();
		List<DummyFunction> preds = new ArrayList<>();
		preds.add(new DummyFunction());
		try {
			inputStruc.addCollection(preds, DummyFunction.class);
		} catch (InvalidStructureException e) {
			fail("Did not expect this exception to be thrown. Error msg: " + e.getMessage());
		}
	}
	
	@Predicate
	@Function
	public class DummyFunction{
		@ArgumentType(1)
		private String color;
		@FunctionValue
		private int output;
		
		private DummyFunction(){
			this.color = "blue";
			this.output = 1;
		}
	}
	
	/**
	 * Tests predicate collection with @Predicate annotated class, adding it as both a 2-valued and
	 * 3-valued structure.
	 * @throws InvalidStructureException 
	 */
	@Test(expected=InvalidStructureException.class)
	public void testInvalidPredicateAnnotationCollection3() throws InvalidStructureException{
		IDPStructure inputStruc = new IDPStructure();
		List<DummyPredicate2> preds = new ArrayList<>();
		preds.add(new DummyPredicate2());
		List<DummyPredicate2> truePreds = new ArrayList<>();
		truePreds.add(new DummyPredicate2());
		try {
			inputStruc.addCollection(preds, DummyPredicate2.class);
			inputStruc.addCollection(truePreds, DummyPredicate2.class, ThreeValuedIndicator.CT);
		} catch (InvalidAnnotationException e) {
			fail("Did not expect this exception to be thrown. Error msg: " + e.getMessage());
		}
	}
	
	/**
	 * Tests the results of adding a type object as a three valued structure.
	 */
	@Test(expected = InvalidAnnotationException.class)
	public void testInvalidThreeValuedType() throws InvalidAnnotationException{
		List<DummyType> types = new ArrayList<>();
		types.add(new DummyType());
		
		try {
			IDPStructure inputStruc = new IDPStructure();
			inputStruc.addCollection(types, DummyType.class);
			// shouldn't give any problems here
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Did not expect this exception to be thrown. Error msg: " + e.getMessage());
		}
		
		try {
			IDPStructure inputStruc = new IDPStructure();
			inputStruc.addCollection(types, DummyType.class, ThreeValuedIndicator.CT);
			// types cannot be 3-valued
		} catch (InvalidStructureException e) {
			fail("Did not expect this exception to be thrown. Error msg: " + e.getMessage());
		}
	}
	
	@Type
	public class DummyType{
		@TypeValue
		private String color;
		
		private DummyType(){
			this.color = "blue";
		}
	}
	
	@Predicate
	public class DummyPredicate2{
		@ArgumentType(1)
		private String color;
		
		private DummyPredicate2(){
			this.color = "blue";
		}
	}
	
	/**
	 * Tests automatic fixing of missing type value. 
	 * Infer missing NodeId type value (instance hash code in this case) from an Edge predicate.
	 */
	@Test
	public void testMissingTypeValuesFix(){
		IDPStructure inputStruc = new IDPStructure();
		NodeId nodeId = new NodeId();
		this.edges.add(new Edge(this.hashNodeIds.get(3), nodeId));
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addCollection(null, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}

		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(false)
				.build();
		
			boolean modelFound = solver.sat(inputStruc);
			assertTrue(modelFound);
			
			IDPStructure struc = solver.calculateDefinitions(inputStruc);
			assertEquals(this.hashNodeIds.size(), 8); // original list of node ids
			assertEquals(struc.getCollection(NodeId.class).size(), 9); // inferred list of node ids in result
			
			IDPStructure singleStruc = solver.singleModelExpand(inputStruc);
			assertEquals(this.hashNodeIds.size(), 8); // original list of node ids
			assertEquals(singleStruc.getCollection(NodeId.class).size(), 9); // inferred list of node ids in result
			assertEquals(singleStruc.getCollection(Node.class).size(), 9); // the inferred node id also needs to be assigned a color
			assertTrue(singleStruc.getCollection(NodeId.class).contains(nodeId));
		} catch (IDPException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		} catch (IOException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		}
	}
	
	/**
	 * Tests automatic fixing of missing type values. Infer missing NodeId type and Color type from 
	 * a Node function. The missing NodeId type is linked to another NodeId through an Edge predicate as well.
	 * The addition of the extra color modifies the problem to become a 4-coloring problem instead
	 * of a 3-coloring problem.
	 */
	@Test
	public void testMissingTypeValuesFix2(){
		IDPStructure inputStruc = new IDPStructure();
		NodeId nodeId = new NodeId();
		this.edges.add(new Edge(this.hashNodeIds.get(3), nodeId));
		Node node = new Node(nodeId, "black");
		this.nodes.add(node);
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addCollection(this.nodes, Node.class, ThreeValuedIndicator.CT);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}

		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(false)
				.build();
		
			boolean modelFound = solver.sat(inputStruc);
			assertTrue(modelFound);
			
			IDPStructure struc = solver.calculateDefinitions(inputStruc);
			assertEquals(this.hashNodeIds.size(), 8); // original list of node ids
			assertEquals(struc.getCollection(NodeId.class).size(), 9); // inferred list of node ids in result
			assertEquals(this.colors.size(), 3);
			assertEquals(inputStruc.getStringCollection("color").size(), 3);
			assertEquals(struc.getStringCollection("color").size(), 4);
			assertTrue(struc.getStringCollection("color").contains("black"));
			
			IDPStructure singleStruc = solver.singleModelExpand(inputStruc);
			assertEquals(this.hashNodeIds.size(), 8); // original list of node ids
			assertEquals(singleStruc.getCollection(NodeId.class).size(), 9); // inferred list of node ids in result
			assertEquals(this.colors.size(), 3);
			assertEquals(singleStruc.getStringCollection("color").size(), 4);
			assertTrue(singleStruc.getStringCollection("color").contains("black"));
		} catch (IDPException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		} catch (IOException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		}
	}
	
	/**
	 * Tests automatic fixing of missing types. 
	 * Infer missing NodeId type from an Edge predicate. NodeId collection will not be
	 * entered at all; the collection will be derived solely from the Edge predicates and
	 * Node functions.
	 */
	@Test
	public void testMissingTypesFix(){
		IDPStructure inputStruc = new IDPStructure();
		try {
			//inputStruc.addCollection(this.hashNodeIds, NodeId.class); // not entered
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addCollection(null, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}

		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(false)
				.build();
			
			try{
				inputStruc.getCollection(NodeId.class);
				fail("No exception is thrown when retrieving an empty type collection with elements of"
						+ " NodeId.class, but was expecting UnavailableCollectionException.class.");
			} catch(UnavailableCollectionException e){
			}
			IDPStructure singleStruc = solver.singleModelExpand(inputStruc);
			assertEquals(singleStruc.getCollection(NodeId.class).size(), 8); // inferred list of node ids in result
		} catch (IDPException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		} catch (IOException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		}
	}
	
	/**
	 * Tests results if non-total functions are given as input to IDP. One Node is entered as a
	 * 2-valued structure out of 8 Nodes.
	 */
	@Test(expected=IDPExecutionException.class)
	public void testNonTotalFunction() throws IDPExecutionException{
		IDPStructure inputStruc = new IDPStructure();
		this.nodes.add(new Node(this.hashNodeIds.get(0), "blue"));
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addCollection(this.nodes, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}

		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(false)
				.build();
		
			boolean modelFound = solver.sat(inputStruc);
			assertTrue(modelFound);
		} catch (FailedTempFileException | InvalidAnnotationException | IOException
				| IDPCoreMappingError | InvalidConstructorException | MissingResourceException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		}
	}
	
	/**
	 * Tests results if there are no possible solutions. 
	 */
	@Test(expected=NilStructureException.class)
	public void testImpossibleSolution() throws NilStructureException{
		IDPStructure inputStruc = new IDPStructure();
		this.edges.add(new Edge(this.hashNodeIds.get(0), this.hashNodeIds.get(2)));
		this.edges.add(new Edge(this.hashNodeIds.get(0), this.hashNodeIds.get(3)));
		this.edges.add(new Edge(this.hashNodeIds.get(2), this.hashNodeIds.get(3)));
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addCollection(null, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}

		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(false)
				.build();
		
			boolean modelFound = solver.sat(inputStruc);
			assertFalse(modelFound);
			
			Collection<IDPStructure> defaultStructs = solver.modelExpand(inputStruc);
			assertTrue(defaultStructs.isEmpty());
			
			solver.singleModelExpand(inputStruc);
			fail("Expected a nil structure exception at this point");
		} catch (FailedTempFileException | IOException | InvalidAnnotationException 
				| IDPCoreMappingError | IDPExecutionException | InvalidConstructorException | MissingResourceException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		} 
	}
	
	/**
	 * Tests results if there are no possible solutions. 
	 */
	@Test(expected=NilStructureException.class)
	public void testImpossibleSolution2() throws NilStructureException{
		IDPStructure inputStruc = new IDPStructure();
		List<Node> trueNodes = new ArrayList<>();
		trueNodes.add(new Node(this.hashNodeIds.get(0), this.colors.get(0)));
		List<Node> falseNodes = new ArrayList<>();
		falseNodes.add(new Node(this.hashNodeIds.get(0), this.colors.get(0)));
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addTrueCollection(trueNodes, Node.class);
			inputStruc.addFalseCollection(falseNodes, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}

		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(false)
				.build();
		
			solver.propagate(inputStruc);
			fail("Expected a nil structure exception at this point");
		} catch (FailedTempFileException | IOException | InvalidAnnotationException 
				|  IDPCoreMappingError | IDPExecutionException | InvalidConstructorException | MissingResourceException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		} 
	}
	
	/**
	 * Tests exception if idp times out before a solution has been computed.
	 */
	@Test(expected=IDPExecutionException.class)
	public void testExecutionTimeOut() throws IDPExecutionException{
		IDPStructure inputStruc = new IDPStructure();
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addCollection(null, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}

		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(0)
				.replaceInput(false)
				.execTimeOutInMilliSeconds(1)
				.build();
		
			solver.propagate(inputStruc);
			fail("Expected a time out exception at this point");
		} catch (FailedTempFileException | IOException | InvalidAnnotationException 
				|  IDPCoreMappingError | NilStructureException | InvalidConstructorException | MissingResourceException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		}
	}
	
	/**
	 * Tests exception if null is entered as a collection for an @Type annotated class.
	 */
	@Test
	public void testInvalidTypeCollection(){
		IDPStructure inputStruc = new IDPStructure();
		try {
			inputStruc.addCollection(null, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addCollection(null, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}
		
		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(false)
				.build();
		
			IDPStructure struc = solver.propagate(inputStruc);
			try {
				Collection<NodeId> nodeIds = struc.getCollection(NodeId.class);
				assertEquals(nodeIds.size(), 8); // empty collection instead of null; missing values are filled automatically
			} catch (UnavailableCollectionException e) {
				fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
			}
		} catch (FailedTempFileException | IOException | InvalidAnnotationException 
				| IDPCoreMappingError | IDPExecutionException | InvalidConstructorException 
				| NilStructureException | MissingResourceException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		} 
	}
	
	/**
	 * Tests exception when the constructor for a function lacks the function value.
	 */
	@Test(expected=InvalidConstructorException.class)
	public void testInvalidConstructor() throws InvalidConstructorException {
		IDPStructure inputStruc = new IDPStructure();
		List<Node2> nds = new ArrayList<>();
		Node2 n = new Node2(this.hashNodeIds.get(0));
		n.color = this.colors.get(0);
		nds.add(n);
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addTrueCollection(nds, Node2.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}

		try {
			IDPTheory theo = IDPTheory.loadFile(getClass().getClassLoader().
					getResource("_3coloring/ThreeColoring2.idptheory"));
			IDPSolver solver = new IDPSolver.SolverBuilder(theo, this.idpConfigPath)
				.nbModels(0)
				.replaceInput(false)
				.build();
		
			solver.propagate(inputStruc);
			fail("Expected an invalid constructor exception at this point");
		} catch (FailedTempFileException | IOException | InvalidAnnotationException 
				| NilStructureException | IDPExecutionException | MissingResourceException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		} 
	}
	
	/**
	 * Tests exception when the constructor parameters for a function are mixed up.
	 */
	@Test(expected=InvalidConstructorException.class)
	public void testInvalidConstructor2() throws InvalidConstructorException {
		IDPStructure inputStruc = new IDPStructure();
		List<Node3> nds = new ArrayList<>();
		nds.add(new Node3(this.colors.get(0), this.hashNodeIds.get(0)));
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addTrueCollection(nds, Node3.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}

		try {
			IDPTheory theo = IDPTheory.loadFile(getClass().getClassLoader().
					getResource("_3coloring/ThreeColoring3.idptheory"));
			IDPSolver solver = new IDPSolver.SolverBuilder(theo, this.idpConfigPath)
				.nbModels(0)
				.replaceInput(false)
				.build();
		
			solver.propagate(inputStruc);
			fail("Expected an invalid constructor exception at this point");
		} catch (FailedTempFileException | IOException | InvalidAnnotationException 
				| NilStructureException | IDPExecutionException | MissingResourceException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		} 
	}
	
	/**
	 * Tests normal execution if one of the constructors' parameters for a function 
	 * are mixed up, while another constructor has the correct implementation.
	 */
	@Test
	public void testInvalidConstructor3() {
		IDPStructure inputStruc = new IDPStructure();
		List<Node5> nds = new ArrayList<>();
		// initializing using the 'wrong' constructor is no problem
		nds.add(new Node5(this.colors.get(0), this.hashNodeIds.get(0))); 
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addTrueCollection(nds, Node5.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}

		try {
			IDPTheory theo = IDPTheory.loadFile(getClass().getClassLoader().
					getResource("_3coloring/ThreeColoring5.idptheory"));
			IDPSolver solver = new IDPSolver.SolverBuilder(theo, this.idpConfigPath)
				.nbModels(0)
				.replaceInput(false)
				.build();
		
			solver.propagate(inputStruc);
		} catch (FailedTempFileException | IOException | InvalidAnnotationException 
				| NilStructureException | IDPExecutionException | InvalidConstructorException | MissingResourceException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		} 
	}

	/**
	 * Tests replacement of original input.
	 */
	@Test
	public void testInputReplacement(){
		IDPStructure inputStruc = new IDPStructure();
		Node node = new Node(this.hashNodeIds.get(0), "blue");
		this.nodes.add(node);
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addTrueCollection(this.nodes, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}
		
		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(true)
				.build();
			
			try{
				inputStruc.getFalseCollection(Node.class);
				fail("No exception is thrown when retrieving a non-existent unknown collection for"
						+ " Node.class, but was expecting UnavailableCollectionException.class.");
			} catch(UnavailableCollectionException e){
			}
			
			IDPStructure struc = solver.propagate(inputStruc);
			
			try{
				struc.getCollection(Node.class);
				fail("No exception is thrown when retrieving 2-valued interpretation for"
						+ " Node.class, but was expecting UnavailableCollectionException.class.");
			} catch(UnavailableCollectionException e){
			}
			
			Collection<Node> trueNodes = struc.getTrueCollection(Node.class);
			assertFalse(trueNodes.isEmpty());
			assertTrue(trueNodes.contains(node));
			
			Collection<Node> falseNodes = struc.getFalseCollection(Node.class);
			assertFalse(falseNodes.isEmpty());
			assertEquals(falseNodes.size(), 2);
			assertEquals(inputStruc.getFalseCollection(Node.class).size(),2);
			
			assertFalse(struc.getUnknownCollection(Node.class).isEmpty());
		} catch (IDPException | IOException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		}
	}

	/**
	 * Tests replacement of original input with model expansion parameter.
	 */
	@Test
	public void testInputReplacement2(){
		IDPStructure inputStruc = new IDPStructure();
		Node node = new Node(this.hashNodeIds.get(0), "blue");
		this.nodes.add(node);
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addTrueCollection(this.nodes, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}
		
		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(false)
				.build();
			
			try{
				inputStruc.getCollection(Node.class);
				fail("No exception is thrown when retrieving 2-valued interpretation for"
						+ " Node.class, but was expecting UnavailableCollectionException.class.");
			} catch(UnavailableCollectionException e){
			}
			assertEquals(inputStruc.getTrueCollection(Node.class).size(), 1);
			assertTrue(inputStruc.getTrueCollection(Node.class).contains(node));
			assertEquals(this.nodes.size(), 1);
			assertTrue(this.nodes.contains(node));
			
			solver.modelExpand(inputStruc, 1, true);
			
			assertEquals(inputStruc.getCollection(Node.class).size(), 8);
			assertTrue(inputStruc.getCollection(Node.class).contains(node));
			assertEquals(inputStruc.getTrueCollection(Node.class).size(), 8);
			assertTrue(inputStruc.getTrueCollection(Node.class).contains(node));
			assertEquals(this.nodes.size(), 8);
			assertTrue(this.nodes.contains(node));
			assertTrue(inputStruc.getUnknownCollection(Node.class).isEmpty());
		} catch (IDPException | IOException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		}
	}
	
	/**
	 * Tests replacement of original input with single model expand.
	 */
	@Test
	public void testInputReplacement3(){
		IDPStructure inputStruc = new IDPStructure();
		Node node = new Node(this.hashNodeIds.get(0), "blue");
		this.nodes.add(node);
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addTrueCollection(this.nodes, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}
		
		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(true)
				.build();
			
			try{
				inputStruc.getCollection(Node.class);
				fail("No exception is thrown when retrieving 2-valued interpretation for"
						+ " Node.class, but was expecting UnavailableCollectionException.class.");
			} catch(UnavailableCollectionException e){
			}
			assertEquals(inputStruc.getTrueCollection(Node.class).size(), 1);
			assertTrue(inputStruc.getTrueCollection(Node.class).contains(node));
			assertEquals(this.nodes.size(), 1);
			assertTrue(this.nodes.contains(node));
			
			solver.singleModelExpand(inputStruc);
			
			assertEquals(inputStruc.getCollection(Node.class).size(), 8);
			assertTrue(inputStruc.getCollection(Node.class).contains(node));
			assertEquals(inputStruc.getTrueCollection(Node.class).size(), 8);
			assertTrue(inputStruc.getTrueCollection(Node.class).contains(node));
			assertEquals(this.nodes.size(), 8);
			assertTrue(this.nodes.contains(node));
		} catch (IDPException | IOException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		}
	}
	
	/**
	 * Tests replacement of original input with minimization.
	 */
	@Test
	public void testInputReplacement4(){
		IDPStructure inputStruc = new IDPStructure();
		Node node = new Node(this.hashNodeIds.get(0), "blue");
		this.nodes.add(node);
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addTrueCollection(this.nodes, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}
		
		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(true)
				.build();
			
			try{
				inputStruc.getCollection(Node.class);
				fail("No exception is thrown when retrieving 2-valued interpretation for"
						+ " Node.class, but was expecting UnavailableCollectionException.class.");
			} catch(UnavailableCollectionException e){
			}
			assertEquals(inputStruc.getTrueCollection(Node.class).size(), 1);
			assertTrue(inputStruc.getTrueCollection(Node.class).contains(node));
			assertEquals(this.nodes.size(), 1);
			assertTrue(this.nodes.contains(node));
			
			URL url = getClass().getClassLoader().getResource("_3coloring/minimumBlue.idpterm");
			IDPTerm term = IDPTerm.loadFile(url);
			solver.minimize(inputStruc, term);
			
			assertEquals(inputStruc.getCollection(Node.class).size(), 8);
			assertTrue(inputStruc.getCollection(Node.class).contains(node));
			assertEquals(inputStruc.getTrueCollection(Node.class).size(), 8);
			assertTrue(inputStruc.getTrueCollection(Node.class).contains(node));
			assertEquals(this.nodes.size(), 8);
			assertTrue(this.nodes.contains(node));
			assertTrue(inputStruc.getUnknownCollection(Node.class).isEmpty());
		} catch (IDPException | IOException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		}
	}
	
	/**
	 * Tests replacement of original input, namely when there are missing values.
	 */
	@Test
	public void testInputReplacementValues(){
		IDPStructure inputStruc = new IDPStructure();
		NodeId nodeId = new NodeId();
		this.edges.add(new Edge(this.hashNodeIds.get(3), nodeId));
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addCollection(null, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}

		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(true)
				.build();
		
			assertEquals(this.hashNodeIds.size(), 8);
			assertEquals(inputStruc.getCollection(NodeId.class).size(), 8);
			boolean modelFound = solver.sat(inputStruc);
			assertTrue(modelFound);
			assertEquals(this.hashNodeIds.size(), 8);
			assertEquals(inputStruc.getCollection(NodeId.class).size(), 8);
			
			assertEquals(this.hashNodeIds.size(), 8);
			assertEquals(inputStruc.getCollection(NodeId.class).size(), 8);
			IDPStructure resultingStruc = solver.singleModelExpand(inputStruc);
			assertEquals(resultingStruc.getCollection(NodeId.class).size(), 9);
			assertEquals(inputStruc.getCollection(NodeId.class).size(), 9);
			try{
				inputStruc.getTrueCollection(NodeId.class).size();
				fail("No exception is thrown when retrieving 3-valued interpretation for"
						+ " a type class: NodeId.class, but was expecting "
						+ "UnavailableCollectionException.class.");
			} catch(UnavailableCollectionException e){
			}
			assertEquals(this.hashNodeIds.size(), 9);
		} catch(IDPException | IOException e){
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}
	}
	
	/**
	 * Tests non-replacement of original input, namely when there are missing values.
	 * Replace input is false.
	 */
	@Test
	public void testInputReplacementValues2(){
		IDPStructure inputStruc = new IDPStructure();
		NodeId nodeId = new NodeId();
		this.edges.add(new Edge(this.hashNodeIds.get(3), nodeId));
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addCollection(null, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}

		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(false)
				.build();
		
			assertEquals(this.hashNodeIds.size(), 8);
			assertEquals(inputStruc.getCollection(NodeId.class).size(), 8);
			boolean modelFound = solver.sat(inputStruc);
			assertTrue(modelFound);
			assertEquals(this.hashNodeIds.size(), 8);
			assertEquals(inputStruc.getCollection(NodeId.class).size(), 8);
			
			assertEquals(this.hashNodeIds.size(), 8);
			assertEquals(inputStruc.getCollection(NodeId.class).size(), 8);
			IDPStructure resultingStruc = solver.singleModelExpand(inputStruc);
			assertEquals(resultingStruc.getCollection(NodeId.class).size(), 9);
			assertEquals(inputStruc.getCollection(NodeId.class).size(), 8);
			try{
				inputStruc.getTrueCollection(NodeId.class).size();
				fail("No exception is thrown when retrieving 3-valued interpretation for"
						+ " a type class: NodeId.class, but was expecting "
						+ "UnavailableCollectionException.class.");
			} catch(UnavailableCollectionException e){
			}
			assertEquals(this.hashNodeIds.size(), 8);
		} catch(IDPException | IOException e){
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}
	}
	
	/**
	 * Tests replacement of original input, namely when there are missing types.
	 */
	@Test
	public void testInputReplacementTypes(){
		IDPStructure inputStruc = new IDPStructure();
		try {
			//inputStruc.addCollection(this.hashNodeIds, NodeId.class); // not entered
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addCollection(null, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}

		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(true)
				.build();
			
			try{
				inputStruc.getCollection(NodeId.class);
				fail("No exception is thrown when retrieving an empty type collection with elements of"
						+ " NodeId.class, but was expecting UnavailableCollectionException.class.");
			} catch(UnavailableCollectionException e){
			}
			IDPStructure singleStruc = solver.singleModelExpand(inputStruc);
			assertEquals(singleStruc.getCollection(NodeId.class).size(), 8); // inferred list of node ids in result
			assertEquals(inputStruc.getCollection(NodeId.class).size(), 8);
		} catch (IDPException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		} catch (IOException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		}
	}
	
	/**
	 * Tests non-replacement of original input, namely when there are missing types.
	 * Replace input is false.
	 */
	@Test
	public void testInputReplacementTypes2(){
		IDPStructure inputStruc = new IDPStructure();
		try {
			//inputStruc.addCollection(this.hashNodeIds, NodeId.class); // not entered
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addCollection(null, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}

		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(false)
				.build();
			
			try{
				inputStruc.getCollection(NodeId.class);
				fail("No exception is thrown when retrieving an empty type collection with elements of"
						+ " NodeId.class, but was expecting UnavailableCollectionException.class.");
			} catch(UnavailableCollectionException e){
			}
			IDPStructure singleStruc = solver.singleModelExpand(inputStruc);
			assertEquals(singleStruc.getCollection(NodeId.class).size(), 8); // inferred list of node ids in result
			try{
				inputStruc.getCollection(NodeId.class);
				fail("No exception is thrown when retrieving an empty type collection with elements of"
						+ " NodeId.class, but was expecting UnavailableCollectionException.class.");
			} catch(UnavailableCollectionException e){
			}
		} catch (IDPException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		} catch (IOException e) {
			fail("Did not expect an exception to be thrown. Error msg: " + e.getMessage());
		}
	}
	
	/**
	 * Test the result of entering null in a collection with Integer.class elements.
	 * 
	 * In this case, the integers are contained in a wrapper class: NodeId2.class. The null
	 * references within are mapped to 0 in IDP, but keep the reference to null in Java objects.
	 * In the case of named type collections of integers (no wrapper class),
	 * the null references are mapped to 0. The next test regarding a type collection of 
	 * strings shows a similar case.
	 */
	@Test
	public void testNullIntegerType(){
		IDPStructure inputStruc = new IDPStructure();
		List<NodeId2> nodeIds = new ArrayList<>();
		nodeIds.add(new NodeId2(1));
		nodeIds.add(new NodeId2(2));
		nodeIds.add(new NodeId2(3));
		nodeIds.add(new NodeId2(null));
		Collection<Edge2> edges = new ArrayList<>();
		edges.add(new Edge2(nodeIds.get(0), nodeIds.get(1)));
		edges.add(new Edge2(nodeIds.get(0), nodeIds.get(2)));
		edges.add(new Edge2(nodeIds.get(1), nodeIds.get(3)));
		try {
			inputStruc.addCollection(nodeIds, NodeId2.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(edges, Edge2.class);
			inputStruc.addCollection(null, Node6.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}
		
		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(
					IDPTheory.loadFile(getClass().getClassLoader().
							getResource("_3coloring/ThreeColoring6.idptheory"))
							, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(false)
				.build();
			
			IDPStructure resultStruc = solver.singleModelExpand(inputStruc);
			
			int nullCounter = 0;
			for(NodeId2 n : resultStruc.getCollection(NodeId2.class)){
				if(n.getId() == null)
					nullCounter++;
			}
			assertTrue(nullCounter > 0);
			
			Collection<Node6> resultingNodes = resultStruc.getCollection(Node6.class);
			nullCounter = 0;
			for(Node6 n : resultingNodes){
				if(n.getId().getId() == null){ 
					nullCounter++;
					assertTrue(nodeIds.contains(n.getId())); // The existing type objects are used.
				}
			}
			assertTrue(nullCounter > 0);
		} catch(IDPException | IOException e){
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}
	}
	
	/**
	 * Test the result of entering null in a collection with String.class elements.
	 */
	@Test
	public void testNullStringType(){
		IDPStructure inputStruc = new IDPStructure();
		this.colors.add(null);
		this.nodes.add(new Node(this.hashNodeIds.get(0), "null")); // guarantees one of the nodes to have color "null"
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addTrueCollection(this.nodes, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}
		
		try {
			IDPSolver solver = new IDPSolver.SolverBuilder(this.theory, this.idpConfigPath)
				.nbModels(5)
				.replaceInput(false)
				.build();
			
			assertEquals(inputStruc.getStringCollection("color").size(), 4);
			assertTrue(inputStruc.getStringCollection("color").contains(null));
			IDPStructure resultStruc = solver.singleModelExpand(inputStruc);
			assertTrue(resultStruc.getStringCollection("color").contains(null));
			Collection<Node> resultingNodes = resultStruc.getCollection(Node.class);
			int nullCounter = 0;
			for(Node n : resultingNodes){
				if(n.getColor().equals("null"))
					nullCounter++;
			}
			assertTrue(nullCounter > 0);
		} catch(IDPException | IOException e){
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}
	}
	
	/**
	 * Test the result of entering null in a collection with @Type annotated elements.
	 */
	@Test(expected=InvalidStructureException.class)
	public void testNullObjectType() throws InvalidStructureException{
		IDPStructure inputStruc = new IDPStructure();
		this.hashNodeIds.add(null);
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addCollection(null, Node.class);
		} catch (InvalidAnnotationException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}
	}
	
	/**
	 * Test the result of entering null in a collection with @Predicate annotated elements in
	 * a 2-valued interpretation.
	 */
	@Test(expected=InvalidStructureException.class)
	public void testNullPredicate() throws InvalidStructureException{
		IDPStructure inputStruc = new IDPStructure();
		this.edges.add(null);
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addCollection(null, Node.class);
		} catch (InvalidAnnotationException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}
	}
	
	/**
	 * Test the result of entering null in a collection with @Function annotated elements in
	 * a 3-valued interpretation.
	 */
	@Test(expected=InvalidStructureException.class)
	public void testNullFunction() throws InvalidStructureException{
		IDPStructure inputStruc = new IDPStructure();
		this.nodes.add(null);
		try {
			inputStruc.addCollection(this.hashNodeIds, NodeId.class);
			inputStruc.addTypeCollection(this.colors, "color");
			inputStruc.addCollection(this.edges, Edge.class);
			inputStruc.addTrueCollection(this.nodes, Node.class);
		} catch (InvalidAnnotationException e) {
			fail("Exception thrown when adding collections, but did not expect a problem. "
					+ "Error message was: " + e.getMessage());
		}
	}
}
