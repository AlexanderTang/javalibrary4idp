package examples._3coloring;

import idp.annotations.Type;

@Type
public class NodeId2 {
	private Integer id;
	
	public NodeId2(Integer id){
		this.id = id;
	}
	
	public Integer getId(){
		return this.id;
	}
}