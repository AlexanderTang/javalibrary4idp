package examples._3coloring;

import idp.annotations.ArgumentType;
import idp.annotations.Function;
import idp.annotations.FunctionValue;

@Function
public class Node6 {
	
	@ArgumentType(1)
	private NodeId2 id;
	
	@FunctionValue("color")
	private String color;
	
	public Node6(NodeId2 id, String color) {
		this.id = id;
		this.color = color;
	}
	
	public NodeId2 getId(){
		return this.id;
	}

	public String getColor(){
		return this.color;
	}
}



