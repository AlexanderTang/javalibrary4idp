package examples._3coloring;

import idp.annotations.ArgumentType;
import idp.annotations.Function;
import idp.annotations.FunctionValue;

@Function
public class Node2 {
	
	@ArgumentType(1)
	private NodeId id;
	
	@FunctionValue("color")
	public String color;
	
	public Node2(NodeId id) {
		this.id = id;
	}
	
	public NodeId getId(){
		return this.id;
	}

	public String getColor(){
		return this.color;
	}
}



