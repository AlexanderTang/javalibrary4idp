package examples._3coloring;

import idp.annotations.ArgumentType;
import idp.annotations.Predicate;

@Predicate
public class Edge2 {
	
	@ArgumentType(1)
	private NodeId2 nodeId1;
	
	@ArgumentType(2)
	private NodeId2 nodeId2;
	
	public Edge2(NodeId2 id1, NodeId2 id2){
		this.nodeId1 = id1;
		this.nodeId2 = id2;
	}
}
