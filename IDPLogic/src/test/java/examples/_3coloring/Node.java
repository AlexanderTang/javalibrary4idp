package examples._3coloring;

import idp.annotations.ArgumentType;
import idp.annotations.Function;
import idp.annotations.FunctionValue;

@Function
public class Node {
	
	@ArgumentType(1)
	private NodeId id;
	
	@FunctionValue("color")
	private String color;
	
	public Node(NodeId id, String color) {
		this.id = id;
		this.color = color;
	}
	
	public NodeId getId(){
		return this.id;
	}

	public String getColor(){
		return this.color;
	}
}



