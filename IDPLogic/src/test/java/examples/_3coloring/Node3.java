package examples._3coloring;

import idp.annotations.ArgumentType;
import idp.annotations.Function;
import idp.annotations.FunctionValue;

@Function
public class Node3 {
	
	@ArgumentType(1)
	private NodeId id;
	
	@FunctionValue("color")
	private String color;
	
	public Node3(String color, NodeId id) {
		this.id = id;
		this.color = color;
	}
	
	public NodeId getId(){
		return this.id;
	}

	public String getColor(){
		return this.color;
	}
}



