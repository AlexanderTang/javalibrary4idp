package idp.exceptions;

/**
 * <p>This exception is thrown when the conditions for the IDP annotations have not been 
 * fulfilled. Some examples include: {@literal @Predicate} and {@literal @Function} are specified for the same class, 
 * a {@literal @Function} annotated class does not have a {@literal @FunctionValue} annotated field, ...</p>
 * 
 * @author Alexander Tang
 *
 */
public class InvalidAnnotationException extends IDPException {

	private static final long serialVersionUID = 1L;

	public InvalidAnnotationException(String message){
		super(message);
	}
}
