package idp.exceptions;

/**
 * <p>This exception is thrown when the collections that are added to the IDP structure are invalid.
 * This happens for example when a collection of predicate or function instances are added as both
 * a 2-valued and a 3-valued interpretation, or a predicate class name and a type collection name
 * are the same.</p>
 * 
 * @author Alexander Tang
 *
 */
public class InvalidStructureException extends IDPException {

	private static final long serialVersionUID = 1L;

	public InvalidStructureException(String message){
		super(message);
	}
}
