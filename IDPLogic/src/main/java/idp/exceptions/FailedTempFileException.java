package idp.exceptions;

/**
 * <p>This exception is thrown when the creation of the temporary .idp file has failed. This
 * happens when an IOException occurred when accessing and writing to the .idp file.</p>
 * 
 * @author Alexander Tang
 *
 */
public class FailedTempFileException extends IDPException {

	private static final long serialVersionUID = 1L;

	public FailedTempFileException(String message){
		super(message);
	}
}
