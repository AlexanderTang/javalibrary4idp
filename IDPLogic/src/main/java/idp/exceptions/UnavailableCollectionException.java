package idp.exceptions;

/**
 * <p>This exception is thrown when a collection to be retrieved does not exist. Ie. if a 
 * retrieval for a 2-valued interpretation is requested for a predicate, but it is a 
 * 3-valued interpretation (there are 'unknown' elements), then this exception will 
 * be thrown.</p>
 * 
 * @author Alexander Tang
 *
 */
public class UnavailableCollectionException extends IDPException {

	private static final long serialVersionUID = 1L;

	public UnavailableCollectionException(String message){
		super(message);
	}
}
