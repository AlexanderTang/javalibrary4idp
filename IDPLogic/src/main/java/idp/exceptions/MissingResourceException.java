package idp.exceptions;

/**
 * <p>This exception is thrown a resource could not be loaded properly. E.g. the path of theory 
 * or idp config file are incorrect.</p>
 * 
 * @author Alexander Tang
 *
 */
public class MissingResourceException extends IDPException {

	private static final long serialVersionUID = 1L;

	public MissingResourceException(String message){
		super(message);
	}
}
