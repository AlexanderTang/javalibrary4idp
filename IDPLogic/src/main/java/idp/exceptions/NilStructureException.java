package idp.exceptions;

/**
 * <p>This exception is thrown when IDP does not find any models that satisfy the given theory.</p>
 * 
 * @author Alexander Tang
 *
 */
public class NilStructureException extends IDPException {

	private static final long serialVersionUID = 1L;
	
	public NilStructureException(){
		super("No models satisfy the given theory.");
	}

	public NilStructureException(String message){
		super(message);
	}
}
