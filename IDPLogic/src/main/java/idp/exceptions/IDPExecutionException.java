package idp.exceptions;

/**
 * <p>This exception is thrown when the execution of IDP fails or when 
 * the results could not be parsed. Reasons this may occur include: time out of IDP, results are
 * empty even though output is expected, unexpected output is received, ...</p>
 * 
 * @author Alexander Tang
 *
 */
public class IDPExecutionException extends IDPException {

	private static final long serialVersionUID = 1L;

	public IDPExecutionException(String message){
		super(message);
	}
}
