package idp.exceptions;

/**
 * <p>This exception is thrown when a {@literal @Predicate} or {@literal @Function} annotated class does not have a 
 * constructor of the right format. A constructor of the format (ArgType1, ArgType2,...,ArgTypeN) is
 * required for a {@literal @Predicate} annotated class, where ArgType1 corresponds with 
 * {@literal @ArgumentType(1)}. The ordering of the argument types denotes their ordering in the constructor. 
 * When two argument types have the same class, this exception will not be thrown when they are 
 * switched, but it will likely result in a wrong result.</p> 
 * 
 * <p>For a {@literal @Function} annotated class
 * an extra parameter for the {@literal @FunctionValue} needs to be appended at the end of the
 * constructor (after all the argument types). There needs to be a constructor that contains only
 * these specified parameters.</p>
 * 
 * @author Alexander Tang
 *
 */
public class InvalidConstructorException extends IDPException {

	private static final long serialVersionUID = 1L;

	public InvalidConstructorException(String message){
		super(message);
	}
}
