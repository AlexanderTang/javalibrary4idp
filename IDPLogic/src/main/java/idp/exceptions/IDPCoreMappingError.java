package idp.exceptions;

/**
 * <p>This error is thrown when the IDP mapper is not functioning correctly. 
 * This error class is mainly used to catch and trace bugs in the system that have not 
 * been detected in the JUnit tests and the examples. 
 * Ideally, this error is never thrown.</p>
 * 
 * @author Alexander Tang
 *
 */
public class IDPCoreMappingError extends Error {

	private static final long serialVersionUID = 1L;

	public IDPCoreMappingError(String message){
		super(message);
	}
}
