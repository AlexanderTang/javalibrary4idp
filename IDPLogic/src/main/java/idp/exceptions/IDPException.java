package idp.exceptions;

/**
 * <p>This exception is the super class of all exceptions in the idp.exceptions package, excluding 
 * {@link IDPCoreMappingError}.</p>
 * 
 * @author Alexander Tang
 *
 */
public class IDPException extends Exception {

	private static final long serialVersionUID = 1L;

	public IDPException(String message){
		super(message);
	}
}
