package idp.elements;

import idp.annotations.TypeValue;
import idp.exceptions.IDPCoreMappingError;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class IDPTypeInteger extends IDPType {
	private Set<Integer> instantiatedTypeIntegers;

	public IDPTypeInteger(Class<?> cl, Collection<?> col, String elementName) {
		super(cl, col, elementName);
	}
	
	public IDPTypeInteger(Collection<Integer> col, String elementName){
		super(Integer.class, null, elementName);
		this.instantiatedTypeIntegers = new HashSet<>(col);
	}
	
	public Set<Integer> getInstantiatedTypeIntegers(){
		return this.instantiatedTypeIntegers;
	}

	@Override
	public IDPTypeID getType() {
		return IDPTypeID.INTEGER;
	}

	@Override
	public String getTypeStringForm(){
		return "int";
	}

	@Override
	public Set<String> getStringRepresentationValues() 
			throws IDPCoreMappingError {
		if(getElementClass() == Integer.class){
			if(this.instantiatedTypeIntegers.isEmpty())	return new HashSet<>();
			Set<String> typeValues = new HashSet<>();
			for(Integer i : this.instantiatedTypeIntegers){
				if(i != null)
					typeValues.add(String.valueOf(i));
				else typeValues.add("0");
			}
			return typeValues;
		} else{
			if(this.instantiatedTypeObjects.isEmpty())	return new HashSet<>();
			else{
				Set<String> typeValues = new HashSet<>();
				for(Field f : getElementClass().getDeclaredFields()){
					if(f.isAnnotationPresent(TypeValue.class)){
						f.setAccessible(true);
						for(Object obj : this.instantiatedTypeObjects){
							try{
								Integer i = (Integer) f.get(obj);
								if(i == null)
									typeValues.add("0");
								else typeValues.add(String.valueOf(i));
							} catch(IllegalAccessException e){
								throw new IDPCoreMappingError("An unexpected error occurred when casting attribute objects "
										+ "to their expected class. The error happened due to an error in the core implementation. "
										+ "The relevant error message that occurred: " + e.getMessage());
							}
						}
						return typeValues;
					}
				}
				// no @TypeValue annotated attributes have been found, so use object hash code
				for(Object obj : this.instantiatedTypeObjects)
					typeValues.add(String.valueOf(obj.hashCode()));
				return typeValues;
			}
		}
	}

	@Override
	public Object getTypeInstance(String value) throws IDPCoreMappingError{
		if(getStringRepresentationValues().contains(value)){
			for(Field f : getElementClass().getDeclaredFields()){
				if(f.isAnnotationPresent(TypeValue.class)){
					f.setAccessible(true);
					for(Object obj : this.instantiatedTypeObjects){
						if(obj != null){
							try{
								if(String.valueOf((Integer) f.get(obj)).equals(value))
									return obj;
							} catch(IllegalAccessException e){
								throw new IDPCoreMappingError("An unexpected error occurred when casting attribute objects "
										+ "to their expected class. The error happened due to an error in the core implementation. "
										+ "The relevant error message that occurred: " + e.getMessage());
							}
						}
					}
				}
			}
			// no @TypeValue annotated attributes have been found, so use object hash code
			for(Object obj : this.instantiatedTypeObjects){
				if(String.valueOf(obj.hashCode()).equals(value))
					return obj;
			}
		}
		throw new IDPCoreMappingError("The type with value " + value + " could not be found. IDP"
				+ " does not create new types, so some information got lost during mapping. Error occurred for "
				+ "class " + getElementClass().getName() + ".");
	}
}
