package idp.elements;

import idp.annotations.ArgumentType;
import idp.annotations.TypeValue;
import idp.exceptions.IDPCoreMappingError;
import idp.exceptions.InvalidAnnotationException;
import idp.exceptions.InvalidConstructorException;
import idp.logic.AnnotationChecker;
import idp.logic.IDPMapper;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class IDPPredicate extends IDPElement {
	protected Set<Object> instantiatedObjects;
	protected Set<Object> instantiatedTrueObjects;
	protected Set<Object> instantiatedFalseObjects;
	protected Set<Object> instantiatedUnknownObjects;
	protected List<String> argumentTypeNames;
	
	public IDPPredicate(Class<?> cl, Collection<?> col, String elementName) 
			throws InvalidAnnotationException {
		this(cl, col, null, elementName, false);
	}

	public IDPPredicate(Class<?> cl, Collection<?> col, ThreeValuedIndicator tvi, String elementName) 
			throws InvalidAnnotationException{
		this(cl, col, tvi, elementName, false);
	}
	
	protected IDPPredicate(Class<?> cl, Collection<?> col, ThreeValuedIndicator tvi, String elementName, boolean function) 
			throws InvalidAnnotationException{
		super(cl, elementName);

		if(col != null){
			if(tvi == null){
				if(this.instantiatedObjects == null)
					this.instantiatedObjects = new HashSet<>();
				this.instantiatedObjects.addAll(col);
			}
			else addThreeValuedCollection(col, tvi);
		}
		
		if(!function)	validatePredicateClass(cl);
		if(!validArgumentTypeOrdering(cl))
			throw new InvalidAnnotationException("The @ArgumentType annotated attributes of an @Predicate or"
					+ " @Function annotated class should have a valid ordering. Error "
					+ "occurred in this class: " + cl.getName() + ".");
		initiateArgumentTypeNames(cl);
	}
	
	public void addThreeValuedCollection(Collection<?> col, ThreeValuedIndicator tvi){
		if(tvi == ThreeValuedIndicator.CT){
			if(this.instantiatedTrueObjects == null)
				this.instantiatedTrueObjects = new HashSet<>();
			this.instantiatedTrueObjects.addAll(col);
		} else if(tvi == ThreeValuedIndicator.CF){
			if(this.instantiatedFalseObjects == null)
				this.instantiatedFalseObjects = new HashSet<>();
			this.instantiatedFalseObjects.addAll(col);
		} else{
			if(this.instantiatedUnknownObjects == null)
				this.instantiatedUnknownObjects = new HashSet<>();
			this.instantiatedUnknownObjects.addAll(col);
		}
	}

	public Set<Object> getInstantiatedObjects(){
		return this.instantiatedObjects;
	}
	
	public Set<Object> getInstantiatedTrueObjects(){
		return this.instantiatedTrueObjects;
	}
	
	public Set<Object> getInstantiatedFalseObjects(){
		return this.instantiatedFalseObjects;
	}
	
	public Set<Object> getInstantiatedUnknownObjects(){
		return this.instantiatedUnknownObjects;
	}
	
	public List<String> getArgumentTypeNames(){
		return Collections.unmodifiableList(this.argumentTypeNames);
	}
	
	private void validatePredicateClass(Class<?> cl) throws InvalidAnnotationException{
		if(!AnnotationChecker.hasPredicateAnnotation(cl))
			throw new InvalidAnnotationException("Only @Predicate annotated classes are accepted for the IDPPredicate class. Error "
					+ "occurred in this class: " + cl.getName() + ".");
	}
	
	/**
	 * Finds all the @ArgumentType annotated attributes of the class argument. This method returns
	 * true if the annotations have a valid ordering. For example: three @ArgumentType annotations could
	 * have @ArgumentType(1), @ArgumentType(value=2, typeName="myCollectionName"), @ArgumentType(3).
	 * When the numbers don't have a logical, sequential ordering, this method will return false. This means
	 * that the numbers 1,2,4 are not allowed (because 3 is missing) and 1,2,2 are not allowed (because
	 * two attributes use the number 2).
	 * 
	 * @param cl	The class to search @ArgumentType annotated attributes over.
	 * @return	Returns true if the @ArgumentType annotated attributes have a valid ordering.
	 */
	protected boolean validArgumentTypeOrdering(Class<?> cl){
		List<Integer> fieldOrderNumbers = new ArrayList<Integer>();
		for(Field f : cl.getDeclaredFields()){
			if(f.isAnnotationPresent(ArgumentType.class)){
				int orderNumber = f.getAnnotation(ArgumentType.class).value();
				if(!fieldOrderNumbers.contains(orderNumber))
					fieldOrderNumbers.add(orderNumber);
				else return false;
			}
		}
		for(int i = 1; i <= fieldOrderNumbers.size(); i++){
			if(!fieldOrderNumbers.contains(i))
				return false;
		}
		return true;
	}
	
	protected void initiateArgumentTypeNames(Class<?> cl) throws InvalidAnnotationException {
		Map<Integer,String> orderTypeNameMap = new HashMap<>();
		for(Field f : cl.getDeclaredFields()){
			if(f.isAnnotationPresent(ArgumentType.class)){
				int orderNumber = f.getAnnotation(ArgumentType.class).value();
				if(int.class.equals(f.getType()) || String.class.equals(f.getType())){
					orderTypeNameMap.put(orderNumber, f.getAnnotation(ArgumentType.class).typeName());
				} else if(f.getType() instanceof Object){
					orderTypeNameMap.put(orderNumber, f.getType().getSimpleName());
				} else throw new InvalidAnnotationException("Only objects, String and int datatypes may be "
						+ "annotated with @ArgumentType. Error occurred in class " + cl.getName() + " at"
						+ " attribute " + f.getName() + ".");
			}
		}
		
		List<String> argumentTypeNames = new ArrayList<String>();
		for(int i = 1; i <= orderTypeNameMap.keySet().size(); i++){
			argumentTypeNames.add(orderTypeNameMap.get(i));
		}
		this.argumentTypeNames = argumentTypeNames;
	}

	@Override
	public String getVocabularyString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.elementName + "(");
		boolean first = true;
		for(String argumentName : getArgumentTypeNames()){
			if(first){
				sb.append(argumentName);
				first = false;
			}
			else sb.append("," + argumentName);
		}
		sb.append(")");
		return sb.toString();
	}

	@Override
	public String getStructureString() throws InvalidAnnotationException, IDPCoreMappingError{
		StringBuilder sb = new StringBuilder();
		if(this.instantiatedObjects != null){
			sb.append(structureBuilder(elementBuilder(this.instantiatedObjects)));
			sb.append("\n");
		}
		if(this.instantiatedTrueObjects != null){
			sb.append(structureBuilder(elementBuilder(this.instantiatedTrueObjects), 
					ThreeValuedIndicator.CT));
			sb.append("\n");
		}
		if(this.instantiatedFalseObjects != null){
			sb.append(structureBuilder(elementBuilder(this.instantiatedFalseObjects), 
					ThreeValuedIndicator.CF));
			sb.append("\n");
		}
		if(this.instantiatedUnknownObjects != null)
			sb.append(structureBuilder(elementBuilder(this.instantiatedUnknownObjects), 
					ThreeValuedIndicator.U));
		return sb.toString();
	}
	
	protected List<String> elementBuilder(Collection<Object> objects) 
			throws InvalidAnnotationException, IDPCoreMappingError{
		List<Object> orderedObjects = new ArrayList<>(objects);
		String nullString = null;
		orderedObjects.removeAll(Arrays.asList(nullString));
		return elementBuilderBody(orderedObjects);
	}
	
	protected List<String> elementBuilderBody(List<Object> orderedObjects) throws
			InvalidAnnotationException, IDPCoreMappingError{
		Map<Integer,List<String>> orderValueMap = new HashMap<>();
		for(Field f : getElementClass().getDeclaredFields()){
			if(f.isAnnotationPresent(ArgumentType.class)){
				int orderNumber = f.getAnnotation(ArgumentType.class).value();
				List<String> tempValueList = getFieldValueList(orderedObjects, f);
				orderValueMap.put(orderNumber, tempValueList);
			}
		}
		
		List<String> orderedValues = new ArrayList<String>();
		if(orderValueMap.keySet().size() == 0)	return orderedValues;
		for(int i = 0; i < orderedObjects.size(); i++){
			StringBuilder sb = new StringBuilder();
			boolean first = true;
			for(int j = 1; j <= orderValueMap.keySet().size(); j++){
				if(first){
					sb.append(orderValueMap.get(j).get(i));
					first = false;
				}
				else sb.append("," + orderValueMap.get(j).get(i));
			}
			orderedValues.add(sb.toString());
		}
		return orderedValues;
	}
	
	protected List<String> getFieldValueList(List<Object> orderedObjects, Field f) 
			throws IDPCoreMappingError{
		try{
			f.setAccessible(true);
			List<String> tempValueList = new ArrayList<>();
			if(int.class.equals(f.getType()) || Integer.class.equals(f.getType())){
				for(Object obj : orderedObjects)
					tempValueList.add(String.valueOf((Integer) f.get(obj)));
			} else if(String.class.equals(f.getType())){
				for(Object obj : orderedObjects)
					tempValueList.add((String) f.get(obj));
			} else if(f.getType() instanceof Object){
				boolean fieldFound = false;
				for(Field f2 : f.getType().getDeclaredFields()){
					if(f2.isAnnotationPresent(TypeValue.class)){
						f2.setAccessible(true);
						fieldFound = true;
						if(int.class.equals(f2.getType()) || Integer.class.equals(f2.getType())){
							for(Object obj : orderedObjects)
								tempValueList.add(String.valueOf((Integer) f2.get(f.get(obj))));
							break;
						} else if(String.class.equals(f2.getType())){
							for(Object obj : orderedObjects)
								tempValueList.add((String) f2.get(f.get(obj)));
							break;
						}
					}
				}
				if(!fieldFound){
					// no @TypeValue annotated attribute has been found, so use hash code
					for(Object obj : orderedObjects)
						tempValueList.add(String.valueOf(f.get(obj).hashCode()));
				}
			}
			return tempValueList;
		} catch(IllegalAccessException e){
			throw new IDPCoreMappingError("An unexpected error occurred when casting attribute objects "
					+ "to their expected class. The error happened due to an error in the core implementation. "
					+ "The relevant error message that occurred: " + e.getMessage());
		}
	}
	
	public Object getInstance(List<String> values, IDPMapper mapper, Collection<Object> instances) 
			throws InvalidConstructorException{
		Object obj = getExistingInstance(values, instances);
		if(obj != null)
			return obj;
		else{
			try{
				Class<?>[] classArray = new Class<?>[getArgumentTypeNames().size()];
				fillClassArrayArguments(classArray, mapper);	
				Object[] objectArray = new Object[classArray.length];
				fillArrayArguments(objectArray, classArray, values, mapper);
				Constructor<?> constructor = getConstructor(getElementClass(), classArray);
				if(constructor == null)
					throw new InvalidConstructorException("Could not find a valid constructor for " +
							this.getElementName() + ". Make sure there exists a constructor with only the " +
						"argument types (in order).");
				return constructor.newInstance(objectArray);
			} catch(InstantiationException | IllegalAccessException 
					| IllegalArgumentException | InvocationTargetException e){
				e.printStackTrace();
				throw new IDPCoreMappingError("A valid constructor has been found to create new instances,"
						+ " but the instance creation still failed. This is likely due to an implementation"
						+ " error in the IDP mapper. The error message was: " + e.getMessage());
			}
		}
	}
	
	protected Constructor<?> getConstructor(Class<?> cl, Class<?>[] classArray){
		for(Constructor<?> constr : cl.getConstructors()){
			Class<?>[] parameters = constr.getParameterTypes();
			if(parameters.length != classArray.length)	continue;
			boolean constructorFound = true;
			for(int i = 0; i < parameters.length; i++){
				if(parameters[i] != classArray[i]){
					if((parameters[i] != int.class && parameters[i] != Integer.class) ||
							(classArray[i] != int.class && classArray[i] != Integer.class)){
						constructorFound = false;
						break;
					}
				}
			}
			if(constructorFound)
				return constr;
		}
		return null;
	}
	
	protected void fillClassArrayArguments(Class<?>[] classArray, IDPMapper mapper){
		String argumentName;
		for(int i = 0; i < getArgumentTypeNames().size(); i++){
			argumentName = getArgumentTypeNames().get(i);
			classArray[i] = mapper.getTypeMap().get(argumentName).getElementClass();
		}
	}
	
	protected void fillArrayArguments(Object[] objectArray, Class<?>[] classArray, 
			List<String> values, IDPMapper mapper) {
		String argumentName;
		for(int i = 0; i < values.size(); i++){
			if(classArray[i] == String.class)
				objectArray[i] = values.get(i);
			else if(classArray[i] == Integer.class)
				objectArray[i] = Integer.valueOf(values.get(i));
			else{
				argumentName = getArgumentTypeNames().get(i);
				objectArray[i] = mapper.getTypeMap().get(argumentName).getTypeInstance(values.get(i));
			}
		}
	}
	
	private Object getExistingInstance(List<String> values, Collection<Object> instances) 
			throws IDPCoreMappingError{
		Collection<Object> objects = new HashSet<>();
		objects.addAll(instances);
		objects.addAll(getAllObjects());
		for(Object obj : objects){
			if(matchingArguments(obj, values))
				return obj;
		}
		return null;
	}
	
	protected Set<Object> getAllObjects(){
		Set<Object> allObjects = new HashSet<>();
		if(getInstantiatedObjects() != null)
			allObjects.addAll(getInstantiatedObjects());
		if(getInstantiatedTrueObjects() != null)
			allObjects.addAll(getInstantiatedTrueObjects());
		if(getInstantiatedFalseObjects() != null)
			allObjects.addAll(getInstantiatedFalseObjects());
		if(getInstantiatedUnknownObjects() != null)
			allObjects.addAll(getInstantiatedUnknownObjects());
		return allObjects;
	}
		
	protected boolean matchingArguments(Object obj, List<String> values) 
			throws IDPCoreMappingError{
		try{
			for(Field f : getElementClass().getDeclaredFields()){
				if(f.isAnnotationPresent(ArgumentType.class)){
					int orderNumber = f.getAnnotation(ArgumentType.class).value();
					if(mismatchFound(f, obj, values.get(orderNumber-1)))
						return false;
				}
			}
			return true;
		} catch(IllegalAccessException e){
			throw new IDPCoreMappingError("Error occurred when trying to match predicate "
					+ "arguments with a predicate object. Error occurred for the class "
					+ getElementClass() + ".");
		}
	}
	
	protected boolean mismatchFound(Field f, Object obj, String value) 
			throws IllegalArgumentException, IllegalAccessException{
		f.setAccessible(true);
		if(int.class.equals(f.getType())){
			if(!String.valueOf((Integer) f.get(obj)).equals(value))
				return true;
		} else if(String.class.equals(f.getType())){
			if(!((String) f.get(obj)).equals(value))
				return true;
		} else if(f.getType() instanceof Object){
			boolean fieldFound = false;
			for(Field f2 : f.getClass().getDeclaredFields()){
				if(f2.isAnnotationPresent(TypeValue.class)){
					f2.setAccessible(true);
					fieldFound = true;
					if(int.class.equals(f2.getType())){
						if(!String.valueOf((Integer) f2.get(obj)).equals(value))
							return true;
						break;
					} else if(String.class.equals(f2.getType())){
						if(!((String) f2.get(obj)).equals(value))
							return true;
						break;
					}
				}
			}
			if(!fieldFound){
				// no @TypeValue annotated attribute has been found, so use hash code
				if(!String.valueOf(f.get(obj).hashCode()).equals(value))
					return true;
			}
		}
		return false;
	}
}
