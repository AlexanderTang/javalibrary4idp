package idp.elements;

import idp.annotations.TypeValue;
import idp.exceptions.IDPCoreMappingError;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class IDPTypeString extends IDPType {
	private Set<String> instantiatedTypeStrings;

	public IDPTypeString(Class<?> cl, Collection<?> col, String elementName) {
		super(cl, col, elementName);
	}
	
	public IDPTypeString(Collection<String> col, String elementName){
		super(String.class, null, elementName);
		this.instantiatedTypeStrings = new HashSet<>(col);
	}
	
	public Set<String> getInstantiatedTypeStrings(){
		return this.instantiatedTypeStrings;
	}

	@Override
	public IDPTypeID getType() {
		return IDPTypeID.STRING;
	}

	public String getTypeStringForm(){
		return "string";
	}

	@Override
	public Set<String> getStringRepresentationValues() 
			throws IDPCoreMappingError{
		if(getElementClass() == String.class){
			if(this.instantiatedTypeStrings.isEmpty())	return new HashSet<>();
			Set<String> typeValues = new HashSet<>();
			for(String s : this.instantiatedTypeStrings){
				if(s != null)
					typeValues.add(s);
				else typeValues.add("null");
			}
			return typeValues;
		} else{
			if(this.instantiatedTypeObjects.isEmpty())	return new HashSet<>();
			else{
				Set<String> typeValues = new HashSet<>();
				for(Field f : getElementClass().getDeclaredFields()){
					if(f.isAnnotationPresent(TypeValue.class)){
						f.setAccessible(true);
						for(Object obj : this.instantiatedTypeObjects){
							try{
								String s = (String) f.get(obj);
								if(s == null)
									typeValues.add("null");
								else typeValues.add(s);
							} catch(IllegalAccessException e){
								throw new IDPCoreMappingError("An unexpected error occurred when casting attribute objects "
										+ "to their expected class. The error happened due to an error in the core implementation. "
										+ "The relevant error message that occurred: " + e.getMessage());
							}
						}
						return typeValues;
					}
				}
				throw new IDPCoreMappingError("The class " + this.elementName + " using a hashcode has"
						+ " been incorrectly categorized as a string subclass in IDP mapping.");
			}
		}
	}

	@Override
	public Object getTypeInstance(String value) throws IDPCoreMappingError {
		if(getStringRepresentationValues().contains(value)){
			for(Field f : getElementClass().getDeclaredFields()){
				if(f.isAnnotationPresent(TypeValue.class)){
					f.setAccessible(true);
					for(Object obj : this.instantiatedTypeObjects){
						if(obj != null){
							try{
								if(((String) f.get(obj)).equals(value))
									return obj;
							} catch(IllegalAccessException e){
								throw new IDPCoreMappingError("An unexpected error occurred when casting attribute objects "
										+ "to their expected class. The error happened due to an error in the core implementation. "
										+ "The relevant error message that occurred: " + e.getMessage());
							}
						}
					}
				}
			}
		}
		throw new IDPCoreMappingError("The type with value " + value + " could not be found. IDP"
				+ " does not create new types, so some information got lost during mapping. Error occurred for "
				+ "class " + getElementClass().getName());
	}
}
