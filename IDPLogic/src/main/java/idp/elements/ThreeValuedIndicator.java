package idp.elements;

/**
 * This enum class is used to denote the set {certainly true (CT), certainly false (CF), unknown (U)} 
 * of the three-valued interpretations in IDP.
 * 
 * @author Alexander Tang
 *
 */
public enum ThreeValuedIndicator {
	CT, CF, U;
}
