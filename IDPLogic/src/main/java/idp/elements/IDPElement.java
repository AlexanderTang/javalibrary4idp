package idp.elements;

import idp.exceptions.IDPCoreMappingError;
import idp.exceptions.InvalidAnnotationException;

import java.util.Collection;

public abstract class IDPElement {
	protected String elementName;
	protected Class<?> cl;
	
	public IDPElement(Class<?> cl, String elementName){
		this.elementName = elementName;
		this.cl = cl;
	}
	
	public Class<?> getElementClass(){
		return this.cl;
	}
	
	public String getElementName(){
		return this.elementName;
	}
	
	public abstract String getVocabularyString();
	
	public abstract String getStructureString() 
			throws InvalidAnnotationException, IDPCoreMappingError;
	
	protected String structureBuilder(Collection<String> elements){
		return structureBuilder(elements, null);
	}
	
	protected String structureBuilder(Collection<String> elements, ThreeValuedIndicator tvi){
		StringBuilder sb = new StringBuilder();
		sb.append(this.elementName);
		if(tvi == ThreeValuedIndicator.CT)
			sb.append("<ct>");
		else if(tvi == ThreeValuedIndicator.CF)
			sb.append("<cf>");
		else if(tvi == ThreeValuedIndicator.U)
			sb.append("<u>");
		sb.append(" = {");
		boolean first = true;
		for(String s : elements){
			if(first){
				sb.append(s);
				first = false;
			}
			else sb.append(";" + s);
		}
		sb.append("}");
		return sb.toString();
	}
}
