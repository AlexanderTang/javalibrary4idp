package idp.elements;

import idp.annotations.FunctionValue;
import idp.exceptions.IDPCoreMappingError;
import idp.exceptions.InvalidAnnotationException;
import idp.exceptions.InvalidConstructorException;
import idp.logic.AnnotationChecker;
import idp.logic.IDPMapper;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class IDPFunction extends IDPPredicate{
	private String functionValueTypeName;

	public IDPFunction(Class<?> cl, Collection<?> col, String elementName) 
			throws InvalidAnnotationException{
		this(cl, col, null, elementName);
	}
	
	public IDPFunction(Class<?> cl, Collection<?> col, ThreeValuedIndicator tvi, String elementName) 
			throws InvalidAnnotationException {
		super(cl, col, tvi, elementName, true);
		validateFunctionClass(cl);
		initiateFunctionValueTypeName(cl);
	}

	public String getFunctionValueTypeName(){
		return this.functionValueTypeName;
	}
	
	protected void validateFunctionClass(Class<?> cl) throws InvalidAnnotationException{
		if(!AnnotationChecker.hasFunctionAnnotation(cl))
			throw new InvalidAnnotationException("Only @Function annotated classes are accepted for the IDPFunction class. Error "
					+ "occurred in this class: " + cl.getName() + ".");
	}
	
	private void initiateFunctionValueTypeName(Class<?> cl) 
			throws InvalidAnnotationException {
		for(Field f : cl.getDeclaredFields()){
			if(f.isAnnotationPresent(FunctionValue.class)){
				if(int.class.equals(f.getType()) || String.class.equals(f.getType())){
					this.functionValueTypeName = f.getAnnotation(FunctionValue.class).value();
					return;
				} else if(f.getType() instanceof Object){
					this.functionValueTypeName = f.getType().getSimpleName();
					return;
				} else throw new InvalidAnnotationException("Only objects, String and int datatypes may be "
						+ "annotated with @FunctionValue. Error occurred in class " + cl.getName() + " at"
						+ " attribute " + f.getName() + ".");
			}
		}
		throw new InvalidAnnotationException("A function value is missing for class " + cl.getName());
	}
	
	@Override
	public String getVocabularyString() {
		return super.getVocabularyString() + " : " + this.functionValueTypeName;
	}
	
	@Override
	protected List<String> elementBuilderBody(List<Object> orderedObjects) 
			throws InvalidAnnotationException, IDPCoreMappingError {
		List<String> baseElements = super.elementBuilderBody(orderedObjects);
		List<String> tempValueList = null;
		for(Field f : getElementClass().getDeclaredFields()){
			if(f.isAnnotationPresent(FunctionValue.class)){
				tempValueList = getFieldValueList(orderedObjects, f);
				break;
			}
		}
		if(tempValueList == null || baseElements.size() != tempValueList.size())
			throw new InvalidAnnotationException("Not all functions have an annotated @FunctionValue attribute.\n"
					+ "Error occurred for the class: " + getElementClass() + ".");
		List<String> concatenatedList = new ArrayList<>();
		for(int i = 0; i < baseElements.size(); i++)
			concatenatedList.add(baseElements.get(i) + "->" + tempValueList.get(i));
		return concatenatedList;
	}
	
	public Object getInstance(List<String> values, String functionValue, IDPMapper mapper,
			Collection<Object> instances)
					throws InvalidConstructorException{
		Object obj = getExistingInstance(values, functionValue, instances);
		if(obj != null)
			return obj;
		else{
			try{
				Class<?>[] classArray = new Class<?>[getArgumentTypeNames().size()+1];
				fillClassArrayArguments(classArray, mapper);
				classArray[classArray.length-1] = 
						mapper.getTypeMap().get(getFunctionValueTypeName()).getElementClass();
				Object[] objectArray = new Object[classArray.length];
				fillArrayArguments(objectArray, classArray, values, mapper);
				addArrayFunctionValue(classArray, objectArray, mapper, functionValue);
				Constructor<?> constructor = getConstructor(getElementClass(), classArray);
				if(constructor == null)
					throw new InvalidConstructorException("Could not find a valid constructor for " +
							this.getElementName() + ". Make sure there exists a constructor with only the " +
						"argument types (in order) and the function value as the last argument.");
				return constructor.newInstance(objectArray);
			} catch(InvocationTargetException | InstantiationException | 
					IllegalAccessException | IllegalArgumentException e){
				throw new IDPCoreMappingError("A valid constructor has been found to create new instances,"
						+ " but the instance creation still failed. This is likely due to an implementation"
						+ " error in the IDP mapper. The error message was: " + e.getMessage());
			}
		}
	}
	
	private void addArrayFunctionValue(Class<?>[] classArray, Object[] objectArray, 
			IDPMapper mapper, String functionValue) 
					throws IDPCoreMappingError{
		if(classArray[classArray.length-1] == String.class)
			objectArray[objectArray.length-1] = functionValue;
		else if(classArray[classArray.length-1] == Integer.class)
			objectArray[objectArray.length-1] = Integer.valueOf(functionValue);
		else{
			String argumentName = getFunctionValueTypeName();
			objectArray[objectArray.length-1] = 
					mapper.getTypeMap().get(argumentName).getTypeInstance(functionValue);
		}
	}
	
	private Object getExistingInstance(List<String> values, String functionValue,
			Collection<Object> instances)
					throws IDPCoreMappingError{
		Collection<Object> objects = new HashSet<>();
		objects.addAll(instances);
		objects.addAll(getAllObjects());
		for(Object obj : objects){
			if(matchingArguments(obj, values) && matchingFunctionValue(obj, functionValue))
				return obj;
		}
		return null;
	}
	
	private boolean matchingFunctionValue(Object obj, String functionValue) 
			throws IDPCoreMappingError{
		try{
			for(Field f : getElementClass().getDeclaredFields()){
				if(f.isAnnotationPresent(FunctionValue.class)){
					if(mismatchFound(f, obj, functionValue))
						return false;
				}
			}
			return true;
		} catch(IllegalAccessException e){
			throw new IDPCoreMappingError("Error occurred when trying to match predicate "
					+ "arguments with a predicate object.\nThe error was " + e.getMessage() + ".");
		}
	}
}
