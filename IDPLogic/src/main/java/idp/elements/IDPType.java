package idp.elements;

import idp.exceptions.IDPCoreMappingError;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public abstract class IDPType extends IDPElement {
	protected Set<Object> instantiatedTypeObjects;
	
	public IDPType(Class<?> cl, Collection<?> col, String elementName) {
		super(cl, elementName);
		if(col != null)	this.instantiatedTypeObjects = new HashSet<>(col);
		else	this.instantiatedTypeObjects = null;
	}

	public abstract IDPTypeID getType();
	
	public Set<Object> getInstantiatedTypeObjects(){
		return this.instantiatedTypeObjects;
	}

	@Override
	public String getVocabularyString(){
		return "type " + this.elementName + " isa " + getTypeStringForm();
	}
	
	public abstract String getTypeStringForm();
	
	@Override
	public String getStructureString() 
			throws IDPCoreMappingError {
		Set<String> typeValues = getStringRepresentationValues();
		if(typeValues.isEmpty())	return "";
		else return structureBuilder(typeValues);
	}
	
	public abstract Set<String> getStringRepresentationValues() 
			throws IDPCoreMappingError;
	
	public abstract Object getTypeInstance(String value) 
			throws IDPCoreMappingError;
}
