package idp.logic;

import idp.elements.ThreeValuedIndicator;
import idp.exceptions.InvalidAnnotationException;
import idp.exceptions.InvalidStructureException;
import idp.exceptions.UnavailableCollectionException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>This class contains collections of predicates, functions and types. This class can be used as both input
 * or output. As an input structure, the collections will be mapped to IDP elements. As an output
 * structure, it represents a resulting model where the IDP elements are mapped to collections 
 * in this class.</p>
 * 
 * <p>Types can be added as objects in the {@link #addCollection(Collection,Class)} method, 
 * but not as a 3-valued interpretation. Types can also be entered as a collection of String or int
 * elements, using {@link #addTypeCollection(Collection,String)}.</p>
 * 
 * <p>Predicates and functions can be added as a 2-valued interpretation in the 
 * {@link #addCollection(Collection,Class)} method. To enter a 3-valued interpretation, either 
 * {@link #addCollection(Collection,Class,ThreeValuedIndicator)} is used, or the methods
 * {@link #addTrueCollection(Collection,Class)}, {@link #addFalseCollection(Collection,Class)} and
 * {@link #addUnknownCollection(Collection,Class)}.</p>
 * 
 * <p>The corresponding getters for the collections are implemented in a similar format.</p>
 * 
 * @author Alexander Tang
 *
 */
public class IDPStructure{
	protected Map<Class<?>, Collection<?>> objectMap;
	private Map<String, Collection<String>> namedStringTypeMap;
	private Map<String, Collection<Integer>> namedIntegerTypeMap;
	
	// 3-valued interpretation maps
	protected Map<Class<?>, Collection<?>> objectTrueMap;
	protected Map<Class<?>, Collection<?>> objectFalseMap;
	protected Map<Class<?>, Collection<?>> objectUnknownMap;
	
	/**
	 * Initializes new IDPStructure.class instance.
	 */
	public IDPStructure(){
		this.objectMap = new HashMap<>();
		this.namedStringTypeMap = new HashMap<>();
		this.namedIntegerTypeMap = new HashMap<>();
		this.objectTrueMap = new HashMap<>();
		this.objectFalseMap = new HashMap<>();
		this.objectUnknownMap = new HashMap<>();
	}
	
	/**
	 * This method adds the given class as an undefined interpretation. IDP (3.6.0) enters this as
	 * a 3-valued interpretation by default, populating the <i>unknown</i> collection. The class is required 
	 * to have either the {@literal @Predicate} or {@literal @Function} class annotation. If a {@literal @Type} annotated class is added, it will
	 * be added with an empty collection.
	 * 
	 * @param cl	The class to be entered as an undefined interpretation.
	 * @throws InvalidAnnotationException Thrown when the constraint <i>"Exactly one of the class annotations {@literal @Predicate},
	 * 					{@literal @Function} and {@literal @Type} needs to be present."</i> is violated. In the case of a class annotated by
	 * 					{@literal @Function}, exactly one {@literal @FunctionValue} annotated field should be present, and that field should not
	 * 					have a {@literal @ArgumentType} annotation as well. In the case of a class annotated by {@literal @Type}, at most one
	 * 					{@literal @TypeValue} annotated field can be present.
	 * @throws InvalidStructureException Thrown when the given collection contains one or more null references, if
	 * 					the given class is already entered as a 3-valued interpretation, or if there is already a
	 * 					collection of type elements that shares the same name. Ie. a collection of String.class elements
	 * 					with the name "Foo" cannot co-exist with a class named "Foo". The IDP execution does not allow 
	 * 					this format due to its ambiguous nature.
	 */
	public void addClass(Class<?> cl) throws InvalidAnnotationException, 
			InvalidStructureException{
		addCollection(null, cl);
	}
	
	/**
	 * <p>This method adds a collection of elements that correspond with predicate, function or type objects, 
	 * depending on the class annotation. The collection is added as a 2-valued interpretation. If the user 
	 * wishes to enter an undefined interpretation, then the given collection should be null or use the 
	 * {@link #addClass(Class)} method instead.</p>
	 * 
	 * <p>For members of type collections that are String or Integer instances,
	 * use the {@link #addTypeCollection(Collection,String)} method instead.</p>
	 * 
	 * @param col The collection of elements to enter for the corresponding class. The collection cannot contain
	 * 				null references. If the given collection itself is a null reference, then the class is entered
	 * 				as an undefined interpretation. For IDP types, a null reference corresponds with an empty collection.
	 * @param cl The class to enter as a 2-valued interpretation.
	 * @throws InvalidAnnotationException Thrown when the constraint <i>"Exactly one of the class 
	 * 					annotations {@literal @Predicate}, {@literal @Function} or {@literal @Type} needs to be present."</i> is violated. In the case 
	 * 					of a class annotated by {@literal @Function}, exactly one {@literal @FunctionValue} annotated field should be present, 
	 * 					and that field should not have a {@literal @ArgumentType} annotation as well. In the case of a class 
	 * 					annotated by {@literal @Type}, at most one {@literal @TypeValue} annotated field can be present.
	 * @throws InvalidStructureException Thrown when the given collection contains one or more null references, if
	 * 					the given class is already entered as a 3-valued interpretation, or if there is already a
	 * 					collection of type elements that shares the same name. Ie. a collection of String.class elements
	 * 					with the name "Foo" cannot co-exist with a class named "Foo". The IDP execution does not allow 
	 * 					this format due to its ambiguous nature.
	 */
	public <T> void addCollection(Collection<T> col, Class<T> cl) throws InvalidAnnotationException, 
			InvalidStructureException{
		if(col != null && col.contains(null))
			throw new InvalidStructureException("Collections cannot contain null references for"
					+ " 2-valued interpretations and type objects. Error occurred in"
					+ " class:" + cl.getName() + ".");
		if(this.objectTrueMap.keySet().contains(cl) || this.objectFalseMap.keySet().contains(cl) || this.objectUnknownMap.keySet().contains(cl))
			throw new InvalidStructureException("Cannot add a collection as a 2-valued interpretation, if the same class "
					+ "is added as a 3-valued interpretation. Error occurred in this class: " + cl.getName() + ".");
		if(this.namedIntegerTypeMap.containsKey(cl.getSimpleName()) || this.namedStringTypeMap.containsKey(cl.getSimpleName()))
			throw new InvalidStructureException("A type collection with the same name as the given class name already exists"
					+ " for the class " + cl.getName() + ". Please do not use a "
					+ "name for a class that corresponds with an existing collection.");
		
		int count = 0;
		if(AnnotationChecker.hasFunctionAnnotation(cl))		count++;
		if(AnnotationChecker.hasPredicateAnnotation(cl))	count++;
		if(AnnotationChecker.hasTypeAnnotation(cl)){
			if(col == null)
				col = new ArrayList<>();
			count++;
		}
		
		if(count > 1)
			throw new InvalidAnnotationException("Only one annotation is allowed from @Predicate, @Function and @Type. "
					+ "Specifying any combination of @Predicate, @Function and @Type is not allowed. "
					+ "Error occurred in this class: " + cl.getName() + ".");
		if(count < 1)
			throw new InvalidAnnotationException("The elements in the collection must be annotated with @Predicate, @Function or @Type. "
					+ "Error occurred in this class: " + cl.getName() + ".");
		this.objectMap.put(cl, col);
	}

	/**
	 * <p>This method adds a collection of elements that corresponds with a predicate or function object.
	 * The collection is added as a 3-valued interpretation. The possible enum types are <b>CT</b>, <b>CF</b> 
	 * and <b>U</b>, which correspond to <i>'certainly true'</i>, <i>'certainly false'</i> and <i>'unknown'</i> 
	 * respectively.</p>
	 * 
	 * <p>If the user wishes to enter an undefined interpretation, use the {@link #addClass(Class)} method.</p>
	 * 
	 * <p>{@literal @Type} annotated classes cannot be added using this method. Use the 2-valued interpretation method instead:
	 * {@link #addCollection(Collection, Class)}.</p>
	 * 
	 * @param col The collection of elements to enter for the corresponding class. The collection cannot contain
	 * 				null references. The given collection itself also can not be a null reference.
	 * @param cl	The class to enter as a 3-valued interpretation.
	 * @param tvi The enum possibilities: CT (certainly true), CF (certainly false), U (unknown). If null
	 * 				is entered, then the given collection is added as a 2-valued interpretation for the given class.
	 * @throws InvalidAnnotationException <p>If the given 3-valued indicator is <b>CT</b>, <b>CF</b> or <b>U</b>: 
	 * 					Thrown when the constraint <i>"Exactly one of the class annotations {@literal @Predicate} or {@literal @Function} needs 
	 * 					to be present."</i> is violated. In the case of a class annotated by {@literal @Function}, exactly 
	 * 					one {@literal @FunctionValue} annotated field should be present, and that field should not
	 * 					have a {@literal @ArgumentType} annotation as well. If a class is annotated by {@literal @Type}, then this exception
	 * 					will be thrown as well.</p>
	 * 
	 * 					<p>If the given 3-valued indicator is null: 
	 * 					Thrown when the constraint <i>"Exactly one of the class annotations {@literal @Predicate}, {@literal @Function} or {@literal @Type} 
	 * 					needs to be present."</i> is violated. In the case of a class annotated by {@literal @Function}, exactly 
	 * 					one {@literal @FunctionValue} annotated field should be present, and that field should not have a {@literal @ArgumentType} 
	 * 					annotation as well. In the case of a class annotated by {@literal @Type}, at most one {@literal @TypeValue} annotated 
	 * 					field can be present.</p>
	 * 
	 * 					@see #addCollection(Collection, Class)
	 * @throws InvalidStructureException <p>Thrown when the given collection is a null reference or contains one or 
	 * 					more null references, or if there is already a collection of type elements that shares the 
	 * 					same name. Ie. a collection of String.class elements with the name "Foo" cannot co-exist with a 
	 * 					class named "Foo". The IDP execution does not allow this format due to its ambiguous nature.</p>
	 * 
	 * 					<p>If the given 3-valued indicator is <b>CT</b>, <b>CF</b> or <b>U</b>: 
	 * 					Thrown when the given class is already entered as a 2-valued interpretation.</p>
	 * 
	 * 					<p>If the given 3-valued indicator is null: 
	 * 					Thrown when the given class is already entered as a 3-valued interpretation.</p>
	 * 
	 * 					@see #addCollection(Collection, Class)
	 */
	public <T> void addCollection(Collection<T> col, Class<T> cl, ThreeValuedIndicator tvi) 
			throws InvalidAnnotationException, InvalidStructureException{
		if(tvi == null)
			addCollection(col, cl);
		else if(tvi == ThreeValuedIndicator.CT)
			addTrueCollection(col, cl);
		else if(tvi == ThreeValuedIndicator.CF)
			addFalseCollection(col, cl);
		else addUnknownCollection(col, cl);
	}

	/**
	 * This method adds a collection of String or Integer values that corresponds with a IDP type.
	 * 
	 * @param col	The collection of String or Integer values. The collection can not be a null reference or
	 * 				be empty. In the case of a collection of String elements, null elements correspond to the
	 * 				String "null" in IDP. For a collection of Integer elements, null elements correspond to 0 in IDP.
	 * @param name	The name of the type collection.
	 * @throws InvalidStructureException Thrown when: (i) the elements of the collection are not instances of Integer.class
	 * 				or String.class, (ii) the name of the collection has already been used for a different type collection
	 * 				or clashes with a class name (ie. Foo.class and collection name "Foo").
	 */
	@SuppressWarnings("unchecked")
	public void addTypeCollection(Collection<?> col, String name) throws InvalidStructureException{
		if(col == null || col.isEmpty())	return;
		Collection<Class<?>> existingClasses = new ArrayList<>();
		existingClasses.addAll(this.objectMap.keySet());
		existingClasses.addAll(this.objectTrueMap.keySet());
		existingClasses.addAll(this.objectFalseMap.keySet());
		existingClasses.addAll(this.objectFalseMap.keySet());
		for(Class<?> cl : existingClasses){
			if(cl.getSimpleName().equals(name))
				throw new InvalidStructureException("A class with the same name as the given type"
						+ " name already exists for the name: " + name + ". Please do not use a "
						+ "name for a collection that corresponds with an existing class.");
		}
		ArrayList<?> list = new ArrayList<>(col);
		if(list.get(0) instanceof String){
			if(!this.namedIntegerTypeMap.containsKey(name))
				this.namedStringTypeMap.put(name, (Collection<String>) col);
			else throw new InvalidStructureException("A collection with the same name as the given type"
					+ " name already exists for the name: " + name + ". Please do not use a "
					+ "name for a collection that corresponds with an existing collection.");
		} else if(list.get(0) instanceof Integer){
			if(!this.namedStringTypeMap.containsKey(name))
				this.namedIntegerTypeMap.put(name, (Collection<Integer>) col);
			else throw new InvalidStructureException("A collection with the same name as the given type"
					+ " name already exists for the name: " + name + ". Please do not use a "
					+ "name for a collection that corresponds with an existing collection.");
		}else	
			throw new InvalidStructureException("The elements of this collection must be instances of the class Integer or String.\n"
					+ "Error happened for the type collection name: " + name + ".");
	}
	
	/**
	 * This method adds a collection of <i>certainly true</i> values that corresponds with a predicate or function object.
	 * The collection is added as a 3-valued interpretation. IDP types can not be entered using this method.
	 * 
	 * @param col The collection elements to be added as <i>certainly true</i> in a 3-valued interpretation.
	 * @param cl The class to of the given collection elements.
	 * @throws InvalidAnnotationException Thrown when the constraint <i>"Exactly one of the class 
	 * 					annotations {@literal @Predicate} or {@literal @Function} needs to be present."</i> is violated. In the case of a 
	 * 					class annotated by {@literal @Function}, exactly one {@literal @FunctionValue} annotated field should be present, and 
	 * 					that field should not have a {@literal @ArgumentType} annotation as well. If a class is annotated by {@literal @Type}, 
	 * 					then this exception will be thrown as well.
	 * @throws InvalidStructureException Thrown when: (i) the given collection is a null reference or contains one or 
	 * 					more null references, (ii) if there is already a collection of type elements that shares the 
	 * 					same name (ie. A collection of String.class elements with the name "Foo" cannot co-exist with a 
	 * 					class named "Foo". The IDP execution does not allow this format due to its ambiguous nature),
	 * 					(iii) the given class is already entered as a 2-valued interpretation.
	 */
	public <T> void addTrueCollection(Collection<T> col, Class<T> cl) 
			throws InvalidAnnotationException, InvalidStructureException{
		addThreeValuedCollection(col, cl, this.objectTrueMap);
	}
	
	/**
	 * This method adds a collection of <i>certainly false</i> values that corresponds with a predicate or function object.
	 * The collection is added as a 3-valued interpretation. IDP types can not be entered using this method.
	 * 
	 * @param col The collection elements to be added as <i>certainly false</i> in a 3-valued interpretation.
	 * @param cl The class to of the given collection elements.
	 * @throws InvalidAnnotationException Thrown when the constraint <i>"Exactly one of the class 
	 * 					annotations {@literal @Predicate} or {@literal @Function} needs to be present."</i> is violated. In the case of a 
	 * 					class annotated by {@literal @Function}, exactly one {@literal @FunctionValue} annotated field should be present, 
	 * 					and that field should not have a {@literal @ArgumentType} annotation as well. If a class is annotated 
	 * 					by {@literal @Type}, then this exception will be thrown as well.
	 * @throws InvalidStructureException Thrown when: (i) the given collection is a null reference or contains one or 
	 * 					more null references, (ii) if there is already a collection of type elements that shares the 
	 * 					same name (ie. A collection of String.class elements with the name "Foo" cannot co-exist with a 
	 * 					class named "Foo". The IDP execution does not allow this format due to its ambiguous nature),
	 * 					(iii) the given class is already entered as a 2-valued interpretation.
	 */
	public <T> void addFalseCollection(Collection<T> col, Class<T> cl) 
			throws InvalidAnnotationException, InvalidStructureException{
		addThreeValuedCollection(col, cl, this.objectFalseMap);
	}
	
	/**
	 * This method adds a collection of <i>unknown</i> values that corresponds with a predicate or function object.
	 * The collection is added as a 3-valued interpretation. IDP types can not be entered using this method.
	 * 
	 * @param col The collection elements to be added as <i>unknown</i> in a 3-valued interpretation.
	 * @param cl The class to of the given collection elements.
	 * @throws InvalidAnnotationException Thrown when the constraint <i>"Exactly one of the class 
	 * 					annotations {@literal @Predicate} or {@literal @Function} needs to be present."</i> is violated. In the case of a 
	 * 					class annotated by {@literal @Function}, exactly one {@literal @FunctionValue} annotated field should be present, and 
	 * 					that field should not have a {@literal @ArgumentType} annotation as well. If a class is annotated by {@literal @Type}, 
	 * 					then this exception will be thrown as well.
	 * @throws InvalidStructureException Thrown when: (i) the given collection is a null reference or contains one or 
	 * 					more null references, (ii) if there is already a collection of type elements that shares the 
	 * 					same name (ie. A collection of String.class elements with the name "Foo" cannot co-exist with a 
	 * 					class named "Foo". The IDP execution does not allow this format due to its ambiguous nature),
	 * 					(iii) the given class is already entered as a 2-valued interpretation.
	 */
	public <T> void addUnknownCollection(Collection<T> col, Class<T> cl) 
			throws InvalidAnnotationException, InvalidStructureException{
		addThreeValuedCollection(col, cl, this.objectUnknownMap);
	}
	
	private <T> void addThreeValuedCollection(Collection<T> col, Class<T> cl, Map<Class<?>, Collection<?>> map) 
			throws InvalidStructureException, InvalidAnnotationException{
		if(col == null)	throw new InvalidStructureException("Collections cannot be null for "
				+ "3-valued interpretations. To specify an undefined interpretation for a predicate"
				+ " or function, use the addClass/1 method. The error occurred for class: "
				+ cl.getName() + ".");
		if(col.contains(null))	throw new InvalidStructureException("Collections cannot contain "
				+ "null references in 3-valued interpretations. Error occurred for class: " + cl.getName());
		if(this.namedIntegerTypeMap.containsKey(cl.getSimpleName()) || this.namedStringTypeMap.containsKey(cl.getSimpleName()))
			throw new InvalidStructureException("A type collection with the same name as the given class name already exists"
					+ " for the class " + cl.getName() + ". Please do not use a "
					+ "name for a class that corresponds with an existing collection.");
		if(noTwoValuedStructure(cl) && validThreeValuedStructureCollection(cl))
			map.put(cl, col);
		else throw new InvalidAnnotationException("The elements in the collection must be annotated with {@literal @Predicate} or {@literal @Function}.\n"
				+ "Error occurred in this class: " + cl.getName() + ".");
	}

	/**
	 * This method is used to verify that collections with elements of the given class are not entered
	 * as both a 2-valued and 3-valued interpretation.
	 * 
	 * @param cl	The class of elements to perform the check on.
	 * @return	Returns true if there are no 2-valued interpretations for the given class.
	 * @throws InvalidStructureException	Thrown if the given class already contains elements of the
	 * 										given class in a 2-valued interpretation.
	 */
	private boolean noTwoValuedStructure(Class<?> cl) throws InvalidStructureException{
		if(this.objectMap.keySet().contains(cl))
			throw new InvalidStructureException("Can not add a collection as a 3-valued interpretation, if the same collection is added to a 2-valued interpretation."
					+ " Error occurred in this class: " + cl.getName() + ".");
		else return true;
	}

	/**
	 * Checks if the given class is eligible to be entered as a 3-valued interpretation.
	 * 
	 * @param cl	The class to check eligibility as a 3-valued interpretation for.
	 * @return	Returns true if the class has either the {@literal @Predicate} or {@literal @Function} class annotation and returns
	 * 			false otherwise.
	 * @throws InvalidAnnotationException Thrown when the constraint <i>"Exactly one of the class annotations {@literal @Predicate} 
	 * 					or {@literal @Function} needs to be present."</i> is violated. In the case of a class annotated by {@literal @Function}, 
	 * 					exactly one {@literal @FunctionValue} annotated field should be present, and that field should not
	 * 					have a {@literal @ArgumentType} annotation as well. If a class is annotated by {@literal @Type}, then this exception
	 * 					will be thrown as well.
	 */
	private <T> boolean validThreeValuedStructureCollection(Class<T> cl) 
			throws InvalidAnnotationException{
		if(AnnotationChecker.hasTypeAnnotation(cl))
			throw new InvalidAnnotationException("3-valued interpretations don't apply to IDP types.\n"
				+ "Error occurred in this class: " + cl.getName() + ".");
		if(AnnotationChecker.hasFunctionAnnotation(cl) && AnnotationChecker.hasPredicateAnnotation(cl))
			throw new InvalidAnnotationException("Only one annotation is allowed from {@literal @Predicate} and {@literal @Function}. "
					+ "Specifying both {@literal @Predicate} and {@literal @Function} is not allowed. "
					+ "Error occurred in this class: " + cl.getName() + ".");
		if(AnnotationChecker.hasFunctionAnnotation(cl) || AnnotationChecker.hasPredicateAnnotation(cl))
			return true;
		else return false;
	}
	
	/**
	 * Retrieves a 2-valued collection that corresponds with the given {@literal @Predicate}, {@literal @Function} or {@literal @Type} annotated class.
	 * 
	 * @param cl	The class of the elements of the 2-valued collection to be retrieved.
	 * @return	Returns the the collection of which the elements correspond with the given class. 
	 * @throws UnavailableCollectionException Thrown if no collection could be found for the given class 
	 * 				or if the corresponding collection is a null reference.
	 */
	@SuppressWarnings("unchecked")
	public <T> Collection<T> getCollection(Class<T> cl) throws UnavailableCollectionException{
		if(this.objectMap.containsKey(cl)){
			Collection<T> c = (Collection<T>) this.objectMap.get(cl);
			if(c != null)
				return c;
		} 
		throw new UnavailableCollectionException("No collection "
				+ "exists for the 2-valued interpretation of the given class: " + cl + ".");
	}
	
	/**
	 * Retrieves a 3-valued collection that corresponds with the given {@literal @Predicate} or {@literal @Function} annotated class.
	 * 
	 * @param cl	The class of the elements of the 3-valued collection to be retrieved.
	 * @param tvi	The enum parameter with possible values {CT,CF,U}, denoting <i>certainly true</i>, 
	 * 				<i>certainly false</i> and <i>unknown</i> respectively. If null is entered, then 
	 * 				the 2-valued collection is returned instead.
	 * @return	Returns the 3-valued collection that corresponds with the given three valued indicator.
	 * 			Returns a 2-valued collection instead if the three valued indicator is a null reference.
	 * @throws UnavailableCollectionException Thrown if no collection could be found for the given class 
	 * 				or if the corresponding collection is a null reference.
	 */
	public <T> Collection<T> getCollection(Class<T> cl, ThreeValuedIndicator tvi) 
			throws UnavailableCollectionException{
		if(tvi == null)
			return getCollection(cl);
		else if(tvi == ThreeValuedIndicator.CT)
			return getTrueCollection(cl);
		else if(tvi == ThreeValuedIndicator.CF)
			return getFalseCollection(cl);
		else return getUnknownCollection(cl);
	}
	
	/**
	 * Retrieves a <i>certainly true</i> collection that corresponds with the given {@literal @Predicate} or {@literal @Function} 
	 * annotated class.
	 * 
	 * @param cl	The class of the elements of the <i>certainly true</i> collection to be retrieved.
	 * @return	Returns the <i>certainly true</i> collection.
	 * @throws UnavailableCollectionException Thrown if no <i>certainly true</i> collection could be found for the given class 
	 * 				or if the corresponding collection is a null reference.
	 */
	@SuppressWarnings("unchecked")
	public <T> Collection<T> getTrueCollection(Class<T> cl) throws UnavailableCollectionException{
		if(this.objectTrueMap.containsKey(cl))
			return (Collection<T>) this.objectTrueMap.get(cl);
		else throw new UnavailableCollectionException("No collection of 'certainly true' "
				+ "elements exists for the given class: " + cl + ".");
	}
	
	/**
	 * Retrieves a <i>certainly false</i> collection that corresponds with the given {@literal @Predicate} or {@literal @Function} 
	 * annotated class.
	 * 
	 * @param cl	The class of the elements of the <i>certainly false</i> collection to be retrieved.
	 * @return	Returns the <i>certainly false</i> collection.
	 * @throws UnavailableCollectionException Thrown if no <i>certainly false</i> collection could be found for the given class 
	 * 				or if the corresponding collection is a null reference.
	 */
	@SuppressWarnings("unchecked")
	public <T> Collection<T> getFalseCollection(Class<T> cl) throws UnavailableCollectionException{
		if(this.objectFalseMap.containsKey(cl))
			return (Collection<T>) this.objectFalseMap.get(cl);
		else throw new UnavailableCollectionException("No collection of 'certainly false' "
				+ "elements exists for the given class: " + cl + ".");
	}
	
	/**
	 * Retrieves a <i>unknown</i> collection that corresponds with the given {@literal @Predicate} or {@literal @Function} 
	 * annotated class.
	 * 
	 * @param cl	The class of the elements of the <i>unknown</i> collection to be retrieved.
	 * @return	Returns the <i>unknown</i> collection.
	 * @throws UnavailableCollectionException Thrown if no <i>unknown</i> collection could be found for the given class 
	 * 				or if the corresponding collection is a null reference.
	 */
	@SuppressWarnings("unchecked")
	public <T> Collection<T> getUnknownCollection(Class<T> cl) throws UnavailableCollectionException{
		if(this.objectUnknownMap.containsKey(cl))
			return (Collection<T>) this.objectUnknownMap.get(cl);
		else throw new UnavailableCollectionException("No collection of 'unknown' "
				+ "elements exists for the given class: " + cl + ".");
	}
	
	/**
	 * Retrieves the collection of String elements that corresponds with the given collection name.
	 * 
	 * @param name	The name of the collection of String elements to retrieve.
	 * @return	Returns a collection of String elements that corresponds with the given name.
	 * @throws UnavailableCollectionException Thrown when no collection of String elements could be found
	 * 				for the given name.
	 */
	public Collection<String> getStringCollection(String name) throws UnavailableCollectionException{
		if(this.namedStringTypeMap.containsKey(name))
			return this.namedStringTypeMap.get(name);
		else throw new UnavailableCollectionException("No type "
				+ "collection of strings exists for the given name: " + name + ".");
	}
	
	/**
	 * Retrieves the collection of Integer elements that corresponds with the given collection name.
	 * 
	 * @param name	The name of the collection of Integer elements to retrieve.
	 * @return	Returns a collection of Integer elements that corresponds with the given name.
	 * @throws UnavailableCollectionException Thrown when no collection of Integer elements could be found
	 * 				for the given name.
	 */
	public Collection<Integer> getIntegerCollection(String name) throws UnavailableCollectionException{
		if(this.namedIntegerTypeMap.containsKey(name))
			return this.namedIntegerTypeMap.get(name);
		else throw new UnavailableCollectionException("No type "
				+ "collection of integers exists for the given name: " + name + ".");
	}
	
	protected <T> void unrestrictedOverrideCollection(Collection<T> col, Class<T> cl, ThreeValuedIndicator tvi){
		if(tvi == null)
			this.objectMap.put(cl, col);
		else if(tvi == ThreeValuedIndicator.CT)
			this.objectTrueMap.put(cl, col);
		else if(tvi == ThreeValuedIndicator.CF)
			this.objectFalseMap.put(cl, col);
		else this.objectUnknownMap.put(cl, col);
	}
	
	Map<Class<?>, Collection<?>> getObjectMap(){
		return this.objectMap;
	}
	
	Map<String, Collection<String>> getStringTypeMap(){
		return this.namedStringTypeMap;
	}
	
	Map<String, Collection<Integer>> getIntegerTypeMap(){
		return this.namedIntegerTypeMap;
	}
	
	Map<Class<?>, Collection<?>> getObjectTrueMap(){
		return this.objectTrueMap;
	}
	
	Map<Class<?>, Collection<?>> getObjectFalseMap(){
		return this.objectFalseMap;
	}
	
	Map<Class<?>, Collection<?>> getObjectUnknownMap(){
		return this.objectUnknownMap;
	}	
}
