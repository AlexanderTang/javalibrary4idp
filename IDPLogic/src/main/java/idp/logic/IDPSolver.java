package idp.logic;

import idp.elements.ThreeValuedIndicator;
import idp.exceptions.FailedTempFileException;
import idp.exceptions.IDPCoreMappingError;
import idp.exceptions.IDPExecutionException;
import idp.exceptions.InvalidAnnotationException;
import idp.exceptions.InvalidConstructorException;
import idp.exceptions.MissingResourceException;
import idp.exceptions.NilStructureException;
import idp.exceptions.UnavailableCollectionException;
import idp.procedure.CalculateDefinitions;
import idp.procedure.GroundPropagate;
import idp.procedure.IDPProcedure;
import idp.procedure.Maximize;
import idp.procedure.Minimize;
import idp.procedure.ModelExpand;
import idp.procedure.OptimalPropagate;
import idp.procedure.ProcedureParameters;
import idp.procedure.Propagate;
import idp.procedure.Sat;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 * <p>The IDPSolver class provides a collection of methods to perform IDP inferences. The solver 
 * is instantiated through the {@link SolverBuilder} class. Some other optional parameters can be
 * entered through the SolverBuilder class. The inference methods expect at least an input 
 * IDP structure to perform the inference on.</p>
 * 
 * <p>The version of IDP currently supported is 3.6.0.</p>
 * 
 * @author Alexander Tang
 *
 */
public class IDPSolver {
	private int nbModels;
	private boolean replaceInput;
	private IDPTheory theory;
	private StringBuilder warningLog;
	private ProcedureParameters procParams;
	
	private IDPSolver(SolverBuilder builder) throws IOException, MissingResourceException{
		setNumberModels(builder.nbModels);
		replaceInput(builder.replaceInput);
		this.theory = builder.theory;
		String idpExecutionPath = getIDPExecutionPath(builder.idpConfigPath);
		this.procParams = new ProcedureParameters(builder.execTimeOut, idpExecutionPath);
		this.warningLog = new StringBuilder();
	}
	
	private String getIDPExecutionPath(URL idpConfigPath) throws IOException, MissingResourceException{
		BufferedReader reader = null;
		try{
			reader = new BufferedReader(new InputStreamReader(idpConfigPath.openStream()));
		} catch(NullPointerException e){
			throw new MissingResourceException("IDP configuration file could not be found.");
		}
		String line;
		while((line = reader.readLine()) != null){
			line = line.trim();
			if(line.startsWith("idpPath")){
				line = line.split("=")[1];
				line = line.trim();
				line = line.replace("\"", "");
				line = line.replace("'", "");
				break;
			}
		}
		reader.close();
		return line;
	}
	
	
	/*************
	 * INFERENCES
	 *************/

	/**
	 * Checks satisfiability of the given theory-structure combination. 
	 * 
	 * @param inputStruc The input IDP structure.
	 * @return Returns true if and only if there exists a model extending 
	 * 			the structure and satisfying the theory.
	 * @throws FailedTempFileException Thrown when something goes wrong with the construction 
	 * 			of the temporary .idp file. Temporary files are created in the temporary-file
	 * 			directory, using {@link File#createTempFile(String,String)}. 
	 * 			Make sure access rights are unlocked.
	 * @throws InvalidAnnotationException Thrown when IDP annotations are misused. Read the 
	 * 			documentation of the annotation classes in the <i>idp.annotations.*</i> 
	 * 			package to avoid this.
	 * @throws IDPExecutionException Thrown when the execution in IDP went wrong or unexpected
	 * 			results are returned. Error messages produced by IDP will be returned. Ensure that
	 * 			the IDP theory is constructed correctly and matches the given collections in the
	 * 			input IDP structure.
	 * @throws InvalidConstructorException	Thrown when the required constructor of a
	 * 			{@literal @Predicate} or {@literal @Function} annotated class does not exist.
	 * 			Consult {@link idp.annotations.Predicate} or {@link idp.annotations.Function} 
	 * 			for the proper usage.
	 * @throws MissingResourceException Thrown when a resource is missing.
	 */
	public boolean sat(IDPStructure inputStruc) 
			throws FailedTempFileException, InvalidAnnotationException, IDPExecutionException, 
			InvalidConstructorException, MissingResourceException, IDPCoreMappingError {
		try {
			IDPProcedure<Boolean> main = new Sat(this.procParams);
			IDPFile file = createIDPFile(inputStruc);
			boolean b = main.getResults(file, this.warningLog);
			file.getFile().delete(); // delete the temp file
			return b;
		} catch (NilStructureException e) {
			return false;
		}
	}

	/**
	 * Make the structure more precise than the given one by evaluating all definitions with known 
	 * open symbols. This procedure works recursively: as long as some definition of which all 
	 * open symbols are known exists, it calculates the definition (possibly deriving open symbols 
	 * of other definitions). 
	 * 
	 * @param inputStruc The input structure.
	 * @return Returns a more precise structure by evaluating all definitions with known 
	 * 			open symbols.
	 * @throws InvalidAnnotationException Thrown when IDP annotations are misused. Read the 
	 * 			documentation of the annotation classes in the <i>idp.annotations.*</i> 
	 * 			package to avoid this.
	 * @throws FailedTempFileException  Thrown when something goes wrong with the construction 
	 * 			of the temporary .idp file. Temporary files are created in the temporary-file
	 * 			directory, using {@link File#createTempFile(String,String)}. 
	 * 			Make sure access rights are unlocked.
	 * @throws IDPExecutionException Thrown when the execution in IDP went wrong or unexpected
	 * 			results are returned. Error messages produced by IDP will be returned. Ensure that
	 * 			the IDP theory is constructed correctly and matches the given collections in the
	 * 			input IDP structure.
	 * @throws NilStructureException Thrown when no resulting model is found. Make sure there is
	 * 			no inconsistency between the IDP structure and theory.
	 * @throws InvalidConstructorException Thrown when the required constructor of a
	 * 			{@literal @Predicate} or {@literal @Function} annotated class does not exist.
	 * 			Consult {@link idp.annotations.Predicate} or {@link idp.annotations.Function} 
	 * 			for the proper usage.
	 * @throws MissingResourceException Thrown when a resource is missing. 
	 */
	public IDPStructure calculateDefinitions(IDPStructure inputStruc) 
			throws InvalidAnnotationException, FailedTempFileException, IDPExecutionException, 
			NilStructureException, InvalidConstructorException, MissingResourceException, IDPCoreMappingError{
		return getResultingStructure(new CalculateDefinitions(this.procParams), inputStruc);
	}

	/**
	 * Returns a structure, made more precise than the input by grounding and unit propagation 
	 * on the theory.
	 * 
	 * @param inputStruc The input structure.
	 * @return Returns a structure, made more precise than the input by grounding and unit 
	 * 			propagation on the theory.
	 * @throws InvalidAnnotationException Thrown when IDP annotations are misused. Read the 
	 * 			documentation of the annotation classes in the <i>idp.annotations.*</i> 
	 * 			package to avoid this.
	 * @throws FailedTempFileException  Thrown when something goes wrong with the construction 
	 * 			of the temporary .idp file. Temporary files are created in the temporary-file
	 * 			directory, using {@link File#createTempFile(String,String)}. 
	 * 			Make sure access rights are unlocked.
	 * @throws IDPExecutionException Thrown when the execution in IDP went wrong or unexpected
	 * 			results are returned. Error messages produced by IDP will be returned. Ensure that
	 * 			the IDP theory is constructed correctly and matches the given collections in the
	 * 			input IDP structure.
	 * @throws NilStructureException Thrown when no resulting model is found. Make sure there is
	 * 			no inconsistency between the IDP structure and theory.
	 * @throws InvalidConstructorException Thrown when the required constructor of a
	 * 			{@literal @Predicate} or {@literal @Function} annotated class does not exist.
	 * 			Consult {@link idp.annotations.Predicate} or {@link idp.annotations.Function} 
	 * 			for the proper usage.
	 * @throws MissingResourceException Thrown when a resource is missing. 
	 */
	public IDPStructure groundPropagate(IDPStructure inputStruc) 
			throws InvalidAnnotationException, FailedTempFileException, IDPExecutionException, 
			NilStructureException, InvalidConstructorException, MissingResourceException, IDPCoreMappingError {
		return getResultingStructure(new GroundPropagate(this.procParams), inputStruc);
	}

	/**
	 * Returns a structure, made more precise than the input by generating all models and 
	 * checking which literals always have the same truth value. This propagation is complete: 
	 * everything that can be derived from the theory will be derived.
	 * 
	 * @param inputStruc The input structure.
	 * @return Returns a structure, made more precise than the input by generating all models and 
	 * 		  	checking which literals always have the same truth value. This propagation is 
	 * 			complete: everything that can be derived from the theory will be derived.
	 * @throws InvalidAnnotationException Thrown when IDP annotations are misused. Read the 
	 * 			documentation of the annotation classes in the <i>idp.annotations.*</i> 
	 * 			package to avoid this.
	 * @throws FailedTempFileException  Thrown when something goes wrong with the construction 
	 * 			of the temporary .idp file. Temporary files are created in the temporary-file
	 * 			directory, using {@link File#createTempFile(String,String)}. 
	 * 			Make sure access rights are unlocked.
	 * @throws IDPExecutionException Thrown when the execution in IDP went wrong or unexpected
	 * 			results are returned. Error messages produced by IDP will be returned. Ensure that
	 * 			the IDP theory is constructed correctly and matches the given collections in the
	 * 			input IDP structure.
	 * @throws NilStructureException Thrown when no resulting model is found. Make sure there is
	 * 			no inconsistency between the IDP structure and theory.
	 * @throws InvalidConstructorException Thrown when the required constructor of a
	 * 			{@literal @Predicate} or {@literal @Function} annotated class does not exist.
	 * 			Consult {@link idp.annotations.Predicate} or {@link idp.annotations.Function} 
	 * 			for the proper usage.
	 * @throws MissingResourceException Thrown when a resource is missing. 
	 */
	public IDPStructure optimalPropagate(IDPStructure inputStruc) 
			throws InvalidAnnotationException, FailedTempFileException, IDPExecutionException, 
			NilStructureException, InvalidConstructorException, MissingResourceException, IDPCoreMappingError {
		return getResultingStructure(new OptimalPropagate(this.procParams), inputStruc);
	}

	/**
	 * Returns a structure, made more precise than the input by doing symbolic propagation 
	 * on the theory.
	 * 
	 * @param inputStruc The input structure.
	 * @return Returns a structure, made more precise than the input by doing symbolic propagation 
	 * 			on the theory.
	 * @throws InvalidAnnotationException Thrown when IDP annotations are misused. Read the 
	 * 			documentation of the annotation classes in the <i>idp.annotations.*</i> 
	 * 			package to avoid this.
	 * @throws FailedTempFileException  Thrown when something goes wrong with the construction 
	 * 			of the temporary .idp file. Temporary files are created in the temporary-file
	 * 			directory, using {@link File#createTempFile(String,String)}. 
	 * 			Make sure access rights are unlocked.
	 * @throws IDPExecutionException Thrown when the execution in IDP went wrong or unexpected
	 * 			results are returned. Error messages produced by IDP will be returned. Ensure that
	 * 			the IDP theory is constructed correctly and matches the given collections in the
	 * 			input IDP structure.
	 * @throws NilStructureException Thrown when no resulting model is found. Make sure there is
	 * 			no inconsistency between the IDP structure and theory.
	 * @throws InvalidConstructorException Thrown when the required constructor of a
	 * 			{@literal @Predicate} or {@literal @Function} annotated class does not exist.
	 * 			Consult {@link idp.annotations.Predicate} or {@link idp.annotations.Function} 
	 * 			for the proper usage.
	 * @throws MissingResourceException Thrown when a resource is missing. 
	 */
	public IDPStructure propagate(IDPStructure inputStruc) 
			throws InvalidAnnotationException, FailedTempFileException, IDPExecutionException, 
			NilStructureException, InvalidConstructorException, MissingResourceException, IDPCoreMappingError {
		return getResultingStructure(new Propagate(this.procParams), inputStruc);
	}

	/**
	 * Apply model expansion to an IDP theory T and IDP structure S. The structure S can interpret 
	 * a subset of the vocabulary of the theory. This method allows the number of models and
	 * input replacement parameters to override the solver settings.
	 * 
	 * @param inputStruc The input structure S.
	 * @param nbModels	The number of models to return. If 0 is entered, all models will be returned.
	 * @param replaceInput	If true, then the input structure will be replaced by a solution 
	 * 			from the resulting models. If false, the input structure remains unchanged.
	 * @return Returns a collection of resulting models. The collection contains a maximum of 
	 * 			the given number of models (IDP structures). If no models exist that satisfy 
	 * 			IDP theory T and IDP structure S, then an empty collection is returned.
	 * @throws InvalidAnnotationException Thrown when IDP annotations are misused. Read the 
	 * 			documentation of the annotation classes in the <i>idp.annotations.*</i> 
	 * 			package to avoid this.
	 * @throws FailedTempFileException  Thrown when something goes wrong with the construction 
	 * 			of the temporary .idp file. Temporary files are created in the temporary-file
	 * 			directory, using {@link File#createTempFile(String,String)}. 
	 * 			Make sure access rights are unlocked.
	 * @throws IDPExecutionException Thrown when the execution in IDP went wrong or unexpected
	 * 			results are returned. Error messages produced by IDP will be returned. Ensure that
	 * 			the IDP theory is constructed correctly and matches the given collections in the
	 * 			input IDP structure.
	 * @throws NilStructureException Thrown when no resulting model is found. Make sure there is
	 * 			no inconsistency between the IDP structure and theory.
	 * @throws InvalidConstructorException Thrown when the required constructor of a
	 * 			{@literal @Predicate} or {@literal @Function} annotated class does not exist.
	 * 			Consult {@link idp.annotations.Predicate} or {@link idp.annotations.Function} 
	 * 			for the proper usage.
	 * @throws MissingResourceException Thrown when a resource is missing. 
	 */
	public Collection<IDPStructure> modelExpand(IDPStructure inputStruc, int nbModels, boolean replaceInput) 
			throws FailedTempFileException, InvalidAnnotationException, IDPExecutionException, 
			InvalidConstructorException, MissingResourceException, IDPCoreMappingError {
		IDPProcedure<Collection<IDPStructure>> main = new ModelExpand(this.procParams, nbModels);
		IDPFile file = createIDPFile(inputStruc);
		Collection<IDPStructure> structures;
		try {
			structures = main.getResults(file, this.warningLog);
		} catch (NilStructureException e) {
			return new HashSet<>();
		}
		if(structures != null && !structures.isEmpty() && replaceInput)
			overrideInputStructure(inputStruc, new ArrayList<>(structures).get(0));
		file.getFile().delete();
		return structures;
	}

	/**
	 * Apply model expansion to an IDP theory T and IDP structure S. The structure S can interpret 
	 * a subset of the vocabulary of the theory.
	 * 
	 * @param inputStruc The input structure S.
	 * @return Returns a collection of resulting models. The collection contains a maximum of 
	 * 			the given number of models (IDP structures). If no models exist that satisfy 
	 * 			IDP theory T and IDP structure S, then an empty collection is returned.
	 * @throws InvalidAnnotationException Thrown when IDP annotations are misused. Read the 
	 * 			documentation of the annotation classes in the <i>idp.annotations.*</i> 
	 * 			package to avoid this.
	 * @throws FailedTempFileException  Thrown when something goes wrong with the construction 
	 * 			of the temporary .idp file. Temporary files are created in the temporary-file
	 * 			directory, using {@link File#createTempFile(String,String)}. 
	 * 			Make sure access rights are unlocked.
	 * @throws IDPExecutionException Thrown when the execution in IDP went wrong or unexpected
	 * 			results are returned. Error messages produced by IDP will be returned. Ensure that
	 * 			the IDP theory is constructed correctly and matches the given collections in the
	 * 			input IDP structure.
	 * @throws NilStructureException Thrown when no resulting model is found. Make sure there is
	 * 			no inconsistency between the IDP structure and theory.
	 * @throws InvalidConstructorException Thrown when the required constructor of a
	 * 			{@literal @Predicate} or {@literal @Function} annotated class does not exist.
	 * 			Consult {@link idp.annotations.Predicate} or {@link idp.annotations.Function} 
	 * 			for the proper usage.
	 * @throws MissingResourceException Thrown when a resource is missing. 
	 * 
	 * @see #modelExpand(IDPStructure,int,boolean)
	 */
	public Collection<IDPStructure> modelExpand(IDPStructure inputStruc) 
			throws FailedTempFileException, InvalidAnnotationException, IDPExecutionException, 
			InvalidConstructorException, MissingResourceException, IDPCoreMappingError {
		return modelExpand(inputStruc, getDefaultNumberModels(), getReplaceInput());
	}

	/**
	 * Apply model expansion to an IDP theory T and IDP structure S. The structure S can interpret 
	 * a subset of the vocabulary of the theory. This method returns only one model (if
	 * a model exists).
	 * 
	 * @param inputStruc The input structure S.
	 * @return Returns one of the resulting models.
	 * @throws InvalidAnnotationException Thrown when IDP annotations are misused. Read the 
	 * 			documentation of the annotation classes in the <i>idp.annotations.*</i> 
	 * 			package to avoid this.
	 * @throws FailedTempFileException  Thrown when something goes wrong with the construction 
	 * 			of the temporary .idp file. Temporary files are created in the temporary-file
	 * 			directory, using {@link File#createTempFile(String,String)}. 
	 * 			Make sure access rights are unlocked.
	 * @throws IDPExecutionException Thrown when the execution in IDP went wrong or unexpected
	 * 			results are returned. Error messages produced by IDP will be returned. Ensure that
	 * 			the IDP theory is constructed correctly and matches the given collections in the
	 * 			input IDP structure.
	 * @throws NilStructureException Thrown when no resulting model is found. Make sure there is
	 * 			no inconsistency between the IDP structure and theory.
	 * @throws InvalidConstructorException Thrown when the required constructor of a
	 * 			{@literal @Predicate} or {@literal @Function} annotated class does not exist.
	 * 			Consult {@link idp.annotations.Predicate} or {@link idp.annotations.Function} 
	 * 			for the proper usage.
	 * @throws MissingResourceException Thrown when a resource is missing. 
	 * 
	 * @see #modelExpand(IDPStructure,int,boolean)
	 */
	public IDPStructure singleModelExpand(IDPStructure inputStruc) 
			throws FailedTempFileException, InvalidAnnotationException, IDPExecutionException, 
			NilStructureException, InvalidConstructorException, MissingResourceException, IDPCoreMappingError {
		Collection<IDPStructure> structures = modelExpand(inputStruc, 1, getReplaceInput());
		if(structures == null || structures.isEmpty())
			throw new NilStructureException();
		return new ArrayList<>(structures).get(0);
	}

	/**
	 * <p>Returns a {@link IDPOptimizationResults} object that contains:
	 * <ul>
	 *   <li>the collection of models that satisfy the minimization term (empty if there 
	 *   		are no resulting models),</li>
	 *   <li>a boolean that's true if an optimum has been found and false otherwise,</li>
	 *   <li>a value that satisfies the minimization constraint.</li>
	 * </ul></p>
	 * 
	 * @param inputStruc The input structure.
	 * @param term Term that expresses the minimization constraint.
	 * @return Returns minimization results.
	 * @throws InvalidAnnotationException Thrown when IDP annotations are misused. Read the 
	 * 			documentation of the annotation classes in the <i>idp.annotations.*</i> 
	 * 			package to avoid this.
	 * @throws FailedTempFileException  Thrown when something goes wrong with the construction 
	 * 			of the temporary .idp file. Temporary files are created in the temporary-file
	 * 			directory, using {@link File#createTempFile(String,String)}. 
	 * 			Make sure access rights are unlocked.
	 * @throws IDPExecutionException Thrown when the execution in IDP went wrong or unexpected
	 * 			results are returned. Error messages produced by IDP will be returned. Ensure that
	 * 			the IDP theory is constructed correctly and matches the given collections in the
	 * 			input IDP structure.
	 * @throws InvalidConstructorException Thrown when the required constructor of a
	 * 			{@literal @Predicate} or {@literal @Function} annotated class does not exist.
	 * 			Consult {@link idp.annotations.Predicate} or {@link idp.annotations.Function} 
	 * 			for the proper usage.
	 * @throws MissingResourceException Thrown when a resource is missing. 
	 */
	public IDPOptimizationResults minimize(IDPStructure inputStruc, IDPTerm term) 
			throws InvalidAnnotationException, FailedTempFileException, IDPExecutionException, 
			InvalidConstructorException, MissingResourceException, IDPCoreMappingError {
		return getResultingOptimization(new Minimize(this.procParams, getDefaultNumberModels()), inputStruc, term);
	}

	/**
	 * <p>Returns a {@link IDPOptimizationResults} object that contains:
	 * <ul>
	 *   <li>the collection of models that satisfy the maximization term (empty if there 
	 *   		are no resulting models),</li>
	 *   <li>a boolean that's true if an optimum has been found and false otherwise,</li>
	 *   <li>a value that satisfies the maximization constraint.</li>
	 * </ul></p>
	 * 
	 * @param inputStruc The input structure.
	 * @param term Term that expresses the maximization constraint.
	 * @return Returns maximization results.
	 * @throws InvalidAnnotationException Thrown when IDP annotations are misused. Read the 
	 * 			documentation of the annotation classes in the <i>idp.annotations.*</i> 
	 * 			package to avoid this.
	 * @throws FailedTempFileException  Thrown when something goes wrong with the construction 
	 * 			of the temporary .idp file. Temporary files are created in the temporary-file
	 * 			directory, using {@link File#createTempFile(String,String)}. 
	 * 			Make sure access rights are unlocked.
	 * @throws IDPExecutionException Thrown when the execution in IDP went wrong or unexpected
	 * 			results are returned. Error messages produced by IDP will be returned. Ensure that
	 * 			the IDP theory is constructed correctly and matches the given collections in the
	 * 			input IDP structure.
	 * @throws InvalidConstructorException Thrown when the required constructor of a
	 * 			{@literal @Predicate} or {@literal @Function} annotated class does not exist.
	 * 			Consult {@link idp.annotations.Predicate} or {@link idp.annotations.Function} 
	 * 			for the proper usage.
	 * @throws MissingResourceException Thrown when a resource is missing. 
	 */
	public IDPOptimizationResults maximize(IDPStructure inputStruc, IDPTerm term) 
			throws InvalidAnnotationException, FailedTempFileException, IDPExecutionException, 
			InvalidConstructorException, MissingResourceException, IDPCoreMappingError {
		return getResultingOptimization(new Maximize(this.procParams, getDefaultNumberModels()), inputStruc, term);
	}
	
	private IDPStructure getResultingStructure(IDPProcedure<IDPStructure> main, IDPStructure inputStruc) 
			throws InvalidAnnotationException, FailedTempFileException, IDPExecutionException, 
			NilStructureException, InvalidConstructorException, MissingResourceException, IDPCoreMappingError{
		IDPFile file = createIDPFile(inputStruc);
		IDPStructure struc = main.getResults(file, this.warningLog);
		if(struc != null && getReplaceInput())
			overrideInputStructure(inputStruc, struc);
		file.getFile().delete();
		return struc;
	}
	
	private IDPOptimizationResults getResultingOptimization(IDPProcedure<IDPOptimizationResults> main, 
			IDPStructure inputStruc, IDPTerm term) 
					throws InvalidAnnotationException, FailedTempFileException, IDPExecutionException, 
					InvalidConstructorException, MissingResourceException, IDPCoreMappingError{
		try {
			IDPFile file = createIDPFile(inputStruc, term);
			IDPOptimizationResults results = main.getResults(file, this.warningLog);
			if(results.getStructures() != null && !results.getStructures().isEmpty() 
					&& getReplaceInput())
				overrideInputStructure(inputStruc, new ArrayList<>(results.getStructures()).get(0));
			file.getFile().delete();
			return results;
		} catch (NilStructureException e) {
			return new IDPOptimizationResults(new ArrayList<IDPStructure>(), false, 0);
		}
		
	}
	
	private IDPFile createIDPFile(IDPStructure inputStruc) 
			throws InvalidAnnotationException, FailedTempFileException, MissingResourceException, IDPCoreMappingError{
		return new IDPFile(getTheory(), inputStruc);
	}
	
	private IDPFile createIDPFile(IDPStructure inputStruc, IDPTerm term) 
			throws InvalidAnnotationException, FailedTempFileException, MissingResourceException, IDPCoreMappingError{
		return new IDPFile(getTheory(), inputStruc, term);
	}
	
	private void overrideInputStructure(IDPStructure inputStruc, IDPStructure resultStruc){
		try{
			for(String intCollectionName : resultStruc.getIntegerTypeMap().keySet()){
				Collection<Integer> intCollection = 
						inputStruc.getIntegerCollection(intCollectionName);
				intCollection.clear();
				intCollection.addAll(resultStruc.getIntegerTypeMap().get(intCollectionName));
			}
			for(String stringCollectionName : resultStruc.getStringTypeMap().keySet()){
				Collection<String> stringCollection = 
						inputStruc.getStringCollection(stringCollectionName);
				stringCollection.clear();
				stringCollection.addAll(resultStruc.getStringTypeMap().get(stringCollectionName));
			}
		} catch(UnavailableCollectionException e){
			throw new IDPCoreMappingError("A type collection of strings or integers could not "
					+ "be found despite it existing in the input structure. This is an implementation"
					+ " error of the IDP mapper.\nThe error message was: " + e.getMessage());
		}
		for(Class<?> cl : resultStruc.getObjectMap().keySet())
			replaceCollection(inputStruc, cl, resultStruc.getObjectMap().get(cl), null);
		for(Class<?> cl : resultStruc.getObjectTrueMap().keySet())
			replaceCollection(inputStruc, cl, resultStruc.getObjectTrueMap().get(cl),
					ThreeValuedIndicator.CT);
		for(Class<?> cl : resultStruc.getObjectFalseMap().keySet())
			replaceCollection(inputStruc, cl, resultStruc.getObjectFalseMap().get(cl),
					ThreeValuedIndicator.CF);
		for(Class<?> cl : resultStruc.getObjectUnknownMap().keySet())
			replaceCollection(inputStruc, cl, resultStruc.getObjectUnknownMap().get(cl),
					ThreeValuedIndicator.U);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void replaceCollection(IDPStructure inputStruc, Class<?> cl, 
			Collection<?> output, ThreeValuedIndicator tvi){
		if(output == null){
			inputStruc.unrestrictedOverrideCollection(null, cl, tvi);
			return;
		}
		Collection col;
		try{
			col = inputStruc.getCollection(cl, tvi);
		} catch(UnavailableCollectionException e){
			col = new HashSet<>();
			inputStruc.unrestrictedOverrideCollection(col, cl, tvi);
		}
		col.clear();
		col.addAll(output);
	}

	/**********************
	 * SETTERS AND GETTERS
	 **********************/
	
	/**
	 * Sets the number of models to generate for inferences that generate a collection
	 * 					of models.
	 * 
	 * @param nbModels	The number of models to generate for inferences that generate a collection
	 * 					of models.
	 */
	public void setNumberModels(int nbModels){
		if(nbModels < 0)
			this.nbModels = 0;
		this.nbModels = nbModels;
	}
	
	/**
	 * Returns the maximum amount of models this solver will generate for inferences
	 * that generate a collection of models. If 0 is returned, it means all models 
	 * will be generated.
	 * 
	 * @return	Returns the maximum amount of models this solver will generate for inferences
	 * 			that generate a collection of models. If 0 is returned, it means all models 
	 * will be generated.
	 */
	public int getDefaultNumberModels(){
		return this.nbModels;
	}
	
	/**
	 * <p>Set true if the input structure should be modified by the results and false otherwise. <b>The
	 * collection instances are kept:</b> If a reference to the collection exists outside of the
	 * IDP structure, there is no need to retrieve the collection from the IDP structure to 
	 * update the contents of the collection.</p>
	 * 
	 * @param replace	True if the input structure should be modified by the results and 
	 * 				false otherwise.
	 */
	public void replaceInput(boolean replace){
		this.replaceInput = replace;
	}
	
	/**
	 * <p>Returns true if the input structure will be modified by the results and false otherwise. <b>The
	 * collection instances are kept:</b> If a reference to the collection exists outside of the
	 * IDP structure, there is no need to retrieve the collection from the IDP structure to 
	 * update the contents of the collection.</p>
	 * 
	 * @return	Returns true if the input structure will be modified by the results and false otherwise.
	 */
	public boolean getReplaceInput(){
		return this.replaceInput;
	}
	
	/**
	 * Returns the IDP theory for this solver.
	 * 
	 * @return	Returns the IDP theory for this solver.
	 */
	public IDPTheory getTheory(){
		return this.theory;
	}
	
	/**
	 * Returns a warning log that occurred from inference methods in this solver. The log is 
	 * built up cumulatively.
	 * 
	 * @return	Returns a warning log that occurred from inference methods in this solver.
	 */
	public String getWarningLog(){
		return this.warningLog.toString();
	}
	
	/**
	 * <p>The solver builder is used to instantiate the IDPSolver. The primary use of this builder is to provide
	 * a readable and flexible way to enter a subset of the (potentially numerous) possible parameters, in any
	 * order the user chooses.  Only the mandatory parameters are fixed in the constructor.</p>
	 * 
	 * <p>By default:
	 * <ul>
	 *  <li>the maximum number of models are generated (all of them),</li>
	 *  <li>the input structure does not get replaced,</li>
	 *  <li>the IDP execution times out after 1 minute.</li>
	 * </ul></p>
	 * 
	 * <p>The version of IDP currently supported is 3.6.0.</p>
	 * 
	 * @author Alexander Tang
	 *
	 */
	public static class SolverBuilder {
		private int nbModels;
		private boolean replaceInput;
		private IDPTheory theory;
		private long execTimeOut;
		private URL idpConfigPath;
		
		/**
		 * <p>Instantiates the solver builder. An IDP theory instance and the path to the
		 * IDP configuration file are required.</p>
		 * 
		 * <p>By default:
		 * <ul>
		 *  <li>the maximum number of models are generated (all of them),</li>
		 *  <li>the input structure does not get replaced,</li>
		 *  <li>the IDP execution times out after 1 minute.</li>
		 * </ul></p>
		 * 
		 * @param theory The instance containing the path(s) to IDP theory files.
		 * @param idpConfigPath The path to the IDP configuration file.
		 */
		public SolverBuilder(IDPTheory theory, URL idpConfigPath){
			this.theory = theory;
			this.idpConfigPath = idpConfigPath;
			this.nbModels = 0;
			this.replaceInput = false;
			this.execTimeOut = 60000; // default 60 seconds
		}

		/**
		 * The execution time of IDP will timeout after the specified amount of milliseconds.
		 * 
		 * @param timeout The execution time of IDP will timeout after the specified 
		 * 			amount of milliseconds.
		 * @return Returns the solver builder.
		 */
		public SolverBuilder execTimeOutInMilliSeconds(long timeout){
			this.execTimeOut = timeout;
			return this;
		}

		/**
		 * The execution time of IDP will timeout after the specified amount of seconds.
		 * 
		 * @param timeout The execution time of IDP will timeout after the 
		 * 			specified amount of seconds.
		 * @return Returns the solver builder.
		 */
		public SolverBuilder execTimeOutInSeconds(long timeout){
			this.execTimeOut = timeout * 1000;
			return this;
		}

		/**
		 * The execution time of IDP will timeout after the specified amount of minutes.
		 * 
		 * @param timeout The execution time of IDP will timeout after the specified 
		 * 			amount of minutes.
		 * @return Returns the solver builder.
		 */
		public SolverBuilder execTimeOutInMinutes(long timeout){
			this.execTimeOut = timeout * 60000;
			return this;
		}
		
		/**
		 * The solver will compute the specified amount of models for inferences that compute 
		 * models (IDP structures). If there are less models available or the given
		 * number of models is equal or less than 0, then all models will be computed.
		 * 
		 * @param nbModels Number of models to compute. Enter 0 or less to compute all models.
		 * @return Returns the solver builder.
		 */
		public SolverBuilder nbModels(int nbModels){
			if(nbModels < 0)
				nbModels = 0;
			this.nbModels = nbModels;
			return this;
		}
		
		/**
		 * <p>The solver will replace the input structure by the resulting IDP structure (or the 
		 * first computed model if multiple are available) if the <i>replace</i> parameter equals 
		 * true. The original collection instances that were used in the input structure are 
		 * kept, such that the collections don't need to be retrieved again from the input
		 * structure after computation. The input structure remains unchanged if the 
		 * <i>replace</i> parameter equals false.</p>
		 * 
		 * @param replace	Replaces the input structure by a resulting model if true.
		 * @return Returns the solver builder.
		 */
		public SolverBuilder replaceInput(boolean replace){
			this.replaceInput = replace;
			return this;
		}
		
		/**
		 * Builds and returns a IDPSolver.class instance with the builder parameters.
		 * 
		 * @return	Returns a new instance of IDPSolver.class.
		 * @throws IOException	Thrown when the IDP configuration file could not be accessed.
		 * @throws MissingResourceException Thrown when the IDP configuration file could not be found.
		 */
		public IDPSolver build() throws IOException, MissingResourceException{
			return new IDPSolver(this);
		}
	}

}
