package idp.logic;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * This class stores IDP theory files (standard extension is .idptheory).
 * 
 * @author Alexander Tang
 *
 */
public class IDPTheory {
	private List<URL> files = new ArrayList<>();
	
	private IDPTheory(URL fileURL){
		addFile(fileURL);
	}
	
	/**
	 * Initialize a IDPTheory object by entering the path of a idp theory file.
	 * 
	 * @param fileURL Path to idp theory file.
	 * @return Returns a new instance of IDPTheory.class that contains a path to the idp theory file.
	 */
	public static IDPTheory loadFile(URL fileURL){
		return new IDPTheory(fileURL);
	}
	
	/**
	 * Add an additional path to a idp theory file.
	 * 
	 * @param fileURL Path to idp theory file.
	 */
	public void addFile(URL fileURL){
		this.files.add(fileURL);
	}
	
	/**
	 * Remove the specified path to a idp theory file from the current list of paths.
	 * 
	 * @param filepath Path to idp theory file to remove.
	 */
	public void removeFile(URL filepath){
		this.files.remove(filepath);
	}
	
	/**
	 * Returns a collection of all the paths to idp theory files.
	 * 
	 * @return Returns a collection of all the paths to idp theory files.
	 */
	public Collection<URL> getTheoryFilePaths(){
		return Collections.unmodifiableCollection(this.files);
	}

}
