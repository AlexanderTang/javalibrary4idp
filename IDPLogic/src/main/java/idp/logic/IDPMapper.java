package idp.logic;

import idp.annotations.ArgumentType;
import idp.annotations.FunctionValue;
import idp.elements.IDPFunction;
import idp.elements.IDPPredicate;
import idp.elements.IDPType;
import idp.elements.IDPTypeID;
import idp.elements.IDPTypeInteger;
import idp.elements.IDPTypeString;
import idp.elements.ThreeValuedIndicator;
import idp.exceptions.IDPCoreMappingError;
import idp.exceptions.InvalidAnnotationException;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class IDPMapper {
	private IDPStructure inputStruc;
	
	/**
	 * Maps to retrieve Java classes from IDP strings.
	 */
	private Map<String, IDPPredicate> predicateIDPToClass;
	private Map<String, IDPFunction> functionIDPToClass;
	private Map<String, IDPType> typeIDPToClass;
	
	IDPMapper(IDPStructure struc) throws InvalidAnnotationException{
		this.inputStruc = struc;
		
		this.predicateIDPToClass = new HashMap<>();
		this.functionIDPToClass = new HashMap<>();
		this.typeIDPToClass = new HashMap<>();
		
		initMaps();
	}
	
	public IDPStructure getInputStructure(){
		return this.inputStruc;
	}
	
	public Map<String, IDPPredicate> getPredicateMap(){
		return this.predicateIDPToClass;
	}
	
	public Map<String, IDPFunction> getFunctionMap(){
		return this.functionIDPToClass;
	}
	
	public Map<String, IDPType> getTypeMap(){
		return this.typeIDPToClass;
	}
	
	private void initMaps() throws InvalidAnnotationException{
		try{
			for(String intTypeCollectionName : this.inputStruc.getIntegerTypeMap().keySet()){
				Collection<Integer> col = this.inputStruc.getIntegerTypeMap().get(intTypeCollectionName);
				if(!this.typeIDPToClass.containsKey(intTypeCollectionName))
					this.typeIDPToClass.put(intTypeCollectionName, new IDPTypeInteger(col, intTypeCollectionName));
				else ((IDPTypeInteger) this.typeIDPToClass.get(intTypeCollectionName)).getInstantiatedTypeIntegers().addAll(col);
			}
			for(String stringTypeCollectionName : this.inputStruc.getStringTypeMap().keySet()){
				Collection<String> col = this.inputStruc.getStringTypeMap().get(stringTypeCollectionName);
				if(!this.typeIDPToClass.containsKey(stringTypeCollectionName))
					this.typeIDPToClass.put(stringTypeCollectionName, new IDPTypeString(col, stringTypeCollectionName));
				else ((IDPTypeString) this.typeIDPToClass.get(stringTypeCollectionName)).getInstantiatedTypeStrings().addAll(col);
			}
		} catch(ClassCastException e){
			throw new IDPCoreMappingError("The same name has been found for two collections "
					+ "of IDP types where one is an integer and the other is a String.\n"
					+ "This issue should have been resolved in IDPStructure.class.\n"
					+ "Error message: " + e.getMessage());
		}
		
		categorizeObjects(this.inputStruc.getObjectMap());
		categorizeThreeValuedObjects(this.inputStruc.getObjectTrueMap(), ThreeValuedIndicator.CT);
		categorizeThreeValuedObjects(this.inputStruc.getObjectFalseMap(), ThreeValuedIndicator.CF);
		categorizeThreeValuedObjects(this.inputStruc.getObjectUnknownMap(), ThreeValuedIndicator.U);
	}
	
	private void categorizeObjects(Map<Class<?>, Collection<?>> map) 
			throws InvalidAnnotationException{
		try{
			for(Class<?> cl : map.keySet()){
				if(AnnotationChecker.hasTypeAnnotation(cl)){
					if(AnnotationChecker.getTypeIdentifier(cl) == IDPTypeID.INTEGER || AnnotationChecker.getTypeIdentifier(cl) == null){
						if(!this.typeIDPToClass.containsKey(cl.getSimpleName()))
							this.typeIDPToClass.put(cl.getSimpleName(),
									new IDPTypeInteger(cl, map.get(cl), cl.getSimpleName()));
						else this.typeIDPToClass.get(cl.getSimpleName()).getInstantiatedTypeObjects().addAll(map.get(cl));
					} else if(AnnotationChecker.getTypeIdentifier(cl) == IDPTypeID.STRING){
						if(!this.typeIDPToClass.containsKey(cl.getSimpleName()))
							this.typeIDPToClass.put(cl.getSimpleName(), 
									new IDPTypeString(cl, map.get(cl), cl.getSimpleName()));
						else this.typeIDPToClass.get(cl.getSimpleName()).getInstantiatedTypeObjects().addAll(map.get(cl));
					} else throw new InvalidAnnotationException("Type classes must have either a String or int attribute "
							+ "with the @TypeValue annotation. In the case of a type class using its hash value, "
							+ "no @TypeValue annotated attribute should be given. Error occurred in this class: " 
							+ cl.getName() + ".");
				}
				else if(AnnotationChecker.hasPredicateAnnotation(cl)){
					fillMissingTypes(map, cl, false);
					this.predicateIDPToClass.put(cl.getSimpleName(), 
							new IDPPredicate(cl, map.get(cl), cl.getSimpleName()));
				}
				else if(AnnotationChecker.hasFunctionAnnotation(cl)){
					fillMissingTypes(map, cl, true);
					this.functionIDPToClass.put(cl.getSimpleName(), 
							new IDPFunction(cl, map.get(cl), cl.getSimpleName()));
				}
				else throw new InvalidAnnotationException("An annotation @Predicate, @Function or @Type is "
						+ "missing for class " + cl.getName() + ".");
			}
		} catch(ClassCastException e){
			throw new IDPCoreMappingError("An error has occurred where some collections and/or @Type annotated classes have the same name,"
					+ " but different return types. Prevent the use of the same names if the IDP types should be different.\n"
					+ "This issue should have been resolved in IDPStructure.class.\n" + 
					"Original error message: " + e.getMessage());
		}
	}
	
	private void categorizeThreeValuedObjects(Map<Class<?>, Collection<?>> map, ThreeValuedIndicator tvi) 
			throws InvalidAnnotationException{
		try{
			for(Class<?> cl : map.keySet()){
				if(AnnotationChecker.hasPredicateAnnotation(cl)){
					fillMissingTypes(map, cl, false);
					if(!this.predicateIDPToClass.containsKey(cl.getSimpleName()))
						this.predicateIDPToClass.put(cl.getSimpleName(), 
								new IDPPredicate(cl, map.get(cl), tvi, cl.getSimpleName()));
					else this.predicateIDPToClass.get(cl.getSimpleName()).addThreeValuedCollection(map.get(cl), tvi);
				}
				else if(AnnotationChecker.hasFunctionAnnotation(cl)){
					fillMissingTypes(map, cl, true);
					if(!this.functionIDPToClass.containsKey(cl.getSimpleName()))
						this.functionIDPToClass.put(cl.getSimpleName(), 
								new IDPFunction(cl, map.get(cl), tvi, cl.getSimpleName()));
					else this.functionIDPToClass.get(cl.getSimpleName()).addThreeValuedCollection(map.get(cl), tvi);
				}
				else throw new InvalidAnnotationException("An annotation @Predicate or @Function is "
						+ "missing for class " + cl.getName() + ".");
			}
		} catch(ClassCastException e){
			throw new IDPCoreMappingError("An error has occurred where some collections and/or @Type annotated classes have the same name,"
					+ " but different return types. Prevent the use of the same names if the IDP types should be different.\n"
					+ "This issue should have been resolved in IDPStructure.class.\n" + 
					"Original error message: " + e.getMessage());
		}
	}
	
	private void fillMissingTypes(Map<Class<?>, Collection<?>> map, Class<?> cl, boolean function) 
			throws InvalidAnnotationException{
		if(map.get(cl) == null)	return;
		for(Field f : cl.getDeclaredFields()){
			f.setAccessible(true);
			if(f.isAnnotationPresent(ArgumentType.class) || (function && f.isAnnotationPresent(FunctionValue.class))){
				try{
					if(int.class.equals(f.getType()) || String.class.equals(f.getType())){
						String typeName = null;
						if(f.getAnnotation(ArgumentType.class) != null)
							typeName = f.getAnnotation(ArgumentType.class).typeName();
						else typeName = f.getAnnotation(FunctionValue.class).value();
						
						if("".equals(typeName))	throw new InvalidAnnotationException("An @ArgumentType annotated String or int attribute must be"
								+ " given a type name. Error occurred in class " + cl.getName() + " at"
								+ " attribute " + f.getName() + ".");
						if(int.class.equals(f.getType())){
							if(!this.typeIDPToClass.containsKey(typeName))
								this.typeIDPToClass.put(typeName, new IDPTypeInteger(new HashSet<Integer>(), typeName));
							Set<Integer> instantiatedTypes = ((IDPTypeInteger) this.typeIDPToClass.get(typeName)).getInstantiatedTypeIntegers();
							for(Object obj : map.get(cl)){
								if(!instantiatedTypes.contains(f.getInt(obj)))
									instantiatedTypes.add(f.getInt(obj));
							}
						} else if(String.class.equals(f.getType())){
							if(!this.typeIDPToClass.containsKey(typeName))
								this.typeIDPToClass.put(typeName, new IDPTypeString(new HashSet<String>(), typeName));
							Set<String> instantiatedTypes = ((IDPTypeString) this.typeIDPToClass.get(typeName)).getInstantiatedTypeStrings();
							for(Object obj : map.get(cl)){
								if(!instantiatedTypes.contains((String) f.get(obj)))
									instantiatedTypes.add((String) f.get(obj));
							}
						} 
					}
					else if(f.getType() instanceof Object){
						String typeName = f.getType().getSimpleName();
						IDPTypeID typeID = AnnotationChecker.getTypeIdentifier(f.getType());
						if(!this.typeIDPToClass.containsKey(typeName)){
							if(typeID == null || typeID == IDPTypeID.INTEGER)
								this.typeIDPToClass.put(typeName, new IDPTypeInteger(f.getType(), new HashSet<>(), typeName));
							else this.typeIDPToClass.put(typeName, new IDPTypeString(f.getType(), new HashSet<>(), typeName));
						}
						Set<Object> instantiatedTypes = this.typeIDPToClass.get(typeName).getInstantiatedTypeObjects();
						for(Object obj : map.get(cl)){
							if(!instantiatedTypes.contains(f.get(obj)))
								instantiatedTypes.add(f.get(obj));
							
						}
					}
					else throw new InvalidAnnotationException("Only objects, String and int datatypes may be "
							+ "annotated with @ArgumentType. Error occurred in class " + cl.getName() + " at"
							+ " attribute " + f.getName() + ".");
					
					//
					if(AnnotationChecker.hasTypeAnnotation(cl)){
						if(AnnotationChecker.getTypeIdentifier(cl) == IDPTypeID.INTEGER || AnnotationChecker.getTypeIdentifier(cl) == null){
							if(!this.typeIDPToClass.containsKey(cl.getSimpleName()))
								this.typeIDPToClass.put(cl.getSimpleName(), 
										new IDPTypeInteger(cl, map.get(cl), cl.getSimpleName()));
							else this.typeIDPToClass.get(cl.getSimpleName()).getInstantiatedTypeObjects().addAll(map.get(cl));
						} else if(AnnotationChecker.getTypeIdentifier(cl) == IDPTypeID.STRING){
							if(!this.typeIDPToClass.containsKey(cl.getSimpleName()))
								this.typeIDPToClass.put(cl.getSimpleName(), 
										new IDPTypeString(cl, map.get(cl), cl.getSimpleName()));
							else this.typeIDPToClass.get(cl.getSimpleName()).getInstantiatedTypeObjects().addAll(map.get(cl));
						} else throw new InvalidAnnotationException("Type classes must have either a String or int attribute with the @TypeValue annotation. In the case of a "
								+ "type class using its hash value, no @TypeValue annotated attribute should be given. "
								+ "Error occurred in this class: " + cl.getName() + ".");
					}
					//
				} catch(IllegalAccessException e){
					throw new IDPCoreMappingError("An unexpected error occurred when casting attribute objects "
							+ "to their expected class. The error happened due to an error in the core implementation. "
							+ "The relevant error message that occurred: " + e.getMessage() + ". The error happened for "
							+ "the class: " + cl.getName() + ".");
				}
			}
		}
	}

	
}
