package idp.logic;

import idp.exceptions.FailedTempFileException;
import idp.exceptions.MissingResourceException;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * This class stores a IDP term. IDP terms can be stored by loading a String or by loading a file
 * (standard extension is .idpterm).
 * 
 * @author Alexander Tang
 *
 */
public class IDPTerm {
	private String term;
	
	private IDPTerm(String term){
		this.term = term;
	}

	/**
	 * Returns the string form of the idp term.
	 * 
	 * @return Returns the string form of the idp term.
	 */
	public String getTerm(){
		return this.term;
	}
	
	/**
	 * Creates a IDPTerm.class instance by entering the string of the idp term.
	 * 
	 * @param string IDP term in string form.
	 * @return Returns a IDPTerm.class instance which contains the idp term from the given string.
	 */
	public static IDPTerm loadString(String string) {
		return new IDPTerm(string);
	}

	/**
	 * Creates a IDPTerm.class instance by entering the path to the idp term file.
	 * 
	 * @param url The path to the idp term file.
	 * @return	Returns a IDPTerm.class instance that contains the contents of the given idp term file.
	 * @throws IOException Thrown when the contents of the given idp term file could not be read. Ensure
	 *					that the correct path is given.
	 * @throws FailedTempFileException Thrown when the given URL is invalid.
	 * @throws MissingResourceException Thrown when the IDP term file could not be found.
	 */
	public static IDPTerm loadFile(URL url) throws IOException, FailedTempFileException, MissingResourceException {
		try{
			Path path = null;
			FileSystem fs = null;
			String[] tempArray = null;
			try{
				tempArray = url.toURI().toString().split("!"); // required to deal with maven resource path
			} catch(NullPointerException e){
				throw new MissingResourceException("IDP term file could not be found.");
			}
			// see http://stackoverflow.com/questions/22605666/java-access-files-in-jar-causes-java-nio-file-filesystemnotfoundexception
			if(tempArray.length == 1){
				URI uri = url.toURI();
				path = Paths.get(uri);
			}
			else{
				fs = IDPFile.initFileSystem(URI.create(tempArray[0]));
				path = fs.getPath(tempArray[1]);
			}
			byte[] encoded = Files.readAllBytes(path);
			String fileContent = new String(encoded, StandardCharsets.UTF_8);
			if(fs != null)	fs.close();
			return new IDPTerm(fileContent);
		} catch(URISyntaxException e){
			throw new FailedTempFileException("The URL that points to the IDP term is invalid.\n" + e.getMessage());
		}
		
	}

}
