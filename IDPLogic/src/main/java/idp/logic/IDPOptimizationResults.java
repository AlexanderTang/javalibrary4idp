package idp.logic;

import java.util.Collection;

/**
 * <p>This class is a wrapper for the results from the optimization inferences 
 * <b>Minimize</b> and <b>Maximize</b>. It contains the collection of models that fulfill 
 * the optimization, a boolean to indicate whether at least one model has been found and an 
 * integer to represent the optimal value.</p>
 * 
 * @author Alexander Tang
 *
 */
public class IDPOptimizationResults {
	private Collection<IDPStructure> structures;
	private boolean optimumFound;
	private int optimalValue;
	
	/**
	 * Initializes a new IDPOptimizationResults.class instance.
	 * 
	 * @param structures Collection of models (IDP structures) that fulfill the optimization inference.
	 * 					This collection is empty if no models are found.
	 * @param optimumFound	Returns true if an optimum has been found, and false otherwise.
	 * @param optimalValue	Returns the value of the optimal value for the optimization inference.
	 * 						Returns 0 if no optimum has been found.
	 */
	public IDPOptimizationResults(Collection<IDPStructure> structures, boolean optimumFound, int optimalValue){
		this.structures = structures;
		this.optimumFound = optimumFound;
		this.optimalValue = optimalValue;
	}

	/**
	 * Returns the collection of models computed by the optimization inference. All the models
	 * satisfy the optimization.
	 * 
	 * @return	Returns the collection of models computed by the optimization inference.
	 */
	public Collection<IDPStructure> getStructures() {
		return this.structures;
	}

	/**
	 * Returns true if an optimal model has been found and returns false otherwise.
	 * 
	 * @return	Returns true if an optimal model has been found and returns false otherwise.
	 */
	public boolean optimumFound() {
		return this.optimumFound;
	}

	/**
	 * Returns the optimal value. Returns 0 if no optimum exists.
	 * 
	 * @return	Returns the optimal value. Returns 0 if no optimum exists.
	 */
	public int getOptimalValue() {
		return this.optimalValue;
	}

}
