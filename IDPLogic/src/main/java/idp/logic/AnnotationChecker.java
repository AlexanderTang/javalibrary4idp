package idp.logic;

import idp.annotations.ArgumentType;
import idp.annotations.Function;
import idp.annotations.FunctionValue;
import idp.annotations.Predicate;
import idp.annotations.Type;
import idp.annotations.TypeValue;
import idp.elements.IDPTypeID;
import idp.exceptions.InvalidAnnotationException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class AnnotationChecker {

	public static boolean hasPredicateAnnotation(Class<?> cl){
		Annotation[] annotations = cl.getAnnotations();
		for(Annotation annotation : annotations){
			if(annotation instanceof Predicate)	return true;
		}
		return false;
	}

	public static boolean hasFunctionAnnotation(Class<?> cl) throws InvalidAnnotationException{
		Annotation[] annotations = cl.getAnnotations();
		for(Annotation annotation : annotations){
			if(annotation instanceof Function){
				if(hasFunctionValueAnnotation(cl))	return true;
				else throw new InvalidAnnotationException("A function must have one value. "
						+ "Error occurred in this class: " + cl.getName() + ".");
			}
		}
		return false;
	}
	
	public static boolean hasFunctionValueAnnotation(Class<?> cl) throws InvalidAnnotationException{
		Field field = null;
		for(Field f : cl.getDeclaredFields()){
			if(f.isAnnotationPresent(FunctionValue.class)){
				if(f.isAnnotationPresent(ArgumentType.class))
					throw new InvalidAnnotationException("A @FunctionValue annotated attribute cannot be"
							+ " annotated by @ArgumentType as well. Error occurred in the class " 
							+ cl.getName() + " for the field " + f.getName() + ".");
				if(field == null)	field = f;
				else	throw new InvalidAnnotationException("A function cannot have more than one function value. "
						+ "Error occurred in this class: " + cl.getName() + ".");
			}
		}
		return field != null;
	}
	
	public static boolean hasTypeAnnotation(Class<?> cl) throws InvalidAnnotationException {
		if(!hasMaximumOneTypeValue(cl))	return false;
		for(Annotation annotation : cl.getAnnotations()){
			if(annotation instanceof Type)	{
				return true;
			}
		}
		return false;
	}
	
	public static boolean hasMaximumOneTypeValue(Class<?> cl) throws InvalidAnnotationException {
		Field field = null;
		for(Field f : cl.getDeclaredFields()){
			if(f.isAnnotationPresent(TypeValue.class)){
				if(field == null)	field = f;
				else	throw new InvalidAnnotationException("A type cannot have more than one type value. "
						+ "Error occurred in this class: " + cl.getName() + ".");
			}
		}
		return true;
	}
	
	public static IDPTypeID getTypeIdentifier(Class<?> cl) throws InvalidAnnotationException{
		if(hasTypeAnnotation(cl)){
			for(Field f : cl.getDeclaredFields()){
				if(f.isAnnotationPresent(TypeValue.class)){
					if(Integer.class.equals(f.getType()) || int.class.equals(f.getType()))
						return IDPTypeID.INTEGER;
					else if(String.class.equals(f.getType()))
						return IDPTypeID.STRING;
					else throw new InvalidAnnotationException("The @TypeValue annotation is only allowed for String or int attributes. "
						+ "Error occurred in this class: " + cl.getName() + ".");
				}
			}
			return null;
		} else throw new InvalidAnnotationException("The class must be annotated with @Type before a @TypeValue can be searched. Error "
				+ "occurred in this class: " + cl.getName() + ".");
	}
	
	
}
