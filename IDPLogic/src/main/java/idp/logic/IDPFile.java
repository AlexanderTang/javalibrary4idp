package idp.logic;

import idp.exceptions.FailedTempFileException;
import idp.exceptions.IDPCoreMappingError;
import idp.exceptions.InvalidAnnotationException;
import idp.exceptions.MissingResourceException;
import idp.procedure.IDPProcedure;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class IDPFile {
	private File file;
	private IDPMapper mapper;
	
	public IDPFile(IDPTheory theory, IDPStructure struc) 
			throws InvalidAnnotationException, FailedTempFileException, 
			IDPCoreMappingError, MissingResourceException{
		try {
			this.file = File.createTempFile("temp", ".idp"); // temp files end up in %AppData%\Local\Temp\
			this.file.deleteOnExit(); // delete temp file as JVM terminates
			this.mapper = new IDPMapper(struc);
			
			constructStructure(struc);
			constructTheory(theory);
		} catch (IOException e) {
			throw new FailedTempFileException("The temporary .idp file could not be created.\n" + e.getMessage());
		}
	}
	
	public IDPFile(IDPTheory theory, IDPStructure struc, IDPTerm term) 
			throws FailedTempFileException, InvalidAnnotationException,
			IDPCoreMappingError, MissingResourceException {
		this(theory, struc);
		constructTerm(term);
	}
	
	private void constructTheory(IDPTheory theory) throws FailedTempFileException, MissingResourceException{
		try(FileWriter fw = new FileWriter(getFile().getPath(), true);
		    BufferedWriter bw = new BufferedWriter(fw);
		    PrintWriter out = new PrintWriter(bw)){
			out.println("theory T : V {");
			for(URL url : theory.getTheoryFilePaths()){
				String[] tempArray = null;
				try{
					tempArray = url.toURI().toString().split("!"); // required to deal with maven resource path
				} catch(NullPointerException e){
					throw new MissingResourceException("IDP theory file could not be found.");
				}
				// see http://stackoverflow.com/questions/22605666/java-access-files-in-jar-causes-java-nio-file-filesystemnotfoundexception
				Path path = null;
				FileSystem fs = null;
				if(tempArray.length == 1){
					URI uri = url.toURI();
					path = Paths.get(uri);
				}
				else{
					fs = initFileSystem(URI.create(tempArray[0]));
					path = fs.getPath(tempArray[1]);
				}
				byte[] encoded = Files.readAllBytes(path);
				String fileContent = new String(encoded, StandardCharsets.UTF_8);
				out.println(fileContent);
				if(fs != null)	fs.close();
			}
			out.println("}\n");
		} catch (IOException | URISyntaxException e) {
			throw new FailedTempFileException("The temporary .idp file could not be opened.\n" + e.getMessage());
		} 
	}


	private void constructStructure(IDPStructure struc) 
			throws InvalidAnnotationException, IDPCoreMappingError, FailedTempFileException{
		try(FileWriter fw = new FileWriter(getFile().getPath(), true);
		    BufferedWriter bw = new BufferedWriter(fw);
		    PrintWriter out = new PrintWriter(bw)){
			out.println(createVocabulary(struc));
			out.println(createStructure(struc));
		} catch (IOException e) {
			throw new FailedTempFileException("The temporary .idp file could not be opened.\n" + e.getMessage());
		}
	}
	
	private String createVocabulary(IDPStructure struc){
		StringBuilder sb = new StringBuilder();
		sb.append("vocabulary V {\n");
		for(String typeName : mapper.getTypeMap().keySet())
			sb.append(this.mapper.getTypeMap().get(typeName).getVocabularyString() + "\n");
		for(String predicateName : mapper.getPredicateMap().keySet())
			sb.append(this.mapper.getPredicateMap().get(predicateName).getVocabularyString() + "\n");
		for(String functionName : mapper.getFunctionMap().keySet())
			sb.append(this.mapper.getFunctionMap().get(functionName).getVocabularyString() + "\n");
		sb.append("}\n");
		return sb.toString();
	}
	
	private String createStructure(IDPStructure struc) throws
			InvalidAnnotationException, IDPCoreMappingError{
		StringBuilder sb = new StringBuilder();
		sb.append("structure S : V {\n");
		for(String typeName : mapper.getTypeMap().keySet())
			sb.append(this.mapper.getTypeMap().get(typeName).getStructureString() + "\n");
		for(String predicateName : mapper.getPredicateMap().keySet())
			sb.append(this.mapper.getPredicateMap().get(predicateName).getStructureString() + "\n");
		for(String functionName : mapper.getFunctionMap().keySet())
			sb.append(this.mapper.getFunctionMap().get(functionName).getStructureString() + "\n");
		sb.append("}\n");
		return sb.toString();
	}
	
	public void constructMainProcedure(IDPProcedure<?> main) throws FailedTempFileException {
		try(FileWriter fw = new FileWriter(getFile().getPath(), true);
		    BufferedWriter bw = new BufferedWriter(fw);
		    PrintWriter out = new PrintWriter(bw)){
			out.println(main.getMainProcedure(this.mapper));
		} catch (IOException e) {
			throw new FailedTempFileException("The temporary .idp file could not be opened.\n" + e.getMessage());
		}
	}
	
	private void constructTerm(IDPTerm term) throws FailedTempFileException{
		try(FileWriter fw = new FileWriter(getFile().getPath(), true);
		    BufferedWriter bw = new BufferedWriter(fw);
		    PrintWriter out = new PrintWriter(bw)){
			StringBuilder sb = new StringBuilder();
			sb.append("term Term : V {");
			sb.append(System.lineSeparator());
			sb.append(term.getTerm());
			sb.append(System.lineSeparator());
			sb.append("}");
			sb.append(System.lineSeparator());
			out.println(sb.toString());
		} catch (IOException e) {
			throw new FailedTempFileException("The temporary .idp file could not be opened.\n" + e.getMessage());
		}
	}
	
	public File getFile(){
		return this.file;
	}
	
	public IDPMapper getMapper(){
		return this.mapper;
	}
	
	static FileSystem initFileSystem(URI uri) throws IOException {
	    try
	    {
	        return FileSystems.getFileSystem(uri);
	    }
	    catch( FileSystemNotFoundException e )
	    {
	        Map<String, String> env = new HashMap<>();
	        env.put("create", "true");
	        return FileSystems.newFileSystem(uri, env);
	    }
	}
}
