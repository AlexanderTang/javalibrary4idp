package idp.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>This annotation is used to mark a field as a IDP function value, which is used by 
 * the corresponding {@literal @Function} annotated Java class.</p>
 * 
 * <p>Exactly one field of the {@literal @Function} annotated class needs to be annotated by a {@literal @FunctionValue} 
 * annotation since every IDP function requires one function value.</p>
 * 
 * @author Alexander Tang
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FunctionValue {
	/**
	 * This denotes the name of the IDP type. The field that this annotation is attached to should
	 * be of the class: String.class, Integer.class or int.class. Otherwise, this variable should
	 * keep its default value, which is the empty string.
	 * 
	 * @return Returns the name of the IDP type of the field. Returns an empty string if the
	 * 			field is not of the class: String.class, Integer.class or int.class.
	 */
	String value() default "";
}
