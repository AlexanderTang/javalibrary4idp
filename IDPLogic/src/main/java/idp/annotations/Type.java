package idp.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>This annotation is used to mark a Java class as a IDP type.</p>
 * 
 * <p>A maximum of one {@literal @TypeValue} annotated field is allowed for a given {@literal @Type} annotated class. The
 * {@literal @TypeValue} annotated field should be of the class: String.class, int.class or Integer.class.
 * If no fields are annotated by {@literal @TypeValue}, then the hash code of the field instance is going to
 * be used as the type value.</p>
 * 
 * @author Alexander Tang
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Type {

}
