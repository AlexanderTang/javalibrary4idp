package idp.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>This annotation is used to mark a Java class as a IDP function.</p>
 * 
 * <p>The fields containing the IDP types need to be annotated by {@literal @ArgumentType} and require their
 * order numbers to be specified. Ie. the field which represents the second argument is annotated
 * by {@literal @ArgumentType(2)}.</p>
 * 
 * <p>Exactly one field needs to be annotated by a {@literal @FunctionValue} annotation since every IDP function
 * requires one function value.</p>
 * 
 * <p>A constructor of the format (ArgType1, ArgType2,...,ArgTypeN, FuncValue) is
 * required, where ArgType1 corresponds with 
 * {@literal @ArgumentType(1)}. The ordering of the argument types denotes their ordering in the constructor. 
 * When two argument types have the same class, this exception will not be thrown when they are 
 * switched, but it will likely result in a wrong result. The {@literal @FunctionValue} needs to be 
 * appended at the end of the constructor (after all the argument types).</p>
 * 
 * @author Alexander Tang
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Function {

}
