package idp.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * <p>This annotation is used to mark a Java attribute as a IDP type value, which is used by 
 * the corresponding {@literal @Type} annotated Java class.</p>
 * 
 * <p>A maximum of one {@literal @TypeValue} annotated field is allowed for a given {@literal @Type} annotated class. The
 * {@literal @TypeValue} annotated field should be of the class: String.class, int.class or Integer.class.</p>
 * 
 * @author Alexander Tang
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TypeValue {
}
