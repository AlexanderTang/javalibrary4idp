package idp.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>This annotation is used to mark a field as a IDP type, which is used by 
 * the corresponding {@literal @Predicate} or {@literal @Function} annotated Java class.</p>
 * 
 * @author Alexander Tang
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ArgumentType {
	/**
	 * This value denotes the ordering of the argument type. Ie. if the value equals 1, then
	 * the field that this annotation is attached to is the first argument of the corresponding
	 * predicate or function.
	 * 
	 * @return Returns the ordering number of the field.
	 */
	int value();
	
	/**
	 * This denotes the name of the IDP type. The field that this annotation is attached to should
	 * be of the class: String.class, Integer.class or int.class. Otherwise, this variable should
	 * keep its default value, which is the empty string.
	 * 
	 * @return Returns the name of the IDP type of the field. Returns an empty string if the
	 * 			field is not of the class: String.class, Integer.class or int.class.
	 */
	String typeName() default "";
}
