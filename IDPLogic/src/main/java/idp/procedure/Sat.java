package idp.procedure;


public class Sat extends BooleanReader {

	public Sat(ProcedureParameters procParams) {
		super(procParams);
	}

	@Override
	protected String getMainFunctionText() {
		return "print(sat(T,S))\n";
	}
}
