package idp.procedure;

import idp.exceptions.IDPExecutionException;
import idp.exceptions.InvalidConstructorException;
import idp.logic.IDPMapper;
import idp.logic.IDPOptimizationResults;
import idp.logic.IDPStructure;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public abstract class OptimizationReader extends IDPProcedure<IDPOptimizationResults>{

	public OptimizationReader(ProcedureParameters procParams) {
		super(procParams, false);
	}

	@Override
	protected IDPOptimizationResults readOutput(List<String> outputLines, IDPMapper mapper) 
			throws IDPExecutionException, InvalidConstructorException{
		boolean optimumFound = BooleanReader.readBooleanOutput(outputLines.subList(0, 1));
		if(!optimumFound)
			return new IDPOptimizationResults(new HashSet<IDPStructure>(), optimumFound, 0);
		int value = Integer.parseInt(outputLines.get(1)) * modifier();
		Collection<IDPStructure> structures = StructureCollectionReader.
				readCollectionOutput(outputLines.subList(2, outputLines.size()), 
						mapper, this.predicates, this.functions);
		return new IDPOptimizationResults(structures, optimumFound, value);
	}
	
	protected abstract int modifier();
	
	@Override
	protected String getMainFunctionText() {
		StringBuilder sb = new StringBuilder();
		String allResults = "Models";
		sb.append(allResults + ",Boolean,Value = " + getInferenceText() + System.lineSeparator());
		sb.append("print(Boolean)" + System.lineSeparator());
		sb.append("print(Value)" + System.lineSeparator());
		sb.append("for i=1,#" + allResults + " do" + System.lineSeparator());
		sb.append("print(i)" + System.lineSeparator());
		sb.append(getResultingStructureName() + " = " + allResults + "[i]" + System.lineSeparator());
		for(String name : this.predicates)
			sb.append(getStructureSolutionLines(name, false));
		for(String name : this.functions)
			sb.append(getStructureSolutionLines(name, true));
		sb.append("end" + System.lineSeparator());
		return sb.toString();
	}
	
	protected abstract String getInferenceText();

}
