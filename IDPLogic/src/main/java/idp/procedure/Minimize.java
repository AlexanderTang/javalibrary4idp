package idp.procedure;

public class Minimize extends OptimizationReader {

	public Minimize(ProcedureParameters procParams, int nbModels){
		super(procParams);
		addOptionalParameterLine("stdoptions.nbmodels = " + nbModels);
	}

	@Override
	protected int modifier() {
		return 1;
	}
	
	@Override
	protected String getInferenceText() {
		return "minimize(T,S,Term)";
	}
}
