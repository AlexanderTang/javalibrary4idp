package idp.procedure;

public class CalculateDefinitions extends StructureReader {

	public CalculateDefinitions(ProcedureParameters procParams) {
		super(procParams);
	}

	@Override
	protected String getInferenceText() {
		return "calculatedefinitions(T,S)";
	}
}
