package idp.procedure;

import idp.exceptions.IDPExecutionException;
import idp.logic.IDPMapper;

import java.util.List;


public abstract class BooleanReader extends IDPProcedure<Boolean>{

	public BooleanReader(ProcedureParameters procParams) {
		super(procParams, false);
	}

	@Override
	protected Boolean readOutput(List<String> outputLines, IDPMapper mapper) 
			throws IDPExecutionException{
		return readBooleanOutput(outputLines);
	}
	
	public static Boolean readBooleanOutput(List<String> outputLines) 
			throws IDPExecutionException{
		if(outputLines.get(0).startsWith("true"))
			return true;
		else if(outputLines.get(0).startsWith("false"))
			return false;
		else throw new IDPExecutionException(produceIDPUnexpectedOutputMessage(outputLines));
	}
}
