package idp.procedure;

public class ModelExpand extends StructureCollectionReader {
	
	public ModelExpand(ProcedureParameters procParams, int nbModels){
		super(procParams, nbModels);
	}

	@Override
	protected String getInferenceText() {
		return "modelexpand(T,S)";
	}
}
