package idp.procedure;

import idp.exceptions.IDPExecutionException;
import idp.exceptions.InvalidConstructorException;
import idp.exceptions.NilStructureException;
import idp.logic.IDPMapper;
import idp.logic.IDPStructure;

import java.util.List;

public abstract class StructureReader extends IDPProcedure<IDPStructure>{

	public StructureReader(ProcedureParameters procParams) {
		super(procParams, true);
	}

	@Override
	protected IDPStructure readOutput(List<String> outputLines, IDPMapper mapper) 
			throws IDPExecutionException, NilStructureException, InvalidConstructorException{
		if(outputLines.get(0).startsWith("true"))
			return new StructureFactory().createStructure(outputLines.subList(1, outputLines.size()), 
					mapper, this.predicates, this.functions);
		else throw new NilStructureException();
	}
	
	@Override
	protected String getMainFunctionText() {
		StringBuilder sb = new StringBuilder();
		sb.append(getResultingStructureName() + " = " + getInferenceText() + System.lineSeparator());
		for(String name : this.predicates)
			sb.append(getStructureSolutionLines(name, false));
		for(String name : this.functions)
			sb.append(getStructureSolutionLines(name, true));
		return sb.toString();
	}
	
	protected abstract String getInferenceText();
	
}
