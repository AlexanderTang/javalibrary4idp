package idp.procedure;

public class GroundPropagate extends StructureReader {

	public GroundPropagate(ProcedureParameters procParams) {
		super(procParams);
	}

	@Override
	protected String getInferenceText() {
		return "groundpropagate(T,S)";
	}
}
