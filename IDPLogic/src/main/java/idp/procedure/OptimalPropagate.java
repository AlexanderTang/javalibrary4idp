package idp.procedure;

public class OptimalPropagate extends StructureReader {

	public OptimalPropagate(ProcedureParameters procParams) {
		super(procParams);
	}

	@Override
	protected String getInferenceText() {
		return "optimalpropagate(T,S)";
	}
}
