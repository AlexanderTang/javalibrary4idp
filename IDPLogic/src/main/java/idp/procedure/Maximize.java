package idp.procedure;

public class Maximize extends OptimizationReader {
	
	public Maximize(ProcedureParameters procParams, int nbModels){
		super(procParams);
		addOptionalParameterLine("stdoptions.nbmodels = " + nbModels);
	}
	
	@Override
	protected int modifier() {
		return -1;
	}

	@Override
	protected String getInferenceText() {
		return "maximize(T,S,Term)";
	}
	
}
