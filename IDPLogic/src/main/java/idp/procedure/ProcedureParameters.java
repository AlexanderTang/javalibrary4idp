package idp.procedure;

public class ProcedureParameters {
	private long executionTimeOut; // in milliseconds; enter a number <= 0 to indicate infinite execution time
	private String idpExecPath;
	
	public ProcedureParameters(long execTimeOut, String idpExecPath){
		if(execTimeOut > 0)
			this.executionTimeOut = execTimeOut;
		else this.executionTimeOut = -1; // infinite time out
		this.idpExecPath = idpExecPath;
	}

	public long getExecutionTimeOut(){
		return this.executionTimeOut;
	}
	
	public String getIDPExecutionPath(){
		return this.idpExecPath;
	}
}
