package idp.procedure;

import idp.exceptions.IDPExecutionException;
import idp.exceptions.InvalidConstructorException;
import idp.exceptions.NilStructureException;
import idp.logic.IDPMapper;
import idp.logic.IDPStructure;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class StructureCollectionReader extends IDPProcedure<Collection<IDPStructure>> {
	protected int nbOfModels;

	public StructureCollectionReader(ProcedureParameters procParams, int numberOfModels) {
		super(procParams, true);
		this.nbOfModels = numberOfModels;
		addOptionalParameterLine("stdoptions.nbmodels = " + this.nbOfModels);
	}

	@Override
	protected Collection<IDPStructure> readOutput(List<String> outputLines, IDPMapper mapper) 
			throws IDPExecutionException, NilStructureException, InvalidConstructorException{
		if(outputLines.get(0).startsWith("true"))
			return readCollectionOutput(outputLines.subList(1, outputLines.size()), 
					mapper, this.predicates, this.functions);
		else throw new NilStructureException();
	}
	
	public static Collection<IDPStructure> readCollectionOutput(List<String> outputLines, 
			IDPMapper mapper, List<String> predicates, List<String> functions) 
					throws IDPExecutionException, InvalidConstructorException{
		List<IDPStructure> structures = new ArrayList<>();
		StructureFactory sf = new StructureFactory();
		int modelCounter = 1;
		int lineCounter = 0;
		while(lineCounter < outputLines.size() && 
				outputLines.get(lineCounter).startsWith(String.valueOf(modelCounter))){
			modelCounter++;
			lineCounter++;
			int amountOfLinesToParse = (predicates.size() + functions.size())*2;
			structures.add(sf.createStructure(outputLines.subList(lineCounter, lineCounter+amountOfLinesToParse), 
					mapper, predicates, functions));
			lineCounter += amountOfLinesToParse;
		}
		return structures;
	}
	
	@Override
	protected String getMainFunctionText() {
		StringBuilder sb = new StringBuilder();
		String allResults = getResultingStructureName() + "All";
		sb.append(allResults + " = " + getInferenceText() + System.lineSeparator());
		sb.append("for i=1,#" + allResults + " do" + System.lineSeparator());
		sb.append("print(i)" + System.lineSeparator());
		sb.append(getResultingStructureName() + " = " + allResults + "[i]" + System.lineSeparator());
		for(String name : this.predicates)
			sb.append(getStructureSolutionLines(name, false));
		for(String name : this.functions)
			sb.append(getStructureSolutionLines(name, true));
		sb.append("end" + System.lineSeparator());
		return sb.toString();
	}
	
	protected abstract String getInferenceText();
}
