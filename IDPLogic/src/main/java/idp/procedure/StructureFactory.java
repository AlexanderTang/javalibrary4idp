package idp.procedure;

import idp.elements.IDPFunction;
import idp.elements.IDPPredicate;
import idp.elements.IDPType;
import idp.elements.IDPTypeID;
import idp.elements.IDPTypeInteger;
import idp.elements.IDPTypeString;
import idp.elements.ThreeValuedIndicator;
import idp.exceptions.IDPCoreMappingError;
import idp.exceptions.IDPExecutionException;
import idp.exceptions.InvalidConstructorException;
import idp.exceptions.InvalidStructureException;
import idp.logic.IDPMapper;
import idp.logic.IDPStructure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class StructureFactory {
	
	public IDPStructure createStructure(List<String> outputLines, IDPMapper mapper, 
			List<String> predicates, List<String> functions) 
					throws IDPExecutionException, InvalidConstructorException {
		IDPStructureUnrestricted struc = new IDPStructureUnrestricted();
		int lineCounter = 0;
		String possiblyTrueLine, possiblyFalseLine;
		for(String predicateName : predicates){
			possiblyTrueLine = removeWhiteSpaces(outputLines.get(lineCounter++));
			possiblyFalseLine = removeWhiteSpaces(outputLines.get(lineCounter++));
			addToIDPStructure(mapper, predicateName, possiblyTrueLine, possiblyFalseLine, struc, false);
		}
		for(String functionName : functions){
			possiblyTrueLine = removeWhiteSpaces(outputLines.get(lineCounter++));
			possiblyFalseLine = removeWhiteSpaces(outputLines.get(lineCounter++));
			addToIDPStructure(mapper, functionName, possiblyTrueLine, possiblyFalseLine, struc, true);
		}
		for(IDPType type : mapper.getTypeMap().values()){
			if(type.getInstantiatedTypeObjects() != null)
				struc.unrestrictedOverrideCollection(type.getInstantiatedTypeObjects(), 
						type.getElementClass(), null);
			else{
				if(type.getType() == IDPTypeID.INTEGER)
					struc.unrestrictedOverrideTypeCollection(
							((IDPTypeInteger) type).getInstantiatedTypeIntegers(), type.getElementName());
				else struc.unrestrictedOverrideTypeCollection(
						((IDPTypeString) type).getInstantiatedTypeStrings(), type.getElementName());
			}
		}
		return struc;
	}
	
	private String removeWhiteSpaces(String line){
		return line.replaceAll("\\s+","");
	}
	
	private List<String> getLineValues(String line){
		if("{}".equals(line))
			return new ArrayList<>();
		line = line.substring(1, line.length()-1);
		return Arrays.asList(line.split(";"));
	}
	
	private Set<Object> getPredicateObjects(IDPMapper mapper, String predicateName, 
			List<String> values, Collection<Object> newInstancedPredicates)
					throws InvalidConstructorException{
		IDPPredicate predicate = mapper.getPredicateMap().get(predicateName);
		Set<Object> predicateInstances = new HashSet<>();
		for(String value : values)
			predicateInstances.add(predicate.getInstance(
					getPredicateArguments(Arrays.asList(value.split(","))), 
					mapper, newInstancedPredicates));
		return predicateInstances;
	}
	
	private Set<Object> getPredicateObjects(IDPMapper mapper, String predicateName, List<String> values) 
			throws InvalidConstructorException{
		return getPredicateObjects(mapper, predicateName, values, new HashSet<>());
	}
	
	private Set<Object> getFunctionObjects(IDPMapper mapper, String functionName, List<String> values,
			Collection<Object> newInstancedFunctions)
					throws IDPExecutionException, InvalidConstructorException{
		IDPFunction function = mapper.getFunctionMap().get(functionName);
		Set<Object> functionInstances = new HashSet<>();
		for(String value : values){
			List<String> allArgs = Arrays.asList(value.split(","));
			String fValue = allArgs.get(allArgs.size()-1);
			allArgs = allArgs.subList(0, allArgs.size()-1);
			functionInstances.add(function.getInstance(getPredicateArguments(allArgs), 
					normalizeString(fValue), mapper,
					newInstancedFunctions));
		}
		return functionInstances;
	}
	
	private Set<Object> getFunctionObjects(IDPMapper mapper, String functionName, List<String> values) 
			throws IDPExecutionException, InvalidConstructorException{
		return getFunctionObjects(mapper, functionName, values, new HashSet<>());
	}
	
	private List<String> getPredicateArguments(List<String> arguments){
		List<String> normalizedArguments = new ArrayList<>();
		for(String arg : arguments)
			normalizedArguments.add(normalizeString(arg));
		return normalizedArguments;
	}
	
	private String normalizeString(String s){
		if(s.startsWith("\"") && s.endsWith("\""))
			return s.substring(1, s.length()-1);
		else return s;
	}
	
	private void addToIDPStructure(IDPMapper mapper, String name, String possiblyTrueLine,
			String possiblyFalseLine, IDPStructureUnrestricted struc, boolean isFunction) 
					throws IDPExecutionException, InvalidConstructorException{
		Set<Object> certainlyTrueSet = new HashSet<>();
		Set<Object> certainlyFalseSet = new HashSet<>();
		Set<Object> unknownSet = new HashSet<>();
		Set<Object> possiblyTrueSet = null;
		Set<Object> possiblyFalseSet = null;
		if(isFunction){
			possiblyTrueSet = getFunctionObjects(mapper, name, getLineValues(possiblyTrueLine));
			possiblyFalseSet = getFunctionObjects(mapper, name, getLineValues(possiblyFalseLine),
					possiblyTrueSet);
		}
		else{
			possiblyTrueSet = getPredicateObjects(mapper, name, getLineValues(possiblyTrueLine));
			possiblyFalseSet = getPredicateObjects(mapper, name, getLineValues(possiblyFalseLine),
					possiblyTrueSet);
		}
				
		for(Object o : possiblyTrueSet){
			if(possiblyFalseSet.contains(o)){
				possiblyFalseSet.remove(o);
				unknownSet.add(o);
			} else	certainlyTrueSet.add(o);
		}
		certainlyFalseSet.addAll(possiblyFalseSet);
		
		addCollectionsToStructure(struc, certainlyTrueSet, certainlyFalseSet, unknownSet, mapper, name);
	}
	
	@SuppressWarnings("unchecked")
	private <T> void addCollectionsToStructure(IDPStructureUnrestricted struc, Set<T> certainlyTrueSet,
			Set<T> certainlyFalseSet, Set<T> unknownSet, IDPMapper mapper, String name){
		Class<T> cl = null;
		if(mapper.getPredicateMap().containsKey(name))
			cl = (Class<T>) mapper.getPredicateMap().get(name).getElementClass();
		else cl = (Class<T>) mapper.getFunctionMap().get(name).getElementClass();
		
		struc.unrestrictedOverrideCollection(certainlyTrueSet, cl, ThreeValuedIndicator.CT);
		struc.unrestrictedOverrideCollection(certainlyFalseSet, cl, ThreeValuedIndicator.CF);
		struc.unrestrictedOverrideCollection(unknownSet, cl, ThreeValuedIndicator.U);
		if(unknownSet.isEmpty())
			struc.unrestrictedOverrideCollection(certainlyTrueSet, cl, null);
		else	struc.unrestrictedOverrideCollection(null, cl, null);
	}
	
	
	public class IDPStructureUnrestricted extends IDPStructure{
		
		private IDPStructureUnrestricted(){
		}
		
		// This method (and class) exists due to Java 8 package visibility limitations.
		@SuppressWarnings({ "rawtypes", "unchecked" })
		@Override
		protected void unrestrictedOverrideCollection(Collection col, 
				Class cl, ThreeValuedIndicator tvi){
			super.unrestrictedOverrideCollection(col, cl, tvi);
		}
		
		private void unrestrictedOverrideTypeCollection(Collection<?> col, String name){
			try {
				addTypeCollection(col, name);
			} catch (InvalidStructureException e) {
				throw new IDPCoreMappingError("An exception occurred when parsing IDP results. A type"
						+ " that existed in the input structure cannot be added to the resulting structure."
						+ " This is most likely a bug in the implementation of the IDP mapper and should"
						+ " not have occurred. The error occurred for the collection name: " + name + ".\n"
						+ " The error message was: " + e.getMessage());
			}
		}
	}
}
