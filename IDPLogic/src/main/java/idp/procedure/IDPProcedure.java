package idp.procedure;

import idp.exceptions.FailedTempFileException;
import idp.exceptions.IDPCoreMappingError;
import idp.exceptions.IDPExecutionException;
import idp.exceptions.InvalidConstructorException;
import idp.exceptions.NilStructureException;
import idp.logic.IDPFile;
import idp.logic.IDPMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.LogOutputStream;
import org.apache.commons.exec.PumpStreamHandler;

public abstract class IDPProcedure<T> {
	private String optionalParameters;
	private long executionTimeOut;
	private List<String> warningLines;
	private String idpExecPath;
	protected boolean verifyConsistentStructure;
	protected List<String> predicates;
	protected List<String> functions;
	
	public IDPProcedure(ProcedureParameters procParams, boolean verifyConsistentStructure){
		this.optionalParameters = "";
		this.executionTimeOut = procParams.getExecutionTimeOut();
		this.idpExecPath = procParams.getIDPExecutionPath();
		this.verifyConsistentStructure = verifyConsistentStructure;
		this.warningLines = new ArrayList<>();
	}
	
	public T getResults(IDPFile file, StringBuilder warningLog) 
			throws FailedTempFileException, IDPExecutionException, NilStructureException, 
			InvalidConstructorException{
		file.constructMainProcedure(this);
		CollectingLogOutputStream logOutput = executeIDP(file, warningLog);
		
		if(logOutput.getLines() != null && logOutput.getLines().size() != 0)
			return readOutput(logOutput.getLines(), file.getMapper());
		else throw new IDPExecutionException(produceIDPWarningMessage(this.warningLines));
	}
	
	protected void addWarningLog(StringBuilder warningLog){
		// create header
		warningLog.append(this.getClass().getSimpleName() + "\n");
		for(int i = 0; i < this.getClass().getSimpleName().length(); i++)
			warningLog.append("-");
		warningLog.append(System.lineSeparator());
		
		// add warning content
		for(String warningLine : this.warningLines)
			warningLog.append(warningLine + "\n");
		warningLog.append(System.lineSeparator());
	}
	
	protected abstract T readOutput(List<String> output, IDPMapper mapper) 
			throws IDPExecutionException, NilStructureException, InvalidConstructorException;
	
	public String getMainProcedure(IDPMapper mapper){
		this.predicates = new ArrayList<>(mapper.getPredicateMap().keySet());
		this.functions = new ArrayList<>(mapper.getFunctionMap().keySet());
		
		StringBuilder sb = new StringBuilder();
		sb.append("procedure main() {" + System.lineSeparator());
		sb.append(this.optionalParameters);
		if(this.verifyConsistentStructure)
			sb.append("print(sat(T,S))" + System.lineSeparator());
		sb.append(getMainFunctionText());
		sb.append("}" + System.lineSeparator());
		return sb.toString();
	}
	
	protected void addOptionalParameterLine(String parLine){
		this.optionalParameters += parLine + "\n";
	}
	
	protected abstract String getMainFunctionText();
	
	protected CollectingLogOutputStream executeIDP(IDPFile file, StringBuilder warningLog) 
			throws IDPExecutionException{
		CollectingLogOutputStream logOutput = new CollectingLogOutputStream(); // this replaces the standard System.out
		CollectingLogOutputStream errorOutput = new CollectingLogOutputStream(); // captures the error stream
		try {
			CommandLine cmdLine = new CommandLine(this.idpExecPath);
			cmdLine.addArgument("-e");
			cmdLine.addArgument("main()");
			cmdLine.addArgument("${file}");
			HashMap<String, File> map = new HashMap<>();
			map.put("file", file.getFile());
			cmdLine.setSubstitutionMap(map);
			
			DefaultExecutor executor = new DefaultExecutor();
			executor.setExitValue(0);
			ExecuteWatchdog watchdog = new ExecuteWatchdog(this.executionTimeOut); // idp execution times out after x milliseconds
			executor.setWatchdog(watchdog);

			errorOutput.flush();
			this.warningLines = errorOutput.getLines();
			addWarningLog(warningLog);
			
		    PumpStreamHandler psh = new PumpStreamHandler(logOutput, errorOutput);
		    executor.setStreamHandler(psh);
			executor.execute(cmdLine);
			
			logOutput.flush();
			return logOutput;
		} catch (IOException e) {
			errorOutput.flush();
			this.warningLines = errorOutput.getLines();
			addWarningLog(warningLog);
			throw new IDPExecutionException("Could not execute the IDP command line. Execution may have timed out.\n" 
					+ e.getMessage() + "\n" + produceIDPWarningMessage(this.warningLines));
		} finally{
			try {
				errorOutput.close();
				logOutput.close();
			} catch (IOException e) {
				throw new IDPCoreMappingError("Could not close IDP streams. " + e.getMessage());
			}
		}
	}
	
	static String produceIDPUnexpectedOutputMessage(List<String> outputLines){
		StringBuilder errorMsg = new StringBuilder();
		errorMsg.append("Unexpected results received from executing IDP.\n");
		errorMsg.append("Received output lines were:\n");
		for(String outputLine : outputLines){
			errorMsg.append(outputLine + "\n");
		}
		return errorMsg.toString();
	}
	
	static String produceIDPWarningMessage(List<String> warningLines){
		StringBuilder errorMsg = new StringBuilder();
		errorMsg.append("No output received when executing IDP.\n");
		errorMsg.append("Received warning lines were:\n");
		for(String warningLine : warningLines){
			errorMsg.append(warningLine + "\n");
		}
		return errorMsg.toString();
	}
	

	protected String getResultingStructureName(){
		return "Result";
	}
	
	protected String getStructureSolutionLines(String name, boolean function){
		return getSolutionLine(name, function, true) + 
				getSolutionLine(name, function, false);
	}

	/**
	 * Predicates and functions can be retrieved using the following format in IDP:
	 * <Resulting Structure>[<vocabulary>::<predicate or function>][OPTIONAL .graph][.pt or pf]
	 * The input parameter function indicates that a function is given if true and a predicate if false.
	 * 
	 * @param name	The name of a predicate or function class.
	 * @param function	True if a function is given, false if a predicate is given.
	 * @param possiblyTrue	True if we look for all 'possibly true' values and false if we look for
	 * 						all 'possibly false' values.
	 * @return	Returns the String line to output the desired solution component, which is of the form:
	 * 		<Resulting Structure>[<vocabulary>::<predicate or function>][OPTIONAL .graph][.pt or pf]
	 */
	protected String getSolutionLine(String name, boolean function, boolean possiblyTrue){
		StringBuilder sb = new StringBuilder();
		sb.append("print(" + getResultingStructureName() + "[V::" + name + "]");
		if(function)
			sb.append(".graph");
		if(possiblyTrue)
			sb.append(".pt");
		else sb.append(".pf");
		sb.append(")" + System.lineSeparator());
		return sb.toString();
	}
	
	protected class CollectingLogOutputStream extends LogOutputStream {
	    private final List<String> lines = new LinkedList<String>();
	    
	    protected CollectingLogOutputStream() {
		}
	    
	    @Override 
	    protected void processLine(String line, int level) {
	        this.lines.add(line);
	    }   
	    
	    protected void processLine(String line){
	    	processLine(line, 0);
	    }
	    
	    protected List<String> getLines() {
	        return this.lines;
	    }
	}
}
