package idp.procedure;

public class Propagate extends StructureReader {

	public Propagate(ProcedureParameters procParams) {
		super(procParams);
	}
	
	@Override
	protected String getInferenceText(){
		return "propagate(T,S)";
	}
}
