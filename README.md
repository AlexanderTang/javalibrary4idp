# IDP Solver Library

## Repository Structure

Each of the subdirectories contains a separate Java project:

* **IDPLogic:** Contains the IDP Solver implementation. This library is required for the examples to run.
* **IDPExamples:** Contains various implemented examples (N-coloring, reachability, Sudoku). The examples show all kinds of different ways to use the library.
* **tutorial.pdf:** A useful step-by-step guide that introduces the library in a more beginner-friendly manner.
* **IDPDerbyDatabaseExample:** Contains a larger example which involves a relational database as an extra layer. The flow is as follows:
     1. Create Java objects from database table rows,
     2. Map Java objects to IDP input structure,
     3. Execute IDP inference method(s),
     4. Map IDP results to Java objects,
     5. (Optional) Map Java objects to database table rows. 

## Setup Guide

### Standard Requirements

The following tools are required for all 3 Java projects:

1. Java 8 *(may work with Java 7, but this is untested)*
2. [IDP 3.6.0](https://dtai.cs.kuleuven.be/software/idp/try)

### IDPLogic

This section will explain how to build the library. First ensure that the path inside `src/test/resources/idp.config` points to the IDP executable. Otherwise running the unit tests will fail. The Maven setup will not build successfully then.

#### Maven Setup

In the directory with `pom.xml`, run `mvn package` in the terminal. This will output the following in the `target/` directory:

* **idpmapper-3.6.0.jar** with corresponding javadoc and sources jars. This is the IDP Solver library.
* **dependency-jars/** contains the dependencies.
* **apidocs/** contains javadoc in html format.

#### Manual Setup

1. Create a Java project with the `src/` folder.
2. Download the dependency [commons-exec-1.3](https://commons.apache.org/proper/commons-exec/download_exec.cgi)
3. [Create a jar](https://www.cs.utexas.edu/~scottm/cs307/handouts/Eclipse%20Help/jarInEclipse.htm)

### IDPExamples

Create a Java project with the `src/` folder. [Add the `idpmapper-3.6.0.jar` and its dependencies as project libraries.](http://www.oxfordmathcenter.com/drupal7/node/44)

### IDPDerbyDatabaseExample

#### Maven Setup

1. The IDP path should point to the IDP executable in `src/main/resources/idp.config`.
2. The path to the local database directory should be entered in:
	1. `src/main/resources/META-INF/persistence.xml` inside the value of property `jdbc.url`.
	2. `src/main/java/studentscheduling/Run.java` as an argument to `db.createDatabase/1`.
3. Ensure that `local_dependencies/` contains `idpmapper-3.6.0.jar`. This jar file is pre-built; it *should* be replaced by your own built version.
4. In the directory with `pom.xml`, run `mvn package` in the terminal. This will output a runnable jar `databaseapp-3.6.0.jar` in the `target/` directory.
5. In directory `IDPDerbyDatabaseExample/target/`, run: 
    * *(Windows)* `java -cp databaseapp-3.6.0.jar;dependency-jars/*;. studentscheduling.Run` to execute the runnable jar
    * *(Linux)* `java -cp databaseapp-3.6.0.jar:dependency-jars/* studentscheduling.Run` to execute the runnable jar

#### Manual Setup

1. Create a Java project with the `src/` folder.
2. Download the dependencies:
    * [commons-exec-1.3](https://commons.apache.org/proper/commons-exec/download_exec.cgi)
    * [Derby-10.12.1.1](http://db.apache.org/derby/derby_downloads.html)
    * [5.2.10.Final](http://hibernate.org/orm/downloads/)
3. [Add the `idpmapper-3.6.0.jar` and its dependencies as project libraries.](http://www.oxfordmathcenter.com/drupal7/node/44)
4. The IDP path should point to the IDP executable in `src/main/resources/idp.config`.
5. The path to the local database directory should be entered in:
	1. `src/main/java/META-INF/persistence.xml` inside the value of property `jdbc.url`.
	2. `src/main/java/studentscheduling/Run.java` as an argument to `db.createDatabase/1`.
6. [Create a jar](https://www.cs.utexas.edu/~scottm/cs307/handouts/Eclipse%20Help/jarInEclipse.htm) or run the example from an IDE.

## Motivation

This repository is created in the context of the Master Thesis *Annotation-based integration of IDP with Java*. The goal of the thesis is to create an input structure in IDP, using Java objects. Classes and their fields are marked by annotations to provide the required metadata for IDP. Various inference methods from IDP can then be used to solve a different aspect of the same problem. 

Consider for example a student that is selecting a study program. There are usually constraints involved:

* course lectures may not overlap
* total amount of course credits must be between the minimum and maximum allowed amount
* ...

One may ask themselves the following questions:

1. Given my current selection of courses, which courses am I still allowed to take? Which courses are no longer available? *This is answered with propagation inference.*
2. Do the courses I currently enrolled for, amount to a valid study program? *This is answered with satisfiability inference.*
3. Given my current selection of courses, generate five study programs to choose from, while minimizing the total amount of credits. *This is answered with optimization inference.*