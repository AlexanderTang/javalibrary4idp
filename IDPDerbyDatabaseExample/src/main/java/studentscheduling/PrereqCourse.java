package studentscheduling;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import idp.annotations.ArgumentType;
import idp.annotations.Predicate;

@Entity
@Table(name="Prerequisites")
@Predicate
public class PrereqCourse implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="Course")
	@ArgumentType(value=1, typeName="Course")
	private String course;
	@Id
	@Column(name="PrereqCourse")
	@ArgumentType(value=2, typeName="Course")
	private String prerequiredCourse;
	
	public PrereqCourse(){
	}
	
	public PrereqCourse(String course, String prereqCourse){
		this.course = course;
		this.prerequiredCourse = prereqCourse;
	}
	
	public String getDependentCourse(){
		return this.course;
	}
	
	public String getPrerequiredCourse(){
		return this.prerequiredCourse;
	}
}
