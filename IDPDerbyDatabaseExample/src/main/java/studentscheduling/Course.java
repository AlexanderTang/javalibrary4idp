package studentscheduling;

import idp.annotations.Type;
import idp.annotations.TypeValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Type
public class Course {
	@TypeValue
	private String name;
	private List<String> students;
	private int credits;
	private List<Course> coursePrequisites;
	private List<TimeSlot> sessions;
	
	public Course(String courseName, int credits){
		this.name = courseName;
		this.credits = credits;
		this.students = new ArrayList<>();
		this.coursePrequisites = new ArrayList<>();
		this.sessions = new ArrayList<>();
	}
	
	public String getName(){
		return this.name;
	}

	public int getCredits(){
		return this.credits;
	}
	
	public void addStudent(String studentName){
		this.students.add(studentName);
	}
	
	public List<String> getStudents(){
		return Collections.unmodifiableList(this.students);
	}
	
	public void addCoursePrerequisites(Course course){
		this.coursePrequisites.add(course);
	}
	
	public List<Course> getCoursePrerequisites(){
		return Collections.unmodifiableList(this.coursePrequisites);
	}
	
	public void addSession(TimeSlot timeslot){
		this.sessions.add(timeslot);
	}
	
	public List<TimeSlot> getSessions(){
		return Collections.unmodifiableList(this.sessions);
	}
	
	@Override
	public String toString(){
		return getName();
	}
}
