package studentscheduling;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import idp.annotations.ArgumentType;
import idp.annotations.Predicate;

@Entity
@Table(name="Enrolled")
@Predicate
public class Enrolled implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="Student")
	private String student;
	@Id
	@Column(name="Course")
	@ArgumentType(value=1, typeName="Course")
	private String course;

	public Enrolled(){
	}
	
	public Enrolled(String course){
		this.course = course;
	}
	
	public Enrolled(String student, String course){
		this(course);
		this.student = student;
	}
	
	public void setStudent(String studentName){
		this.student = studentName;
	}
	
	public String getStudent(){
		return this.student;
	}
	
	public String getCourse(){
		return this.course;
	}
}
