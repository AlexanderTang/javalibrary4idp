package studentscheduling;

public class TimeSlot {
	private String weekday;
	private int beginHour;
	private int endHour;
	
	public TimeSlot(String weekday, int beginHour, int endHour){
		this.weekday = weekday;
		this.beginHour = beginHour;
		this.endHour = endHour;
	}
	
	public String getWeekday(){
		return this.weekday;
	}
	
	public int getBeginHour(){
		return this.beginHour;
	}
	
	public int getEndHour(){
		return this.endHour;
	}
}
