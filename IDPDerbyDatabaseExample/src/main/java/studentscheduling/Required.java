package studentscheduling;

import idp.annotations.ArgumentType;
import idp.annotations.Predicate;

@Predicate
public class Required {
	@ArgumentType(value=1,typeName="Course")
	private String course;
	@ArgumentType(value=2,typeName="Course")
	private String reqCourse;
	
	public Required(String course, String reqCourse){
		this.course = course;
		this.reqCourse = reqCourse;
	}

}
