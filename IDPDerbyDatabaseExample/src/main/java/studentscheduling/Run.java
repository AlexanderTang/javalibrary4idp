package studentscheduling;

import idp.exceptions.IDPException;
import idp.exceptions.InvalidAnnotationException;
import idp.exceptions.InvalidStructureException;
import idp.exceptions.NilStructureException;
import idp.logic.IDPOptimizationResults;
import idp.logic.IDPSolver;
import idp.logic.IDPStructure;
import idp.logic.IDPTerm;
import idp.logic.IDPTheory;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;

public class Run {
	
	private EntityManager em;
	
	public Run(){
		this.em = EntityManagerUtil.getEntityManager();
	}

	public static void main(String [] args) {
		Database db = new Database();
		
		// INITIALIZE DATABASE TABLES
		//Connection con = db.createDatabase("C:\\Users\\Me\\MyDB");
		db.createDatabase("C:\\Users\\Me\\MyDB");
		
		// USE EXISTING DATABASE TABLES
		//Connection con = db.getDatabaseConnection("C:\\Users\\Me\\MyDB");
		
		Run run = new Run();
		run.run();
		//run.run2(con);

//		try {
//			con.close();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
	}

	@SuppressWarnings("unchecked")
	public void run(){
		try {
			URL idpConfig = getClass().getClassLoader().getResource("idp.config");
			IDPTheory theory = IDPTheory.loadFile(getClass().getClassLoader().getResource("theory.idptheory"));
			IDPSolver solver = new IDPSolver.SolverBuilder(theory, idpConfig)
											.replaceInput(false)
											.build();
			
			this.em.getTransaction().begin();
			// get all course credit data
			List<CourseCredit> credits = this.em.createQuery("FROM CourseCredit").getResultList();
			// get all course prerequisites data
			List<PrereqCourse> prerequisites = this.em.createQuery("FROM PrereqCourse").getResultList();
			// get all course sessions data
			List<Session> sessions = this.em.createQuery("FROM Session").getResultList();
			
			IDPStructure structure;
			// get list of all enrolled students
			List<String> enrolledStudents = this.em.createQuery("SELECT DISTINCT e.student FROM Enrolled e").getResultList();
			// for each enrolled student:
			for(String student : enrolledStudents){
				// get enrolled courses
				List<Enrolled> enrolled = this.em.createQuery("FROM Enrolled e WHERE e.student = :studentName")
													.setParameter("studentName", student)
													.getResultList();
				structure = getInputStructure(credits, prerequisites, sessions, enrolled);
				executeInferences(solver, structure, enrolled, student);
			}
			
			this.em.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("ERROR OCCURRED:");
			e.printStackTrace();
			this.em.getTransaction().rollback();
		}
		
		System.out.println("END DATABASE TRANSACTION");
	}
	
	public IDPStructure getInputStructure(List<CourseCredit> credits,
			List<PrereqCourse> prerequisites, List<Session> sessions, List<Enrolled> enrolled){
		IDPStructure structure = new IDPStructure();
		try {
			// The IDP types are not entered: I make use of the automatic type creation and
			// values entering in the library.
			structure.addCollection(credits, CourseCredit.class);
			structure.addCollection(prerequisites, PrereqCourse.class);
			structure.addCollection(sessions, Session.class);
			structure.addTrueCollection(enrolled, Enrolled.class);
			
			structure.addClass(Required.class); // helper predicate for the IDP theory
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			System.out.println("An error occurred when creating the input structure:");
			e.printStackTrace();
		}
		return structure;
	}
	
	private void executeInferences(IDPSolver solver, IDPStructure inputStruc,
			List<Enrolled> enrolled, String student){
		System.out.println("\n\n=== Student " + student + " has chosen to enroll in the following courses: ===");
		for(Enrolled enroll : enrolled){
			System.out.println("\t" + enroll.getCourse());
		}
		
		propagate(solver, inputStruc, enrolled, student);
		modelExpand(solver, inputStruc);
		minimize(solver, inputStruc);
	}
	
	@SuppressWarnings("unchecked")
	private void propagate(IDPSolver solver, IDPStructure inputStruc, List<Enrolled> enrolled, String studentName){
		System.out.println("\n== PERFORMING PROPAGATION ==");
		try {
			IDPStructure outputStruc = solver.optimalPropagate(inputStruc);
			
			Collection<Enrolled> mustEnrolled = outputStruc.getTrueCollection(Enrolled.class);
			if(mustEnrolled.size() != enrolled.size()){
				System.out.println("The student is required to pick up the following additional courses:");
				for(Enrolled enroll : mustEnrolled){
					if(!enrolled.contains(enroll))
						System.out.println("\t" + enroll.getCourse());
				}
			} else System.out.println("The student is not required to pick up any particular additional courses.");
			
			boolean persistResultsToDatabase = true;
			if(persistResultsToDatabase){
				// PERSIST TO DATABASE
				EntityManager newEM = EntityManagerUtil.getEntityManager();
				newEM.getTransaction().begin();
				for(Enrolled enroll : mustEnrolled){
					if(!enrolled.contains(enroll)){
						enroll.setStudent(studentName);
						newEM.persist(enroll);
					}
				}
				newEM.getTransaction().commit();
				
				// VERIFY THAT TABLE HAS BEEN UPDATED
				newEM = EntityManagerUtil.getEntityManager();
				newEM.getTransaction().begin();
				List<Enrolled> enrolled2 = newEM.createQuery("FROM Enrolled e WHERE e.student = :studentName")
						.setParameter("studentName", studentName)
						.getResultList();
				System.out.println("UPDATED COURSES:");
				for(Enrolled enroll : enrolled2){
					System.out.println("\t" + enroll.getCourse());
				}
				newEM.getTransaction().commit();
			}
			
			Collection<Enrolled> denyEnrolled = outputStruc.getFalseCollection(Enrolled.class);
			if(!denyEnrolled.isEmpty()){
				System.out.println("The student can not select the following courses:");
				for(Enrolled enroll : denyEnrolled)
					System.out.println("\t" + enroll.getCourse());
			} else System.out.println("The student is not restricted from picking any particular additional courses.");
		} catch (NilStructureException e) {
			System.out.println("The chosen courses cause a conflict in the schedule. No valid "
					+ "schedule exists.");
		} catch(IDPException e){ // catch all other exceptions
			e.printStackTrace();
		}
	}
	
	private void modelExpand(IDPSolver solver, IDPStructure inputStruc){
		System.out.println("\n== PERFORMING MODEL EXPANSION ==");
		try {
			System.out.println("Only the first 5 models will be returned.");
			Collection<IDPStructure> outputStructs = solver.modelExpand(inputStruc, 5, false);
			
			if(outputStructs.isEmpty())
				System.out.println("No valid schedules have been found.");
			else{
				int counter = 1;
				for(IDPStructure struc : outputStructs){
					System.out.println("The selected courses for Model " + counter + " are:");
					for(Enrolled enroll : struc.getCollection(Enrolled.class))
						System.out.println("\t" + enroll.getCourse());
					counter++;
				}
			}
		} catch(IDPException e){
			e.printStackTrace();
		}
	}
	
	private void minimize(IDPSolver solver, IDPStructure inputStruc){
		System.out.println("\n== PERFORMING MINIMIZATION ==");
		try {
			System.out.println("Minimize the amount of course credits taken. Only the first 5 models will be returned.");
			// method1:
			//IDPTerm term = IDPTerm.loadString("sum{cr[Credits],c[Course] : Enrolled(c) & CourseCredit(c) = cr : cr}");
			// method2:
			IDPTerm term = IDPTerm.loadFile(getClass().getClassLoader().getResource("sumCredits.idpterm"));
			IDPOptimizationResults results = solver.minimize(inputStruc, term);
			System.out.println("The minimum amount credits for any valid schedule is " + results.getOptimalValue() + ";");
			
			if(results.optimumFound()){
				int counter = 1;
				for(IDPStructure struc : results.getStructures()){
					System.out.println("The selected courses for Model " + counter + " are:");
					for(Enrolled enroll : struc.getCollection(Enrolled.class))
						System.out.println("\t" + enroll.getCourse());
					counter++;
				}
			} else System.out.println("No valid schedules have been found.");
		} catch(IDPException | IOException e){
			e.printStackTrace();
		}
	}
	
	// using the old method of pulling data from tables
//	public void run2(Connection con){
//		try{
//			Statement sta = con.createStatement(); 
//
//			// getting the Enrolled data back
//			ResultSet res = sta.executeQuery(
//					"SELECT * FROM Enrolled");
//			System.out.println("List of enrolled students: "); 
//			while (res.next()) {
//				System.out.println(
//						"  "+res.getString("Student")
//						+ ", "+res.getString("Course"));
//			}
//			System.out.println();
//			
//			// getting the CourseCredits data back
//			res = sta.executeQuery(
//					"SELECT * FROM CourseCredits");
//			System.out.println("List of course credits: "); 
//			while (res.next()) {
//				System.out.println(
//						"  "+res.getString("Course")
//						+ ", "+res.getInt("Credits"));
//			}
//			System.out.println();
//			
//			// getting the Prerequisites data back
//			res = sta.executeQuery(
//					"SELECT * FROM Prerequisites");
//			System.out.println("List of course prerequisites: "); 
//			while (res.next()) {
//				System.out.println(
//						"  "+res.getString("Course")
//						+ ", "+res.getString("PrereqCourse"));
//			}
//			System.out.println();
//			
//			// getting the Sessions data back
//			res = sta.executeQuery(
//					"SELECT * FROM Sessions");
//			System.out.println("List of course sessions: "); 
//			while (res.next()) {
//				System.out.println(
//						"  "+res.getString("Course")
//						+ ", "+res.getString("Weekday")
//						+ ", "+res.getInt("BeginTime")
//						+ ", "+res.getInt("EndTime"));
//			}
//			System.out.println();
//			
//			res.close();
//			sta.close();    
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
}
