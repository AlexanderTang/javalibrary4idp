package studentscheduling;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import idp.annotations.ArgumentType;
import idp.annotations.Predicate;

@Entity
@Table(name="Sessions")
@Predicate
public class Session implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="Course")
	@ArgumentType(value=1, typeName="Course")
	private String course;
	@Id
	@Column(name="Weekday")
	@ArgumentType(value=2, typeName="Weekday")
	private String weekday;
	@Id
	@Column(name="BeginTime")
	@ArgumentType(value=3,typeName="HourOfDay")
	private int beginHour;
	@Column(name="EndTime")
	@ArgumentType(value=4,typeName="HourOfDay")
	private int endHour;
	
	public Session(){
	}
	
	public Session(String course, String weekday, int beginHour, int endHour){
		this.course = course;
		this.weekday = weekday;
		this.beginHour = beginHour;
		this.endHour = endHour;
	}
	
	public String getCourse() {
		return this.course;
	}

	public String getWeekday() {
		return this.weekday;
	}

	public int getBeginHour() {
		return this.beginHour;
	}

	public int getEndHour() {
		return this.endHour;
	}
}
