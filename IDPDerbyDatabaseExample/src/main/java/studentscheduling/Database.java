package studentscheduling;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Database {
	private Connection con = null;

	public void createDatabase(String dbPath){
		this.con = getDatabaseConnection(dbPath);
		createTables();
		System.out.println();
		try {
			this.con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
//	public Connection createDatabase(String dbPath){
//		this.con = getDatabaseConnection(dbPath);
//		createTables();
//		System.out.println();
//		return this.con;
//	}
	
	public Connection getDatabaseConnection(String dbPath){
		System.out.println("== CONNECTING DATABASE ==");
		try {
			String path = "jdbc:derby:" + dbPath + ";create=true";
			this.con = DriverManager.getConnection(path);
			System.out.println("DATABASE CONNECTED");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println();
		return this.con;
	}
	
	private void createTables(){ 
		// TABLE ENROLLED
		String tableName = "Enrolled";
		createTable(tableName, "(Student VARCHAR(20), Course VARCHAR(30))");
		insertRows(tableName, "(Student, Course)", getEnrolledValues());
		
		tableName = "CourseCredits";
		createTable(tableName, "(Course VARCHAR(30), Credits INT)");
		insertRows(tableName, "(Course, Credits)", getCourseCreditsValues());
		
		tableName = "Prerequisites";
		createTable(tableName, "(Course VARCHAR(30), PrereqCourse VARCHAR(30))");
		insertRows(tableName, "(Course, PrereqCourse)", getPrereqsValues());
		
		tableName = "Sessions";
		createTable(tableName, "(Course VARCHAR(30), Weekday VARCHAR(15), BeginTime INT, EndTime INT)");
		insertRows(tableName, "(Course, Weekday, BeginTime, EndTime)", getSessionsValues());
	}
	
	private void createTable(String tableName, String tableParameters){
		try {
			System.out.println("== CREATING TABLE " + tableName.toUpperCase() + " ==");
			DatabaseMetaData dbm = this.con.getMetaData();
            ResultSet rs = dbm.getTables(null, null, tableName.toUpperCase(), null);
            if (rs.next()) {  // a table with the same name has been found -> drop table
            	Statement sta = this.con.createStatement(); 
            	sta.executeUpdate("DROP TABLE " + tableName);
                sta.close();
            }
			
			Statement sta = this.con.createStatement(); 
			sta.executeUpdate("CREATE TABLE " + tableName + " " + tableParameters);
			sta.close(); 
			System.out.println("TABLE CREATED");
		} catch (SQLException e) {
			System.out.println("Table already exists.");
		}
	}
	

	private List<String> getEnrolledValues(){
		List<String> tableValues = new ArrayList<>();
		tableValues.add("('Abdul', 'CalculusOne')");
		tableValues.add("('Abdul', 'CalculusTwo')");
		tableValues.add("('Abdul', 'LinearAlgebra')");
		tableValues.add("('Abdul', 'DataStructures')");
		tableValues.add("('Abdul', 'MachineLearningProject')");
		tableValues.add("('Cynthia', 'ModelingAndSimulation')");
		tableValues.add("('Kael', 'ComputerNetworks')");
		tableValues.add("('Kael', 'DataMining')");
		tableValues.add("('Kael', 'ModelingAndSimulation')");
		tableValues.add("('Turing', 'SoftwareDesign')");
		tableValues.add("('Turing', 'DataStructures')");
		tableValues.add("('Turing', 'DeclarativeLanguages')");
		return tableValues;
	}
	
	private List<String> getCourseCreditsValues(){
		List<String> tableValues = new ArrayList<>();
		tableValues.add("('CalculusOne', 9)");
		tableValues.add("('CalculusTwo', 9)");
		tableValues.add("('DataStructures', 6)");
		tableValues.add("('ProgrammingIntroduction', 6)");
		tableValues.add("('SoftwareDesign', 6)");
		tableValues.add("('SoftwareArchitecture', 6)");
		tableValues.add("('ObjectOrientedProgramming', 9)");
		tableValues.add("('DeclarativeLanguages', 6)");
		tableValues.add("('OperatingSystems', 12)");
		tableValues.add("('MachineLearning', 4)");
		tableValues.add("('MachineLearningProject', 4)");
		tableValues.add("('DataMining', 4)");
		tableValues.add("('DistributedSystems', 6)");
		tableValues.add("('ComputerNetworks', 6)");
		tableValues.add("('LinearAlgebra', 6)");
		tableValues.add("('ModelingAndSimulation', 12)");
		return tableValues;
	}
	
	private List<String> getPrereqsValues(){
		List<String> tableValues = new ArrayList<>();
		tableValues.add("('CalculusTwo', 'CalculusOne')");
		tableValues.add("('LinearAlgebra', 'CalculusOne')");
		tableValues.add("('ModelingAndSimulation', 'CalculusTwo')");
		tableValues.add("('ModelingAndSimulation', 'LinearAlgebra')");
		tableValues.add("('ModelingAndSimulation', 'ProgrammingIntroduction')");
		tableValues.add("('SoftwareDesign', 'ProgrammingIntroduction')");
		tableValues.add("('SoftwareArchitecture', 'SoftwareDesign')");
		tableValues.add("('ObjectOrientedProgramming', 'ProgrammingIntroduction')");
		tableValues.add("('DeclarativeLanguages', 'ProgrammingIntroduction')");
		tableValues.add("('DistributedSystems', 'OperatingSystems')");
		tableValues.add("('MachineLearningProject', 'MachineLearning')");
		return tableValues;
	}
	
	private List<String> getSessionsValues(){
		List<String> tableValues = new ArrayList<>();
		tableValues.add("('CalculusOne', 'Monday', 9, 11)");
		tableValues.add("('SoftwareDesign', 'Monday', 10, 11)");
		tableValues.add("('DistributedSystems', 'Monday', 16, 18)");
		tableValues.add("('LinearAlgebra', 'Tuesday', 10, 12)");
		tableValues.add("('SoftwareArchitecture', 'Tuesday', 11, 13)");
		tableValues.add("('ObjectOrientedProgramming', 'Tuesday', 11, 13)");
		tableValues.add("('OperatingSystems', 'Tuesday', 14, 16)");
		tableValues.add("('CalculusTwo', 'Wednesday', 10, 12)");
		tableValues.add("('OperatingSystems', 'Wednesday', 11, 13)");
		tableValues.add("('DeclarativeLanguages', 'Wednesday', 15, 17)");
		tableValues.add("('SoftwareArchitecture', 'Thursday', 9, 11)");
		tableValues.add("('MachineLearning', 'Thursday', 11, 13)");
		tableValues.add("('ComputerNetworks', 'Thursday', 13, 15)");
		tableValues.add("('ModelingAndSimulation', 'Thursday', 16, 18)");
		tableValues.add("('ModelingAndSimulation', 'Friday', 9, 11)");
		tableValues.add("('DataStructures', 'Friday', 10, 12)");
		tableValues.add("('ProgrammingIntroduction', 'Friday', 12, 14)");
		tableValues.add("('MachineLearningProject', 'Friday', 13, 15)");
		tableValues.add("('DataMining', 'Friday', 14, 16)");
		return tableValues;
	}

	private void insertRows(String tableName, String tableParams, List<String> tableValues){
		try{
			Statement sta = this.con.createStatement(); 

			int count = 0;
			System.out.println("== START INSERTING ROWS ==");
			for(String values : tableValues){
				count += sta.executeUpdate("INSERT INTO " + tableName + " "
						+ tableParams + " VALUES " + values);
			}
			System.out.println("NUMBER OF ROWS INSERTED: " + count);

			sta.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
