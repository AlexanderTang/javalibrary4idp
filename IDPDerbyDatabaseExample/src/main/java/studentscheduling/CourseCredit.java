package studentscheduling;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import idp.annotations.ArgumentType;
import idp.annotations.Function;
import idp.annotations.FunctionValue;

@Entity
@Table(name="CourseCredits")
@Function
public class CourseCredit implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="Course")
	@ArgumentType(value=1, typeName="Course")
	private String course;
	@Column(name="Credits")
	@FunctionValue("Credits")
	private int credits;
	
	public CourseCredit(){
	}
	
	public CourseCredit(String course, int credits){
		this.course = course;
		this.credits = credits;
	}
	
	public String getCourse(){
		return this.course;
	}
	
	public int getCredits(){
		return this.credits;
	}
}
