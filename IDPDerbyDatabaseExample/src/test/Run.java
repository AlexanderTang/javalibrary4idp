package test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Run {

	public static void main(String [] args) {
		Database db = new Database();
		Connection con = db.createDatabase("C:\\Users\\Me\\MyDB");
		getResults(con);

		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void getResults(Connection con){
		try{
			Statement sta = con.createStatement(); 

			// getting the data back
			ResultSet res = sta.executeQuery(
					"SELECT * FROM Address");
			System.out.println("List of Addresses: "); 
			while (res.next()) {
				System.out.println(
						"  "+res.getInt("ID")
						+ ", "+res.getString("StreetName")
						+ ", "+res.getString("City"));
			}
			res.close();

			sta.close();    
		} catch (Exception e) {
			System.err.println("Exception: "+e.getMessage());
		}
	}
}
