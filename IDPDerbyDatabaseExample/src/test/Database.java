package test;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {
	private Connection con = null;

	public Connection createDatabase(String dbPath){
		System.out.println("== CONNECTING DATABASE ==");
		try {
			String path = "jdbc:derby:" + dbPath + ";create=true";
			this.con = DriverManager.getConnection(path);
			System.out.println("DATABASE CONNECTED");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		createTable();
		insertRows();

		System.out.println();
		return this.con;
	}

	private void createTable(){
		try {
			System.out.println("== CREATING A TABLE ==");
			DatabaseMetaData dbm = this.con.getMetaData();
            ResultSet rs = dbm.getTables(null, null, "ADDRESS", null);
            if (rs.next()) {  // a table with the same name has been found -> drop table
            	Statement sta = this.con.createStatement(); 
            	sta.executeUpdate("DROP TABLE Address");
                sta.close();
            }
			
			Statement sta = this.con.createStatement(); 
			sta.executeUpdate(
					"CREATE TABLE Address (ID INT, StreetName VARCHAR(20),"
							+ " City VARCHAR(20))");
			System.out.println("TABLE CREATED");
			sta.close();      
		} catch (SQLException e) {
			System.out.println("Table already exists.");
		}
	}

	private void insertRows(){
		try{
			Statement sta = this.con.createStatement(); 

			System.out.println("== START INSERTING ROWS ==");
			// insert 3 rows
			int count = 0;
			int c = sta.executeUpdate("INSERT INTO Address"
					+ " (ID, StreetName, City)"
					+ " VALUES (1, '5 Baker Road', 'Bellevue')");
			count = count + c;

			c = sta.executeUpdate("INSERT INTO Address"
					+ " (ID, StreetName, City)"
					+ " VALUES (2, '25 Bay St.', 'Hull')");
			count = count + c;

			c = sta.executeUpdate("INSERT INTO Address"
					+ " (ID, StreetName, City)"
					+ " VALUES (3, '251 Main St.', 'W. York')");
			count = count + c;
			System.out.println("NUMBER OF ROWS INSERTED: " + count);

			sta.close();      
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
