package examples.sudoku;

import idp.annotations.ArgumentType;
import idp.annotations.Predicate;

/**
 * A cell of Sudoku. This example is based on the Sudoku example of EmbASP.
 * 
 * @author Alexander Tang
 *
 */
@Predicate
public class Cell {
	
	@ArgumentType(value=1, typeName="sudokuValue")
	private int row;
	
	@ArgumentType(value=2, typeName = "sudokuValue")
	private int column;
	
	@ArgumentType(value=3, typeName = "sudokuValue")
	private int value;
	
	public Cell(int row, int column, int value){
		this.row = row;
		this.column = column;
		this.value = value;
	}
	
	public int getRow(){
		return this.row;
	}
	
	public int getColumn(){
		return this.column;
	}
	
	public int getValue(){
		return this.value;
	}
}
