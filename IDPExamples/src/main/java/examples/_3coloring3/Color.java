package examples._3coloring3;

import idp.annotations.Type;
import idp.annotations.TypeValue;

@Type
public class Color {
	
	@TypeValue
	private String color;
	
	public Color(String color){
		this.color = color;
	}
	
	public String getColor(){
		return this.color;
	}

}
