package examples._3coloring3;

import idp.exceptions.IDPException;
import idp.exceptions.InvalidAnnotationException;
import idp.exceptions.InvalidStructureException;
import idp.exceptions.UnavailableCollectionException;
import idp.logic.*;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Run the main method in this class to execute the 3 coloring problem. The values can be
 * adjusted in {@link #externalSystem(Collection,Collection,Collection) externalSystem}.
 * 
 * The representation in this package uses the following setup:
 *  - Edges are instances of an @Predicate annotated class.
 *  - Nodes are instances of an @Function annotated class.
 *  - Node id's are instances of an @Type annotated class. The hash code of the instance
 *  	is used to represent the type's value.
 *  - Colors are instances of an @Type annotated class. An @TypeValue annotated field
 *  	is used to represent the type's value.
 * 
 * @author Alexander Tang
 *
 */
public class Run {
	
	public static void main(String[] args){
		Run r = new Run();
		r.run();
	}
	
	public void run(){
		URL idpConfig = getClass().getClassLoader().getResource("idp.config");
		IDPTheory theory = IDPTheory.loadFile(getClass().getClassLoader().getResource("_3coloring/InputProgram.idptheory"));
		IDPStructure inputStruc = get_3coloring_input_structure();

		try{
			// IDP solver with input program, default 5 models and replaces input collections
			IDPSolver solver = new IDPSolver.SolverBuilder(theory, idpConfig)
											.nbModels(5)
											.replaceInput(false)
											.build();
			
			/*******************************
			 * Solve for various inferences
			 *******************************/
			// check satisfiability
			System.out.println("SATISFACTION");
			boolean modelFound = solver.sat(inputStruc);
			System.out.println("Model found: " + modelFound + "\n");
			
			// calculate definitions and returns the resulting (reduced) structure. 
			// Returns null if struc is nil instead.
			System.out.println("CALCULATE DEFINITIONS");
			IDPStructure struc = solver.calculateDefinitions(inputStruc);
			printStructure(struc);
			
			// grounding and unit propagation
			System.out.println("GROUND PROPAGATE");
			IDPStructure struc2 = solver.groundPropagate(inputStruc);
			printStructure(struc2);
			
			System.out.println("OPTIMAL PROPAGATE");
			IDPStructure struc3 = solver.optimalPropagate(inputStruc);
			printStructure(struc3);
			
			// symbolic propagation
			System.out.println("PROPAGATE");
			IDPStructure struc4 = solver.propagate(inputStruc);
			printStructure(struc4);
			
			// model expansion
			System.out.println("MODEL EXPANSION");
			Collection<IDPStructure> structs = solver.modelExpand(inputStruc, 0, false);  // entering 0 returns all models
			for(IDPStructure structure : structs)
				printStructure(structure);
			
			System.out.println("MODEL EXPANSION 2");
			Collection<IDPStructure> structs2 = solver.modelExpand(inputStruc);
			for(IDPStructure structure : structs2)
				printStructure(structure);
			
			System.out.println("SINGLE MODEL EXPAND");
			IDPStructure structs3 = solver.singleModelExpand(inputStruc); // model expand with parameters 1 and true
			printStructure(structs3);
			System.out.println(solver.getWarningLog());
			
			// optimization
			System.out.println("MINIMIZE");
			IDPTerm term = IDPTerm.loadString("#{a[NodeId] : Node(a) = \"blue\"}");
			IDPOptimizationResults minResults = solver.minimize(inputStruc, term);
			Collection<IDPStructure> minStructs = minResults.getStructures();
			for(IDPStructure structure : minStructs)
				printStructure(structure);
			boolean minFound = minResults.optimumFound();
			System.out.println(minFound);
			int min = minResults.getOptimalValue();
			System.out.println(min);
			
			System.out.println("MAXIMIZE");
			// alternative way to specify term:
			URL url = getClass().getClassLoader().getResource("_3coloring/minimumBlue.idpterm");
			IDPTerm term2 = IDPTerm.loadFile(url);
		    IDPOptimizationResults maxResults = solver.maximize(inputStruc, term2);
		    Collection<IDPStructure> maxStructs = maxResults.getStructures();
		    for(IDPStructure structure : maxStructs)
		    	printStructure(structure);
			boolean maxFound = maxResults.optimumFound();
			System.out.println(maxFound);
			int max = maxResults.getOptimalValue();
			System.out.println(max);
			
		} catch(IDPException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Call this method to create and populate the input program
	public IDPStructure get_3coloring_input_structure(){
		Collection<NodeId> nodeIds = new ArrayList<NodeId>();
		Collection<Color> colors = new ArrayList<Color>();
		Collection<Edge> edges = new ArrayList<Edge>();
		externalSystem(nodeIds, colors, edges); 
		// no Node objects have been instantiated 
		// -> This corresponds to a Node predicate in IDP with an undefined structure.
		
		IDPStructure struc = new IDPStructure();
		try {
			struc.addCollection(nodeIds, NodeId.class);
			struc.addCollection(colors, Color.class);
			struc.addCollection(edges, Edge.class);
			struc.addCollection(null, Node.class);
		} catch (InvalidAnnotationException | InvalidStructureException e) {
			e.printStackTrace();
		}
		return struc;
	}

	/*
	 * This method simulates the functionality where predicates, types etc are pulled from other
	 * Java objects or external systems. For the purposes of the example, some random test values are
	 * created in this method instead.
	 */
	public void externalSystem(Collection<NodeId> nodeIds, Collection<Color> colors, Collection<Edge> edges){
		// instantiate nodes 1..8
		List<NodeId> tempNodeIds = new ArrayList<NodeId>(); // used for instantiating edges
		for(int i = 0; i<8; i++){
			NodeId nodeId = new NodeId();
			nodeIds.add(nodeId);
			tempNodeIds.add(nodeId);
		}
		
		// instantiate colors 'blue', 'red' and 'white'
		colors.addAll(Arrays.asList(new Color("blue"), new Color("red"), new Color("white")));
		
		// instantiate possible edges (in a 2-valued structure)
		edges.add(new Edge(tempNodeIds.get(0), tempNodeIds.get(1))); // link nodeid 1 with nodeid 2
		edges.add(new Edge(tempNodeIds.get(1), tempNodeIds.get(2))); // link nodeid 2 with nodeid 3
		edges.add(new Edge(tempNodeIds.get(1), tempNodeIds.get(3))); // link nodeid 2 with nodeid 4
		edges.add(new Edge(tempNodeIds.get(2), tempNodeIds.get(4))); // link nodeid 3 with nodeid 5
		edges.add(new Edge(tempNodeIds.get(2), tempNodeIds.get(5))); // link nodeid 3 with nodeid 6
		edges.add(new Edge(tempNodeIds.get(1), tempNodeIds.get(5))); // link nodeid 2 with nodeid 6
		edges.add(new Edge(tempNodeIds.get(0), tempNodeIds.get(6))); // link nodeid 1 with nodeid 7
		edges.add(new Edge(tempNodeIds.get(2), tempNodeIds.get(6))); // link nodeid 3 with nodeid 7
		edges.add(new Edge(tempNodeIds.get(6), tempNodeIds.get(7))); // link nodeid 7 with nodeid 8
		//edges.add(new Edge(tempNodeIds.get(3), new NodeId()));
	}
	
	public void printStructure(IDPStructure struc){
		System.out.println("==================================");
		System.out.println("Resulting structure: " + struc);
		System.out.println("----------------------------------");
		try {
			System.out.println("colors: " + struc.getCollection(Color.class));
		} catch (UnavailableCollectionException e) {
			System.out.println("<collection not available>");
		}
		System.out.println("----------------------------------");
		try {
			System.out.println("node ids: " + struc.getCollection(NodeId.class));
		} catch (UnavailableCollectionException e) {
			System.out.println("<collection not available>");
		}
		System.out.println("----------------------------------");
		Collection<Node> nodes;
		try {
			nodes = struc.getTrueCollection(Node.class);
			System.out.println("nodes (ct): " + nodes);
			printNodes(nodes);
		} catch (UnavailableCollectionException e) {
			System.out.println("<collection not available>");
		}
		System.out.println("----------------------------------");
		try {
			nodes = struc.getFalseCollection(Node.class);
			System.out.println("nodes (cf): " + nodes);
			printNodes(nodes);
		} catch (UnavailableCollectionException e) {
			System.out.println("<collection not available>");
		}
		System.out.println("----------------------------------");
		try {
			nodes = struc.getUnknownCollection(Node.class);
			System.out.println("nodes (u): " + nodes);
			printNodes(nodes);
		} catch (UnavailableCollectionException e) {
			System.out.println("<collection not available>");
		}
		System.out.println("----------------------------------");
		try {
			System.out.println("edges: " + struc.getCollection(Edge.class));
			System.out.println("amount of edges: " + struc.getCollection(Edge.class).size());
		} catch (UnavailableCollectionException e) {
			System.out.println("<collection not available>");
		}
		
		System.out.println("==================================");
	}
	
	public void printNodes(Collection<Node> nodes){
		if(nodes == null)	return;
		for(Node n : nodes){
			System.out.println(n.getId() + "  -  " + n.getColor());
		}
	}
}
