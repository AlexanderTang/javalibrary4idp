package examples._3coloring3;

import idp.annotations.ArgumentType;
import idp.annotations.Function;
import idp.annotations.FunctionValue;

@Function
public class Node {
	
	@ArgumentType(1)
	private NodeId id;
	
	@FunctionValue
	private Color color;
	
	public Node(NodeId id, Color color) {
		this.id = id;
		this.color = color;
	}
	
	public NodeId getId(){
		return this.id;
	}

	public String getColor(){
		return this.color.getColor();
	}
}



