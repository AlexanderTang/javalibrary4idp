package examples._3coloring2;

import idp.annotations.ArgumentType;
import idp.annotations.Predicate;

@Predicate
public class Edge {
	
	@ArgumentType(value=1,typeName="NodeId")
	private int nodeId1;
	
	@ArgumentType(value=2,typeName="NodeId")
	private int nodeId2;
	
	public Edge(int id1, int id2){
		this.nodeId1 = id1;
		this.nodeId2 = id2;
	}
}
