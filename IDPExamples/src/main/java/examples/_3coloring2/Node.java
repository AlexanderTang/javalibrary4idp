package examples._3coloring2;

import idp.annotations.ArgumentType;
import idp.annotations.Function;
import idp.annotations.FunctionValue;

@Function
public class Node {
	
	@ArgumentType(value=1,typeName="NodeId")
	private int id;
	
	@FunctionValue("color")
	private String color;
	
	public Node(int id, String color) {
		this.id = id;
		this.color = color;
	}
	
	public int getId(){
		return this.id;
	}

	public String getColor(){
		return this.color;
	}
}



