package examples.reachability;

import idp.annotations.Type;
import idp.annotations.TypeValue;

@Type
public class Node {
	
	@TypeValue
	private int id;
	
	public Node(Integer id) {
		this.id = id;
	}
	
	public int getId(){
		return this.id;
	}

}