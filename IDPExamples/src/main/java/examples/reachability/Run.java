package examples.reachability;

import idp.exceptions.IDPException;
import idp.exceptions.UnavailableCollectionException;
import idp.logic.IDPSolver;
import idp.logic.IDPStructure;
import idp.logic.IDPTheory;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Run the main method in this class to execute the reachability problem. The values can be
 * adjusted in {@link #getReachabilityInputStructure()}.
 * 
 * The representation in this package uses the following setup:
 *  - Edges are instances of an @Predicate annotated class.
 *  - 'Reachable' objects are instances of an @Predicate annotated class.
 *  - Nodes are instances of an @Type annotated class. An @TypeValue annotated field
 *  	is used to represent the type's value.
 * 
 * @author Alexander Tang
 *
 */
public class Run {

	public static void main(String[] args){
		Run r = new Run();
		r.run();
	}
	
	public void run(){
		solveReachability(IDPTheory.loadFile(getClass().getClassLoader().
				getResource("reachability/undirectedReachability.idptheory")), "undirected reachability");
		solveReachability(IDPTheory.loadFile(getClass().getClassLoader().
				getResource("reachability/directedReachability.idptheory")), "directed reachability");
	}
	
	private void solveReachability(IDPTheory theory, String problemTitle){
		URL idpConfig = getClass().getClassLoader().getResource("idp.config");
		IDPStructure inputStruc = getReachabilityInputStructure();

		try{
			// IDP solver with input program, default 5 models and replaces input collections
			IDPSolver solver = new IDPSolver.SolverBuilder(theory, idpConfig)
											.nbModels(5)
											.replaceInput(false)
											.build();
			
			System.out.println(problemTitle.toUpperCase() + "\n");
			
			boolean modelFound = solver.sat(inputStruc);
			System.out.println("Model found: " + modelFound + "\n");
			
			IDPStructure str = solver.calculateDefinitions(inputStruc);
			System.out.println("Calculate definitions - Printing found model:\n");
			printReachability(str);
			
			Collection<IDPStructure> structs2 = solver.modelExpand(inputStruc);
			System.out.println("Model expansion - Printing found models:\n");
			for(IDPStructure structure : structs2)
				printReachability(structure);
		} catch(IDPException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private IDPStructure getReachabilityInputStructure(){
		IDPStructure struc = new IDPStructure();
		
		List<Node> nodes = new ArrayList<>();
		for(int i = 0; i<8; i++)
			nodes.add(new Node(i));
		
		Collection<Edge> edges = new HashSet<>();
		edges.addAll(Arrays.asList(
				new Edge(nodes.get(0), nodes.get(1)), // subgraph 1
				new Edge(nodes.get(1), nodes.get(2)),
				new Edge(nodes.get(1), nodes.get(3)),
				new Edge(nodes.get(2), nodes.get(4)),
				new Edge(nodes.get(3), nodes.get(4)),
				new Edge(nodes.get(5), nodes.get(7)), // subgraph 2
				new Edge(nodes.get(7), nodes.get(6)),
				new Edge(nodes.get(6), nodes.get(5))));
		
		try{
			struc.addCollection(nodes, Node.class);
			struc.addCollection(edges, Edge.class);
			struc.addCollection(null, Reachable.class);
		} catch (IDPException e) {
			e.printStackTrace();
		}
		return struc;
	}
	
	private void printReachability(IDPStructure structure){
		try{
			Collection<Reachable> reachables = structure.getCollection(Reachable.class);
			String[] categories = new String[8];
			for(int i = 0; i<8; i++)
				categories[i] = "";
			for(Reachable r : reachables){
				if(!"".equals(categories[r.getNode1().getId()]))
						categories[r.getNode1().getId()] += ", ";
				categories[r.getNode1().getId()] += r.toString();
			}
			for(String c : categories){
				if(!"".equals(c))
					System.out.println(c);
			}
			System.out.println();
		} catch(UnavailableCollectionException e){
			System.out.println("Could not get reachables collection: " + e.getMessage());
		}
	}
}
