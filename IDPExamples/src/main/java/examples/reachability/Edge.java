package examples.reachability;

import idp.annotations.ArgumentType;
import idp.annotations.Predicate;

@Predicate
public class Edge {
	
	@ArgumentType(1)
	private Node node1;
	
	@ArgumentType(2)
	private Node node2;
	
	public Edge(Node id1, Node id2){
		this.node1 = id1;
		this.node2 = id2;
	}

}
