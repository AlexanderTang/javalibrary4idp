package examples.reachability;

import idp.annotations.ArgumentType;
import idp.annotations.Predicate;

@Predicate
public class Reachable {
	
	@ArgumentType(1)
	private Node node1;
	
	@ArgumentType(2)
	private Node node2;
	
	public Reachable(Node id1, Node id2){
		this.node1 = id1;
		this.node2 = id2;
	}
	
	public Node getNode1(){
		return this.node1;
	}
	
	public Node getNode2(){
		return this.node2;
	}

	@Override
	public String toString(){
		return "Reachable(" + getNode1().getId() + "," + getNode2().getId() + ")";
	}
}
