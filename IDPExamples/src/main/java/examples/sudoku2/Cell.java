package examples.sudoku2;

import idp.annotations.ArgumentType;
import idp.annotations.Function;
import idp.annotations.FunctionValue;

/**
 * A cell of Sudoku. This example is based on the Sudoku example of EmbASP.
 * 
 * @author Alexander Tang
 *
 */
@Function
public class Cell {
	
	@ArgumentType(value=1, typeName="sudokuValue")
	private int row;
	
	@ArgumentType(value=2, typeName = "sudokuValue")
	private int column;
	
	@FunctionValue("sudokuValue")
	private int value;
	
	public Cell(Integer row, Integer column, Integer value){
		this.row = row;
		this.column = column;
		this.value = value;
	}
	
	public int getRow(){
		return this.row;
	}
	
	public int getColumn(){
		return this.column;
	}
	
	public int getValue(){
		return this.value;
	}
}
