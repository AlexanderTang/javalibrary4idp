package examples.sudoku2;

import idp.annotations.ArgumentType;
import idp.annotations.Predicate;

@Predicate
public class WithinBlock {
	
	@ArgumentType(value=1, typeName="sudokuValue")
	private int row;
	
	@ArgumentType(value=2, typeName = "sudokuValue")
	private int column;
	
	@ArgumentType(value=3, typeName = "sudokuValue")
	private int block;
	
	public WithinBlock(Integer row, Integer column, Integer block){
		this.row = row;
		this.column = column;
		this.block = block;
	}

}
