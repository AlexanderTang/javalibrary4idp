package examples._3coloring;

import idp.annotations.ArgumentType;
import idp.annotations.Predicate;

@Predicate
public class Edge {
	
	@ArgumentType(1)
	private NodeId nodeId1;
	
	@ArgumentType(2)
	private NodeId nodeId2;
	
	public Edge(NodeId id1, NodeId id2){
		this.nodeId1 = id1;
		this.nodeId2 = id2;
	}
}
