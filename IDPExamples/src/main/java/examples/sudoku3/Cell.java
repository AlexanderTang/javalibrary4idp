package examples.sudoku3;

import idp.annotations.ArgumentType;
import idp.annotations.Function;
import idp.annotations.FunctionValue;

/**
 * A cell of Sudoku. This example is based on the Sudoku example of EmbASP.
 * 
 * @author Alexander Tang
 *
 */
@Function
public class Cell {
	
	@ArgumentType(1)
	private DomainValue row;
	
	@ArgumentType(2)
	private DomainValue column;
	
	@FunctionValue
	private DomainValue value;
	
	public Cell(DomainValue row, DomainValue column, DomainValue value){
		this.row = row;
		this.column = column;
		this.value = value;
	}
	
	public int getRow(){
		return this.row.getValue();
	}
	
	public int getColumn(){
		return this.column.getValue();
	}
	
	public int getValue(){
		return this.value.getValue();
	}
}
