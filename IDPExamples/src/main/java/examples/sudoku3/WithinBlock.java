package examples.sudoku3;

import idp.annotations.ArgumentType;
import idp.annotations.Predicate;

@Predicate
public class WithinBlock {
	
	@ArgumentType(1)
	private DomainValue row;
	
	@ArgumentType(2)
	private DomainValue column;
	
	@ArgumentType(3)
	private DomainValue block;
	
	public WithinBlock(DomainValue row, DomainValue column, DomainValue block){
		this.row = row;
		this.column = column;
		this.block = block;
	}

}
