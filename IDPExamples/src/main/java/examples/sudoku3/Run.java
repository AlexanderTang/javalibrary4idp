package examples.sudoku3;

import idp.exceptions.IDPException;
import idp.exceptions.InvalidAnnotationException;
import idp.exceptions.InvalidStructureException;
import idp.exceptions.UnavailableCollectionException;
import idp.logic.IDPSolver;
import idp.logic.IDPStructure;
import idp.logic.IDPTheory;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Run the main method in this class to solve sudoku's.
 * 
 * The representation in this package uses the following setup:
 *  - Cells are instances of an @Function annotated class.
 *  - 'Within block' objects are instances of an @Predicate annotated class.
 *  - 'Domain value' objects are instances of an @Type annotated class. An @TypeValue 
 *  	annotated field is used to represent the type's value.
 * 
 * @author Alexander Tang
 *
 */
public class Run {
	
	public static void main(String[] args){
		Run r = new Run();
		r.run();
	}
	
	public void run(){
		try {
			solveSudoku(tarek052Problem(), "tarek052");
		} catch (IDPException e) {
			e.printStackTrace();
		}
	}
	
	public void solveSudoku(IDPStructure inputStruc, String puzzleName){
		URL idpConfig = getClass().getClassLoader().getResource("idp.config");
		IDPTheory theory = IDPTheory.loadFile(getClass().getClassLoader().
				getResource("sudoku3/sudoku.idptheory"));
		
		try{
			// IDP solver with input program, default 5 models and replaces input collections
			IDPSolver solver = new IDPSolver.SolverBuilder(theory, idpConfig)
											.nbModels(5)
											.replaceInput(false)
											.build();
			
			System.out.println(puzzleName.toUpperCase() + "\n");
			
			boolean modelFound = solver.sat(inputStruc);
			System.out.println("Model found: " + modelFound + "\n");
			
			Collection<IDPStructure> structs2 = solver.modelExpand(inputStruc);
			System.out.println("Printing found models:\n");
			for(IDPStructure structure : structs2)
				printSudoku(structure);
		} catch(IDPException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public IDPStructure tarek052Problem() 
			throws InvalidAnnotationException, InvalidStructureException{
		IDPStructure struc = new IDPStructure();
		
		List<DomainValue> domainValues = new ArrayList<>();
		for(int i = 1; i <= 9; i++)
			domainValues.add(new DomainValue(i));
		
		Collection<Cell> cells = new HashSet<Cell>();
		cells.addAll(Arrays.asList(
				new Cell(domainValues.get(0),domainValues.get(2),domainValues.get(0)), 
				new Cell(domainValues.get(0),domainValues.get(5),domainValues.get(3)),
				new Cell(domainValues.get(1),domainValues.get(4),domainValues.get(5)), 
				new Cell(domainValues.get(1),domainValues.get(6),domainValues.get(2)), 
				new Cell(domainValues.get(1),domainValues.get(8),domainValues.get(4)),
				new Cell(domainValues.get(2),domainValues.get(3),domainValues.get(8)), 
				new Cell(domainValues.get(3),domainValues.get(0),domainValues.get(7)), 
				new Cell(domainValues.get(3),domainValues.get(6),domainValues.get(6)),
				new Cell(domainValues.get(3),domainValues.get(8),domainValues.get(2)), 
				new Cell(domainValues.get(4),domainValues.get(7),domainValues.get(1)), 
				new Cell(domainValues.get(4),domainValues.get(8),domainValues.get(7)),
				new Cell(domainValues.get(5),domainValues.get(0),domainValues.get(4)), 
				new Cell(domainValues.get(5),domainValues.get(4),domainValues.get(6)), 
				new Cell(domainValues.get(5),domainValues.get(6),domainValues.get(5)),
				new Cell(domainValues.get(6),domainValues.get(0),domainValues.get(2)), 
				new Cell(domainValues.get(6),domainValues.get(4),domainValues.get(7)), 
				new Cell(domainValues.get(6),domainValues.get(8),domainValues.get(5)),
				new Cell(domainValues.get(7),domainValues.get(2),domainValues.get(8)), 
				new Cell(domainValues.get(7),domainValues.get(3),domainValues.get(1)), 
				new Cell(domainValues.get(8),domainValues.get(1),domainValues.get(3)),
				new Cell(domainValues.get(8),domainValues.get(5),domainValues.get(0))));
		struc.addCollection(domainValues, DomainValue.class);
		struc.addTrueCollection(cells, Cell.class);
		struc.addClass(WithinBlock.class);
		return struc;
	}
	
	private void printSudoku(IDPStructure struc){
		try{
			int[][] sudokuMatrix = new int[9][9];
			Collection<Cell> cells = struc.getCollection(Cell.class);
			for(Cell c : cells)
				sudokuMatrix[c.getRow()-1][c.getColumn()-1] = c.getValue();
			StringBuilder sb = new StringBuilder();
			sb.append(getHorizontalLine());
			sb.append(System.lineSeparator());
			for(int i = 0; i < sudokuMatrix.length; i++){
				sb.append("|");
				for(int j = 0; j < sudokuMatrix[0].length; j++){
					if(j != 0)
						sb.append(" ");
					sb.append(sudokuMatrix[i][j]);
				}
				sb.append("|");
				sb.append(System.lineSeparator());
			}
			sb.append(getHorizontalLine());
			sb.append(System.lineSeparator());
			sb.append(System.lineSeparator());
			System.out.println(sb.toString());
		} catch(UnavailableCollectionException e){
			System.out.println("Could not retrieve the collection of cells. Error was: " +
				e.getMessage());
		}
	}
	
	private String getHorizontalLine(){
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < 18; i++)
			sb.append("-");
		return sb.toString();
	}
}
