package examples.sudoku3;

import idp.annotations.Type;
import idp.annotations.TypeValue;

@Type
public class DomainValue {
	
	@TypeValue
	private int value;
	
	public DomainValue(int value){
		this.value = value;
	}

	public int getValue(){
		return this.value;
	}
}
